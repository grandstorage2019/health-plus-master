package grand.healthplus.vollyutils;


import org.json.JSONObject;

public interface ConnectionView {
    void onRequestSuccess(JSONObject response);
    void onRequestError(Object error);
}
