package grand.healthplus.customviews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

@SuppressLint("NewApi")
public class CustomButton extends AppCompatButton {
    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomButton(Context context) {
        super(context);

        init(context);
    }

    private void init(Context context) {
        setTransformationMethod(null);
        if (!isInEditMode()) {
           // Typeface tf = null;
            //tf = Typeface.createFromAsset(getContext().getAssets(), "font.ttf");
            //setTypeface(tf);
        }
    }

}