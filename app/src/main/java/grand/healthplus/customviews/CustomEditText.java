package grand.healthplus.customviews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

@SuppressLint("NewApi")
public class CustomEditText extends AppCompatEditText {
	public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);

	}

	public CustomEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public CustomEditText(Context context) {

		super(context);
		init(context);
	}


	private void init(Context context) {
		if (!isInEditMode()) {
			//Typeface tf = null;
			//tf = Typeface.createFromAsset(getContext().getAssets(),"font.ttf");
			//setTypeface(tf);



		}
	}

}