package grand.healthplus.customviews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

@SuppressLint("NewApi")
public class CustomTextView extends AppCompatTextView {
    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);

    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomTextView(Context context) {

        super(context);
        init(context);
    }

    private void init(Context context) {
        if (!isInEditMode()) {
            //Typeface tf = null;
            //tf = Typeface.createFromAsset(getContext().getAssets(), "font.ttf");
            //setTypeface(tf);
        }
    }


     /*@Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(replaceArabicNumbers(text), type);
    }

   private String replaceArabicNumbers(CharSequence original) {
        if (original != null) {

            return original.toString().replaceAll("1", "١")
                    .replaceAll("2", "٢")
                    .replaceAll("3", "٣")
                    .replaceAll("4", "٤")
                    .replaceAll("5", "٥")
                    .replaceAll("6", "٦")
                    .replaceAll("7", "٧")
                    .replaceAll("8", "٨")
                    .replaceAll("9", "٩")
                    .replaceAll("0", "٠");

        }

        return null;
    }*/

}