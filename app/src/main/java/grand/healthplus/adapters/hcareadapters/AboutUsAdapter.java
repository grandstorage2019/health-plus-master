package grand.healthplus.adapters.hcareadapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hcaremodels.AboutUsItem;



public class AboutUsAdapter extends RecyclerView.Adapter<AboutUsAdapter.ViewHolder> {
    private ArrayList<AboutUsItem> aboutUsItems;
    Context context;


    public AboutUsAdapter(ArrayList<AboutUsItem> aboutUsItems, Context context) {
        this.aboutUsItems = aboutUsItems;
        this.context = context;
    }

    @Override
    public AboutUsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AboutUsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_about_us, parent, false));
    }

    @Override
    public void onBindViewHolder(AboutUsAdapter.ViewHolder holder, final int position) {
        final AboutUsItem aboutUsItem = aboutUsItems.get(position);
        holder.title.setText(aboutUsItem.getName());
        holder.details.setText(aboutUsItem.getDetails());

    }
    public int getItemCount() {
        return aboutUsItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_about_us_title)
        TextView title;

        @BindView(R.id.tv_about_us_details)
        TextView details;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
