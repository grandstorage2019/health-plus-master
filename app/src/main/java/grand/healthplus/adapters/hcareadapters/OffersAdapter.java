package grand.healthplus.adapters.hcareadapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hcaremodels.HCareHomeItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolder> {
    private ArrayList<HCareHomeItem> hCareHomeItems;
    RelativeLayout.LayoutParams layoutParams;
    Context context;
    int width = 0,hieght=0;

    public OffersAdapter(ArrayList<HCareHomeItem> hCareHomeItems, Context context) {
        this.hCareHomeItems = hCareHomeItems;
        this.context = context;
        width = ViewOperations.getScreenWidth(context);
        hieght=ViewOperations.getScreenHeight(context);
        layoutParams = new RelativeLayout.LayoutParams((int) width-(width / 7), hieght);
    }

    @Override
    public OffersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OffersAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hospital, parent, false));
    }

    @Override
    public void onBindViewHolder(OffersAdapter.ViewHolder holder, final int position) {
        final HCareHomeItem hospitalsItem = hCareHomeItems.get(position);
        holder.hospitalTitle.setText(hospitalsItem.getName());

        holder.hospitalContainer.setLayoutParams(layoutParams);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    MUT.startWebPage(context, hCareHomeItems.get(position).getLink());
                }
                catch (Exception e){
                    e.getStackTrace();
                }
            }
        });

        ConnectionPresenter.loadImage(holder.hospitalPhoto, "care_adverting/" + hospitalsItem.getImage());

    }

    public int getItemCount() {
        return hCareHomeItems.size();
    }

    public HCareHomeItem getItem(int position) {
        return hCareHomeItems.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_hospital_photo)
        ImageView hospitalPhoto;
        @BindView(R.id.tv_hospital_title)
        TextView hospitalTitle;
        @BindView(R.id.rl_hpspital_container)
        RelativeLayout hospitalContainer;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
