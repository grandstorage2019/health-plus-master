package grand.healthplus.adapters.hcareadapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ZoomedImageActivity;
import grand.healthplus.models.hcaremodels.AboutUsItem;
import grand.healthplus.models.hcaremodels.CarePicturesItem;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class HealthCareImagesAdapter extends RecyclerView.Adapter<HealthCareImagesAdapter.ViewHolder> {
    private ArrayList<CarePicturesItem> carePicturesItems;
    Context context;


    public HealthCareImagesAdapter(ArrayList<CarePicturesItem> carePicturesItems, Context context) {
        this.carePicturesItems = carePicturesItems;
        this.context = context;
    }

    @Override
    public HealthCareImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HealthCareImagesAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_care_image, parent, false));
    }

    @Override
    public void onBindViewHolder(HealthCareImagesAdapter.ViewHolder holder, final int position) {
        final CarePicturesItem carePicturesItem = carePicturesItems.get(position);
        ConnectionPresenter.loadImage(holder.photo, "health_cares/" + carePicturesItem.getImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ZoomedImageActivity.class);
                intent.putExtra("image",ConnectionPresenter.filesLink + "health_cares/"+carePicturesItem.getImage());
                context.startActivity(intent);
            }
        });

    }
    public int getItemCount() {
        return carePicturesItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_health_care_image_photo)
        ImageView photo;




        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
