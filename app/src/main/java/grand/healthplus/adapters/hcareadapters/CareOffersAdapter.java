package grand.healthplus.adapters.hcareadapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ZoomedImageActivity;
import grand.healthplus.models.hcaremodels.CareOffersItem;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class CareOffersAdapter extends RecyclerView.Adapter<CareOffersAdapter.ViewHolder> {
    private ArrayList<CareOffersItem> careOffersItems;
    Context context;


    public CareOffersAdapter(ArrayList<CareOffersItem> careOffersItems, Context context) {
        this.careOffersItems = careOffersItems;
        this.context = context;
    }

    @Override
    public CareOffersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CareOffersAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offer, parent, false));
    }

    @Override
    public void onBindViewHolder(CareOffersAdapter.ViewHolder holder, final int position) {
        final CareOffersItem careOffersItem = careOffersItems.get(position);
        ConnectionPresenter.loadImage(holder.photo,"health_cares/"+careOffersItem.getImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ZoomedImageActivity.class);
                intent.putExtra("image",ConnectionPresenter.filesLink + "health_cares/"+careOffersItem.getImage());
                context.startActivity(intent);
            }
        });
    }
    public int getItemCount() {
        return careOffersItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_offer_photo)
        ImageView photo;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
