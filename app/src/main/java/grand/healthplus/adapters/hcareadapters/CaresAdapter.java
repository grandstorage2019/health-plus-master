package grand.healthplus.adapters.hcareadapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ChatActivity;
import grand.healthplus.activities.hcareactivities.HCareMoreinfoActivity;
import grand.healthplus.models.hcaremodels.ChatResponse;
import grand.healthplus.models.hcaremodels.HealthCareItem;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class CaresAdapter extends RecyclerView.Adapter<CaresAdapter.ViewHolder> {
    private ArrayList<HealthCareItem> healthCareItems;
    Context context;


    public CaresAdapter(ArrayList<HealthCareItem> healthCareItems, Context context) {
        this.healthCareItems = healthCareItems;
        this.context = context;
    }

    @Override
    public CaresAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CaresAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_health_care, parent, false));
    }

    @Override
    public void onBindViewHolder(CaresAdapter.ViewHolder holder, final int position) {
        final HealthCareItem healthCareItem = healthCareItems.get(position);
        holder.name.setText(healthCareItem.getCareName());
        holder.about.setText(healthCareItem.getAbout());
        holder.address.setText(healthCareItem.getGovernorateName() + " - " + healthCareItem.getAreaName() + " - " + healthCareItem.getLocationComment());
        holder.phone.setText(healthCareItem.getPhone());
        holder.time.setText(healthCareItem.getFromTime() + " : " + healthCareItem.getToTime());
        holder.rating.setRating((float) healthCareItem.getRate());
        holder.views.setText(context.getResources().getString(R.string.views) + " : " + healthCareItem.getViews());

        if (healthCareItem.isFavourite()) {
            holder.favourite.setImageResource(R.drawable.ic_heart_selected);
        } else {
            holder.favourite.setImageResource(R.drawable.ic_empty_heart);
        }

        ConnectionPresenter.loadImage(holder.photo, "care/" + healthCareItem.getCareLogo());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HCareMoreinfoActivity.class);
                intent.putExtra("healthCareInfo", healthCareItem);
                context.startActivity(intent);
            }
        });

        holder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SharedPreferenceHelper.isLogined(context)) {
                    addDeleteFavourite(position, !healthCareItem.isFavourite());
                }
            }
        });

    }


    private void addDeleteFavourite(final int position, final boolean isAdd) {

        JSONObject jsonObject = new JSONObject();
        try {
            String method =isAdd?"add_favourite":"delete_favourite";
            jsonObject.put("method",method);
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(context).getId());
            jsonObject.put("care_id", healthCareItems.get(position).getCareId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(context, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                        healthCareItems.get(position).setFavourite(isAdd);
                        notifyDataSetChanged();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, true);
    }

    public int getItemCount() {
        return healthCareItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_health_care_name)
        TextView name;
        @BindView(R.id.tv_health_care_about)
        TextView about;
        @BindView(R.id.tv_health_care_address)
        TextView address;
        @BindView(R.id.tv_health_care_phone)
        TextView phone;
        @BindView(R.id.tv_health_care_time)
        TextView time;
        @BindView(R.id.rb_health_care_rating)
        MaterialRatingBar rating;
        @BindView(R.id.tv_health_care_view)
        TextView views;
        @BindView(R.id.iv_doctor_details_favourite)
        ImageView favourite;
        @BindView(R.id.iv_health_care_photo)
        ImageView photo;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
