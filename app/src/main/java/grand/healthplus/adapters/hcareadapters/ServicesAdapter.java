package grand.healthplus.adapters.hcareadapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hcareactivities.HCareMoreinfoActivity;
import grand.healthplus.models.hcaremodels.ServicesItem;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    private ArrayList<ServicesItem> servicesItems;
    Context context;


    public ServicesAdapter(ArrayList<ServicesItem> servicesItems, Context context) {
        this.servicesItems = servicesItems;
        this.context = context;
    }

    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ServicesAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_closed_service, parent, false));
    }

    @Override
    public void onBindViewHolder(ServicesAdapter.ViewHolder holder, final int position) {
        final ServicesItem servicesItem = servicesItems.get(position);

        holder.title.setText(servicesItem.getName());

        if(servicesItem.isOpened()){
            holder.title.setTextColor(context.getResources().getColor(R.color.colorGrayLevel3));
            holder.arrow.setImageResource(R.drawable.ic_down_arrow);
            holder.serviceContainer.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
            holder.services.setVisibility(View.GONE);
        }else {
            holder.arrow.setImageResource(R.drawable.ic_up_white_arrow);
            holder.serviceContainer.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
            holder.title.setTextColor(context.getResources().getColor(R.color.colorWhite));
            holder.services.setVisibility(View.VISIBLE);
        }

        ConnectionPresenter.loadImage(holder.photo,"care_services/"+servicesItem.getLogo());

        if(servicesItem.getSubServices()!=null) {
            SubCareAdapter subCareAdapter = new SubCareAdapter(new ArrayList<>(servicesItem.getSubServices()), context);
            ViewOperations.setRVHVertical(context, holder.services);
            holder.services.setAdapter(subCareAdapter);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    servicesItems.get(position).setOpened(!servicesItems.get(position).isOpened());
                    notifyDataSetChanged();
                }
            });
        }else {
            holder.services.setAdapter(null);
            holder.itemView.setOnClickListener(null);
        }


    }
    public int getItemCount() {
        return servicesItems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_closed_service_icon)
        ImageView photo;
        @BindView(R.id.tv_closed_service_title)
        TextView title;
        @BindView(R.id.iv_closed_service_arrow)
        ImageView arrow;
        @BindView(R.id.rl_closed_service_container)
        RelativeLayout serviceContainer;

        @BindView(R.id.rv_closed_service_sub_services)
        RecyclerView services;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
