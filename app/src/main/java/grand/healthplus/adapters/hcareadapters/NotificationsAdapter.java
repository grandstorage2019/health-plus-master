package grand.healthplus.adapters.hcareadapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.SingleNotificationActivity;
import grand.healthplus.models.hplusmodels.NotificationsItem;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    private ArrayList<NotificationsItem> notificationsItems;
    Context context;


    public NotificationsAdapter(ArrayList<NotificationsItem> notificationsItems, Context context) {
        this.notificationsItems = notificationsItems;
        this.context = context;

    }

    @Override
    public NotificationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(final NotificationsAdapter.ViewHolder holder, final int position) {
        final NotificationsItem notificationsItem = notificationsItems.get(position);
        holder.title.setText(notificationsItem.getMessage());
        holder.time.setText(notificationsItem.getCreatedAt());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SingleNotificationActivity.class);
                intent.putExtra("image","notification_care/"+notificationsItem.getImage());
                intent.putExtra("title",notificationsItem.getMessage());
                context.startActivity(intent);
            }
        });

        ConnectionPresenter.loadImage(holder.notificationIcon,"notification_care/"+notificationsItem.getImage());

    }
    public int getItemCount() {
        return notificationsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_notification_title)
        TextView title;

        @BindView(R.id.tv_notification_time)
        TextView time;

        @BindView(R.id.iv_notification_icon)
        ImageView notificationIcon;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
