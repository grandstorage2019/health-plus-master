package grand.healthplus.adapters.hcareadapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ChooseDepartmentActivity;
import grand.healthplus.activities.SignUpActivity;
import grand.healthplus.activities.hcareactivities.HCareMoreinfoActivity;
import grand.healthplus.models.hcaremodels.HealthCareItem;
import grand.healthplus.models.hcaremodels.SubServicesItem;
import grand.healthplus.models.hcaremodels.registeration.SignUpResponse;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class SubCareAdapter extends RecyclerView.Adapter<SubCareAdapter.ViewHolder> {
    private ArrayList<SubServicesItem> subServicesItems;
    Context context;


    public SubCareAdapter(ArrayList<SubServicesItem> subServicesItems, Context context) {
        this.subServicesItems = subServicesItems;
        this.context = context;
    }

    @Override
    public SubCareAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SubCareAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_opened_service, parent, false));
    }

    @Override
    public void onBindViewHolder(SubCareAdapter.ViewHolder holder, final int position) {
        final SubServicesItem subServicesItem = subServicesItems.get(position);
        holder.name.setText(subServicesItem.getName());
        holder.quantity.setText(subServicesItem.getQuantity()+" "+subServicesItem.getUnit());
        holder.price.setText((int)subServicesItem.getPrice()+" KD");
        ConnectionPresenter.loadImage(holder.image,"CareSubService/"+subServicesItem.getImage());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SharedPreferenceHelper.isLogined(context)) {
                    serviceDialog(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return subServicesItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_opened_service_title)
        TextView name;

        @BindView(R.id.tv_opened_service_quantity)
        TextView quantity;

        @BindView(R.id.tv_opened_service_price)
        TextView price;

        @BindView(R.id.iv_closed_service_image)
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    private void addService(int position) {

            JSONObject sentData = new JSONObject();
            try {
                sentData.put("method", "add_service");
                sentData.put("care_sub_services_id", subServicesItems.get(position).getId());
                sentData.put("patient_id",SharedPreferenceHelper.getUserDetails(context).getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(context, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                    if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                        MUT.successDialog(context);
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }

            });
            connectionPresenter.connect(sentData, true, true);

    }

    public  void serviceDialog(final int position) {
        final Dialog serviceDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar);
        serviceDialog.setCancelable(true);
        serviceDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(context);
        View ratingView = inflater.inflate(R.layout.dialog_cancel_booking, null);
        serviceDialog.setContentView(ratingView);

        TextView title = ratingView.findViewById(R.id.tv_cancel_booking_title);
        TextView details = ratingView.findViewById(R.id.tv_cancel_booking_details);

        title.setText(context.getResources().getString(R.string.book_service));
        details.setText(context.getResources().getString(R.string.want_to_book_service));

        Button confirm = ratingView.findViewById(R.id.btn_cancel_booking_confirm);
        Button cancel = ratingView.findViewById(R.id.btn_cancel_booking_cancel);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addService(position);
                serviceDialog.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceDialog.cancel();
            }
        });

        serviceDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0e000000")));
        serviceDialog.show();
    }

}
