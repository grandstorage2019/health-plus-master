package grand.healthplus.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import grand.healthplus.R;
import grand.healthplus.models.MenuModel;


@SuppressLint("InflateParams")
public class MenuAdapter extends BaseAdapter {

	ArrayList<MenuModel> menuItems;
	LayoutInflater inflater;
	Context context;
	public MenuAdapter(Context context, ArrayList<MenuModel> results) {
		menuItems = results;
		inflater = LayoutInflater.from(context);
		this.context = context;
	}

	public int getCount() {
		return menuItems.size();
	}

	public Object getItem(int position) {
		return menuItems.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {

			convertView = inflater.inflate( R.layout.item_menu, null);
			holder = new ViewHolder();
			holder.title =  convertView.findViewById(R.id.menuItemTitle);
			convertView.setTag(holder);
		}

		else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.title.setText(menuItems.get(position).getTitle());

		return convertView;
	}

	public class ViewHolder {
		TextView title;


	}
}
