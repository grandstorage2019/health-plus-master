package grand.healthplus.adapters.doctoradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.doctormodels.ServicesItem;



public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    private ArrayList<ServicesItem> servicesItems;
    Context context;


    public ServicesAdapter(ArrayList<ServicesItem> servicesItems, Context context) {
        this.servicesItems = servicesItems;
        this.context = context;
    }

    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ServicesAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service, parent, false));
    }

    @Override
    public void onBindViewHolder(ServicesAdapter.ViewHolder holder, final int position) {
        final ServicesItem servicesItem = servicesItems.get(position);

        if(servicesItem.isChecked()){
            holder.icon.setImageResource(R.drawable.ic_selected_checked);
        }else {
            holder.icon.setImageResource(R.drawable.ic_selected_un_checked);
        }

        holder.title.setText(servicesItem.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicesItem.setChecked(!servicesItem.isChecked());
                notifyDataSetChanged();
            }
        });

    }
    public int getItemCount() {
        return servicesItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_service_title)
        TextView title;

        @BindView(R.id.iv_service_icon)
        ImageView icon;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
