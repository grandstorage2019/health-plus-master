package grand.healthplus.adapters.doctoradapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.doctoractivities.PlanDetailsActivity;
import grand.healthplus.models.doctormodels.PlansItem;


public class PlansAdapter extends RecyclerView.Adapter<PlansAdapter.ViewHolder> {
    private ArrayList<PlansItem> plansItems;
    Context context;


    public PlansAdapter(ArrayList<PlansItem> plansItems, Context context) {
        this.plansItems = plansItems;
        this.context = context;
    }

    @Override
    public PlansAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlansAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_plan, parent, false));
    }

    @Override
    public void onBindViewHolder(PlansAdapter.ViewHolder holder, final int position) {
        final PlansItem planItem = plansItems.get(position);
        holder.title.setText(planItem.getName());
        holder.details.setText(planItem.getDetails());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PlanDetailsActivity.class);
                intent.putExtra("planItem",planItem);
                context.startActivity(intent);
            }
        });

    }
    public int getItemCount() {
        return plansItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_plan_title)
        TextView title;

        @BindView(R.id.tv_plan_details)
        TextView details;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
