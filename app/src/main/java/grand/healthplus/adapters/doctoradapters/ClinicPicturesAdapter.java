package grand.healthplus.adapters.doctoradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.doctormodels.ClinicPicturesItem;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class ClinicPicturesAdapter extends RecyclerView.Adapter<ClinicPicturesAdapter.ViewHolder> {
    private ArrayList<ClinicPicturesItem> clinicPicturesItems;
    Context context;


    public ClinicPicturesAdapter(ArrayList<ClinicPicturesItem> clinicPicturesItems, Context context) {
        this.clinicPicturesItems = clinicPicturesItems;
        this.context = context;
    }

    @Override
    public ClinicPicturesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ClinicPicturesAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(ClinicPicturesAdapter.ViewHolder holder, final int position) {
        final ClinicPicturesItem clinicPicturesItem = clinicPicturesItems.get(position);
        ConnectionPresenter.loadImage(holder.image, "Clinic/" + clinicPicturesItem.getImage());
         System.out.println( "Link is :Clinic/" + clinicPicturesItem.getImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }
    public int getItemCount() {
        return clinicPicturesItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_health_image_photo)
        ImageView image;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
