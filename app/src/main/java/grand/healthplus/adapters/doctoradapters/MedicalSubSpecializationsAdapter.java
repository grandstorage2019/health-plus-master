package grand.healthplus.adapters.doctoradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.doctormodels.SubSpecializationsItem;


public class MedicalSubSpecializationsAdapter extends RecyclerView.Adapter<MedicalSubSpecializationsAdapter.ViewHolder> {
    private ArrayList<SubSpecializationsItem> subSpecializationsItems;
    Context context;


    public MedicalSubSpecializationsAdapter(ArrayList<SubSpecializationsItem> subSepcializationsItems, Context context) {
        this.subSpecializationsItems = subSepcializationsItems;
        this.context = context;
    }

    @Override
    public MedicalSubSpecializationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MedicalSubSpecializationsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_specialization, parent, false));
    }

    @Override
    public void onBindViewHolder(MedicalSubSpecializationsAdapter.ViewHolder holder, final int position) {
        final SubSpecializationsItem subSpecializationsItem = subSpecializationsItems.get(position);

        if(subSpecializationsItem.isChecked()){
            holder.icon.setImageResource(R.drawable.ic_selected_checked);
        }else {
            holder.icon.setImageResource(R.drawable.ic_selected_un_checked);
        }

        holder.title.setText(subSpecializationsItem.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subSpecializationsItem.setChecked(!subSpecializationsItem.isChecked());
                notifyDataSetChanged();
            }
        });

    }
    public int getItemCount() {
        return subSpecializationsItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_sub_specialization_title)
        TextView title;

        @BindView(R.id.iv_sub_specialization_icon)
        ImageView icon;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
