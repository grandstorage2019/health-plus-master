package grand.healthplus.adapters.doctoradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;

import grand.healthplus.models.doctormodels.ChatItem;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private ArrayList<ChatItem> chatItems;
    Context context;
    String doctorPhoto = "";

    public ChatAdapter(ArrayList<ChatItem> chatItems, Context context, String doctorPhoto) {
        this.chatItems = chatItems;
        this.doctorPhoto = doctorPhoto;
        this.context = context;
    }

    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChatAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false));
    }

    @Override
    public void onBindViewHolder(ChatAdapter.ViewHolder holder, final int position) {
        final ChatItem chatItem = chatItems.get(position);
        holder.message.setText(chatItem.getMassage());
        holder.message2.setText(chatItem.getMassage());
        ConnectionPresenter.loadImage(holder.photo, "patient_profile/" + doctorPhoto);

        if (chatItem.getWho().equals("doctor")) {
            holder.photo.setVisibility(View.GONE);
            holder.message.setVisibility(View.GONE);
            holder.message2.setVisibility(View.VISIBLE);
        } else {
            holder.photo.setVisibility(View.VISIBLE);
            holder.message.setVisibility(View.VISIBLE);
            holder.message2.setVisibility(View.GONE);
        }


    }

    public int getItemCount() {
        return chatItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_chat_photo)
        ImageView photo;

        @BindView(R.id.tv_chat_message)
        TextView message;

        @BindView(R.id.tv_chat_message_2)
        TextView message2;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
