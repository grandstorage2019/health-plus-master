package grand.healthplus.adapters.doctoradapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.doctoractivities.ChatActivity;
import grand.healthplus.models.doctormodels.ChattersItem;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class ChattersAdapter extends RecyclerView.Adapter<ChattersAdapter.ViewHolder> {
    private ArrayList<ChattersItem> chattersItems;
    Context context;


    public ChattersAdapter(ArrayList<ChattersItem> chattersItems, Context context) {
        this.chattersItems = chattersItems;
        this.context = context;
    }

    @Override
    public ChattersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChattersAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatters, parent, false));
    }

    @Override
    public void onBindViewHolder(ChattersAdapter.ViewHolder holder, final int position) {
        final ChattersItem chattersItem = chattersItems.get(position);
        holder.name.setText(chattersItem.getName());
        holder.message.setText(chattersItem.getMassage());

        if(chattersItem.getImage()==null||chattersItem.getImage().equals("")||chattersItem.getImage().equals("null")) {
            holder.photo.setImageResource(R.drawable.ic_user_temp);
        }else {
            ConnectionPresenter.loadImage(holder.photo, "patient_profile/" + chattersItem.getImage());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("chattersItemDetails",chattersItem);
                context.startActivity(intent);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reservationDialog(position);
            }
        });

    }
    public int getItemCount() {
        return chattersItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_chatters_name)
        TextView name;
        @BindView(R.id.tv_chatters_message)
        TextView message;
        @BindView(R.id.iv_chatters_photo)
        ImageView photo;
        @BindView(R.id.iv_chatters_delete)
        ImageView delete;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    private void deleteChat(final int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "delete_chat");
            jsonObject.put("patient_id", chattersItems.get(position).getPatientId());
            jsonObject.put("doctor_id", chattersItems.get(position).getDoctorId());

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(context, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    Toast.makeText(context, "" + context.getResources().getString(R.string.deleted_successfully), Toast.LENGTH_SHORT).show();
                    chattersItems.remove(position);
                    notifyDataSetChanged();

                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }


    public void reservationDialog(final int position) {
        final Dialog ratingDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar);
        ratingDialog.setCancelable(true);
        ratingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(context);
        View ratingView = inflater.inflate(R.layout.dialog_cancel_booking, null);
        ratingDialog.setContentView(ratingView);

        Button confirm = ratingView.findViewById(R.id.btn_cancel_booking_confirm);
        Button cancel = ratingView.findViewById(R.id.btn_cancel_booking_cancel);
        TextView title = ratingView.findViewById(R.id.tv_cancel_booking_title);
        TextView details = ratingView.findViewById(R.id.tv_cancel_booking_details);

        title.setText(context.getResources().getString(R.string.delete_chat));
        details.setText(context.getResources().getString(R.string.sure_delete_chat));

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteChat(position);
                ratingDialog.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingDialog.cancel();
            }
        });

        ratingDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0e000000")));
        ratingDialog.show();
    }

}
