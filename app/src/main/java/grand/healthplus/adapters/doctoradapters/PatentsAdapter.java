package grand.healthplus.adapters.doctoradapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.doctoractivities.PatientsDetailsActivity;
import grand.healthplus.activities.doctoractivities.PlanDetailsActivity;
import grand.healthplus.models.doctormodels.PatientsItem;
import grand.healthplus.models.doctormodels.PlansItem;


public class PatentsAdapter extends RecyclerView.Adapter<PatentsAdapter.ViewHolder> {
    private ArrayList<PatientsItem> patientsItems;
    Context context;


    public PatentsAdapter(ArrayList<PatientsItem> patientsItems, Context context) {
        this.patientsItems = patientsItems;
        this.context = context;
    }

    @Override
    public PatentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PatentsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_patient_2, parent, false));
    }

    @Override
    public void onBindViewHolder(PatentsAdapter.ViewHolder holder, final int position) {
        final PatientsItem patientsItem = patientsItems.get(position);
        holder.name.setText(context.getResources().getString(R.string.patient_name) + " : " + patientsItem.getName());
        holder.id.setText(context.getResources().getString(R.string.patient_id) + " : " + patientsItem.getId());
        holder.mobile.setText(context.getResources().getString(R.string.mobile) + " : " + patientsItem.getPhone());
        holder.sex.setText(context.getResources().getString(R.string.sex) + " : " + (patientsItem.getGender().equals("male") ? context.getResources().getString(R.string.male) : context.getResources().getString(R.string.female)));

        //holder.date.setText(context.getResources().getString(R.string.birth_day) + " : " + patientsItem.get);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PatientsDetailsActivity.class);
                intent.putExtra("PatientsDetails",patientsItem);
                context.startActivity(intent);
            }
        });

    }
    public int getItemCount() {
        return patientsItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_patient_appointment_name)
        TextView name;

        @BindView(R.id.tv_patient_appointment_id)
        TextView id;
        @BindView(R.id.tv_patient_appointment_mobile)
        TextView mobile;
        @BindView(R.id.tv_patient_appointment_sex)
        TextView sex;
        @BindView(R.id.tv_patient_appointment_date)
        TextView date;



        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
