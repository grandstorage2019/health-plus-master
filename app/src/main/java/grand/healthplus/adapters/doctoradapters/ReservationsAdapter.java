package grand.healthplus.adapters.doctoradapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;

import grand.healthplus.activities.doctoractivities.ReservationsActivity;
import grand.healthplus.models.doctormodels.ReservationsItem;


public class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.ViewHolder> {
    private ArrayList<ReservationsItem> reservationsItems;
    Context context;


    public ReservationsAdapter(ArrayList<ReservationsItem> reservationsItems, Context context) {
        this.reservationsItems = reservationsItems;
        this.context = context;
    }

    @Override
    public ReservationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReservationsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_appointment_summary, parent, false));
    }

    @Override
    public void onBindViewHolder(ReservationsAdapter.ViewHolder holder, final int position) {
        final ReservationsItem reservationsItem = reservationsItems.get(position);


        holder.day.setText(reservationsItem.getDayName(context));

        int numberOfReservations = reservationsItem.getReservations();
        String reservations = numberOfReservations+" "+(context.getResources().getString((numberOfReservations==1)?R.string.reservation:R.string.reservations));

        holder.details.setText(reservationsItem.isHaveReservations() ? reservations: context.getResources().getString(R.string.no_reservations));

        holder.month.setText(reservationsItem.getDate());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReservationsActivity.class);
                intent.putExtra("date",reservationsItem.getDate());
                context.startActivity(intent);
            }
        });

    }

    public int getItemCount() {
        return reservationsItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_appointment_summary_day)
        TextView day;

        @BindView(R.id.tv_appointment_summary_details)
        TextView details;

        @BindView(R.id.tv_appointment_summary_month)
        TextView month;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
