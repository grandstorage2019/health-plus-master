package grand.healthplus.adapters.doctoradapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ZoomedImageActivity;
import grand.healthplus.models.doctormodels.ClinicPicturesItem;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class LicensesAdapter extends RecyclerView.Adapter<LicensesAdapter.ViewHolder> {
    private ArrayList<ClinicPicturesItem> clinicPicturesItems;
    Context context;


    public LicensesAdapter(ArrayList<ClinicPicturesItem> clinicPicturesItems, Context context) {
        this.clinicPicturesItems = clinicPicturesItems;
        this.context = context;
    }

    @Override
    public LicensesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LicensesAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(LicensesAdapter.ViewHolder holder, final int position) {
        final ClinicPicturesItem clinicPicturesItem = clinicPicturesItems.get(position);
        ConnectionPresenter.loadImage(holder.image, "license/" + clinicPicturesItem.getImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ZoomedImageActivity.class);
                intent.putExtra("image",ConnectionPresenter.filesLink + "license/" + clinicPicturesItem.getImage());
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                return false;
            }
        });

    }
    public int getItemCount() {
        return clinicPicturesItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_health_image_photo)
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
