package grand.healthplus.adapters.doctoradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.doctormodels.InsuranceCompaniesItem;
import grand.healthplus.models.doctormodels.ServicesItem;


public class InsuranceCompaniesAdapter extends RecyclerView.Adapter<InsuranceCompaniesAdapter.ViewHolder> {
    private ArrayList<InsuranceCompaniesItem> insuranceCompaniesItems;
    Context context;


    public InsuranceCompaniesAdapter(ArrayList<InsuranceCompaniesItem> insuranceCompaniesItems, Context context) {
        this.insuranceCompaniesItems = insuranceCompaniesItems;
        this.context = context;
    }

    @Override
    public InsuranceCompaniesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InsuranceCompaniesAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service, parent, false));
    }

    @Override
    public void onBindViewHolder(InsuranceCompaniesAdapter.ViewHolder holder, final int position) {
        final InsuranceCompaniesItem insuranceCompaniesItem = insuranceCompaniesItems.get(position);

        if(insuranceCompaniesItem.isChecked()){
            holder.icon.setImageResource(R.drawable.ic_selected_checked);
        }else {
            holder.icon.setImageResource(R.drawable.ic_selected_un_checked);
        }

        holder.title.setText(insuranceCompaniesItem.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insuranceCompaniesItem.setChecked(!insuranceCompaniesItem.isChecked());
                notifyDataSetChanged();
            }
        });

    }
    public int getItemCount() {
        return insuranceCompaniesItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_service_title)
        TextView title;

        @BindView(R.id.iv_service_icon)
        ImageView icon;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
