package grand.healthplus.adapters.doctoradapters;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.doctormodels.TimeModel;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.utils.ViewOperations;


public class TimesAdapter extends RecyclerView.Adapter<TimesAdapter.ViewHolder> {
    private ArrayList<TimeModel> timeModels;
    Context context;


    public TimesAdapter(ArrayList<TimeModel> timeModels, Context context) {
        this.timeModels = timeModels;
        this.context = context;
    }

    @Override
    public TimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TimesAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_times_of_work, parent, false));
    }

    @Override
    public void onBindViewHolder(final TimesAdapter.ViewHolder holder, final int position) {
        final TimeModel timeModel = timeModels.get(position);

        if (timeModel.isDayOpened()) {
            holder.timesContainer.setVisibility(View.VISIBLE);
        } else {
            holder.timesContainer.setVisibility(View.GONE);
        }


        try {

            String fromTime[]=timeModel.getFromTime().split(":");
            String toTime[]=timeModel.getToTime().split(":");
            holder.from.setText(fromTime[0]+":"+fromTime[1]);
            holder.to.setText(toTime[0]+":"+toTime[1]);
        }
        catch (Exception e){
            holder.from.setText(timeModel.getFromTime());
            holder.to.setText(timeModel.getToTime());
            e.getStackTrace();
        }

        holder.reservations.setText(timeModel.getReservations());

        holder.waitingTime.setText(timeModel.getWaitingTime() + " " + (timeModel.getWaitingTime().equals("") ? "" : context.getResources().getString(R.string.minutes)));
        holder.reservationType.setText(timeModel.getType());
        holder.isDayOpen.setText(timeModel.getDay());
        holder.isDayOpen.setChecked(timeModel.isDayOpened());

        if (timeModel.isBookFirst()) {
            holder.reservationSpinner.setVisibility(View.VISIBLE);
            holder.bookFirstSpinner.setVisibility(View.GONE);
            holder.reservationType.setText(context.getResources().getString(R.string.book_first));


        } else {
            holder.reservationSpinner.setVisibility(View.GONE);
            holder.bookFirstSpinner.setVisibility(View.VISIBLE);
            holder.reservationType.setText(context.getResources().getString(R.string.waiting_time));
        }

        holder.isDayOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeModel.setDayOpened(!timeModel.isDayOpened());
                notifyDataSetChanged();
            }
        });

        holder.from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromTime(position);
            }
        });

        holder.to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toTime(position);
            }
        });

        holder.reservationType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setReservationType(position);
            }
        });

        holder.reservationSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setReservationSpinner(position);
            }
        });

        holder.reservations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setReservationSpinner(position);
            }
        });


        holder.waitingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setWaitingTimeSpinner(position);
            }
        });

    }

    public int getItemCount() {
        return timeModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.s_work_times_switch)
        Switch isDayOpen;
        @BindView(R.id.et_work_times_from)
        EditText from;
        @BindView(R.id.et_work_times_to)
        EditText to;
        @BindView(R.id.et_work_times_reservations_type)
        EditText reservationType;
        @BindView(R.id.et_work_times_reservations)
        EditText reservations;
        @BindView(R.id.et_work_times_wating_time)
        EditText waitingTime;
        @BindView(R.id.ll_work_times)
        LinearLayout timesContainer;
        @BindView(R.id.ll_work_times_book_first)
        LinearLayout bookFirstSpinner;
        @BindView(R.id.ll_work_times_reservations)
        LinearLayout reservationSpinner;
        @BindView(R.id.ll_work_times_type)
        LinearLayout typeSpinner;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }



    private void setReservationSpinner(final int pos) {
        Dialog reservationsDialog = null;
        final ArrayList<SpinnerModel> reservationItems;

            reservationItems = new ArrayList<>();

            for (int i = 0; i < 30; i++) {
                reservationItems.add(new SpinnerModel(i, "" + (i + 1)));
            }

            reservationsDialog = ViewOperations.setSpinnerDialog(context, reservationItems, context.getResources().getString(R.string.number_of_booking), new SpinnerListener() {
                @Override
                public void onItemClicked(int position) {
                    timeModels.get(pos).setReservations(reservationItems.get(position).getTitle());
                    notifyDataSetChanged();
                }
            });

            reservationsDialog.show();

    }


    private void setReservationType(final int pos) {
        Dialog reservationTypeDialog = null;
        final ArrayList<SpinnerModel> reservationTypeItems;
        reservationTypeItems = new ArrayList<>();
        reservationTypeItems.add(new SpinnerModel(0, context.getResources().getString(R.string.book_first)));
        reservationTypeItems.add(new SpinnerModel(1, context.getResources().getString(R.string.waiting_time)));
        reservationTypeDialog = ViewOperations.setSpinnerDialog(context, reservationTypeItems, context.getResources().getString(R.string.reservation_type), new SpinnerListener() {
            @Override
            public void onItemClicked(int position) {
                timeModels.get(pos).setType(reservationTypeItems.get(position).getTitle());
                timeModels.get(pos).setBookFirst(position == 0);
                notifyDataSetChanged();
            }
        });
        reservationTypeDialog.show();
    }


    private void setWaitingTimeSpinner(final int pos) {
        Dialog waitingTimeDialog = null;
        final ArrayList<SpinnerModel> waitingTimeItems;

        waitingTimeItems = new ArrayList<>();

        for (int i = 0; i < 60; i += 5) {
            waitingTimeItems.add(new SpinnerModel(i, "" + (i + 5)));
        }

        waitingTimeDialog = ViewOperations.setSpinnerDialog(context, waitingTimeItems, context.getResources().getString(R.string.waiting_time), new SpinnerListener() {
            @Override
            public void onItemClicked(int position) {
                timeModels.get(pos).setWaitingTime(waitingTimeItems.get(position).getTitle());
                notifyDataSetChanged();
            }
        });
        waitingTimeDialog.show();

    }


    private void toTime(final int position) {
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);

        final TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String selectedDate = selectedHour + ":" + selectedMinute;
                timeModels.get(position).setToTime(selectedDate);
                notifyDataSetChanged();
            }
        }, hour, minute, true);

        timePickerDialog.show();
    }

    private void fromTime(final int position) {
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);

        final TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String selectedDate = selectedHour + ":" + selectedMinute;
                timeModels.get(position).setFromTime(selectedDate);
                notifyDataSetChanged();
            }
        }, hour, minute, true);

        timePickerDialog.show();
    }


}
