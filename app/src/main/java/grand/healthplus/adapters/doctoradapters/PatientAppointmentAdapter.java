package grand.healthplus.adapters.doctoradapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.doctoractivities.ReservationsActivity;
import grand.healthplus.models.doctormodels.PatientReservationItem;
import grand.healthplus.models.doctormodels.PatientReservationResponse;
import grand.healthplus.models.doctormodels.ReservationsItem;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class PatientAppointmentAdapter extends RecyclerView.Adapter<PatientAppointmentAdapter.ViewHolder> {
    private ArrayList<PatientReservationItem> patientReservationItems;
    Context context;


    public PatientAppointmentAdapter(ArrayList<PatientReservationItem> patientReservationItems, Context context) {
        this.patientReservationItems = patientReservationItems;
        this.context = context;
    }

    @Override
    public PatientAppointmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PatientAppointmentAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_patient_appointment, parent, false));
    }

    @Override
    public void onBindViewHolder(PatientAppointmentAdapter.ViewHolder holder, final int position) {
        final PatientReservationItem reservationsItem = patientReservationItems.get(position);

        holder.name.setText(context.getResources().getString(R.string.patient_name) + " : " + reservationsItem.getName());
        holder.id.setText(context.getResources().getString(R.string.patient_id) + " : " + reservationsItem.getId());
        holder.mobile.setText(context.getResources().getString(R.string.mobile) + " : " + reservationsItem.getPhone());

        holder.sex.setText(context.getResources().getString(R.string.sex) + " : " + (reservationsItem.getGender().equals("male") ? context.getResources().getString(R.string.male) : context.getResources().getString(R.string.female)));
        holder.time.setText(context.getResources().getString(R.string.time) + " : " + reservationsItem.getTime());
        holder.date.setText(context.getResources().getString(R.string.date) + " : " + patientReservationItems.get(0).getDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.addPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPatientDialog(reservationsItem.getId(),reservationsItem.getName());
            }
        });

    }

    public int getItemCount() {
        return patientReservationItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_patient_appointment_name)
        TextView name;
        @BindView(R.id.tv_patient_appointment_time)
        TextView time;
        @BindView(R.id.tv_patient_appointment_id)
        TextView id;
        @BindView(R.id.tv_patient_appointment_mobile)
        TextView mobile;
        @BindView(R.id.tv_patient_appointment_sex)
        TextView sex;
        @BindView(R.id.tv_patient_appointment_date)
        TextView date;
        @BindView(R.id.iv_patient_appointment_add)
        ImageView addPatient;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void addPatientDialog(final String patientId,String reservationName) {
        final Dialog ratingDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar);
        ratingDialog.setCancelable(true);
        ratingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(context);
        View ratingView = inflater.inflate(R.layout.dialog_cancel_booking, null);
        ratingDialog.setContentView(ratingView);

        Button confirm = ratingView.findViewById(R.id.btn_cancel_booking_confirm);
        Button cancel = ratingView.findViewById(R.id.btn_cancel_booking_cancel);

        TextView details = ratingView.findViewById(R.id.tv_cancel_booking_details);
        TextView title = ratingView.findViewById(R.id.tv_cancel_booking_title);

        title.setText(context.getResources().getString(R.string.add_new_patient));
        details.setText(context.getResources().getString(R.string.want_to_add_new_patient)+" "+reservationName);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPatient(patientId);
                ratingDialog.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingDialog.cancel();
            }
        });

        ratingDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0e000000")));
        ratingDialog.show();
    }

    private void addPatient(String patientId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "add_patient_from_reservation");
            jsonObject.put("added_by_doctor_id", DoctorSharedPreferenceHelper.getUserDetails(context).getId());
            jsonObject.put("id", patientId);

        } catch (Exception e) {
            e.getStackTrace();
        }


        ConnectionPresenter connectionPresenter = new ConnectionPresenter(context, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                PatientReservationResponse patientReservationResponse = new Gson().fromJson(response.toString(), PatientReservationResponse.class);
                if (patientReservationResponse.getState() == ConnectionPresenter.SUCCESS) {
                    Toast.makeText(context, context.getResources().getString(R.string.patient_added), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }

}
