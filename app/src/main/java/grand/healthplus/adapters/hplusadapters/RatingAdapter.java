package grand.healthplus.adapters.hplusadapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hplusmodels.RatingItem;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.ViewHolder> {
    private ArrayList<RatingItem> ratingItems;
    Context context;

    public RatingAdapter(ArrayList<RatingItem> ratingItems, Context context) {
        this.ratingItems = ratingItems;
        this.context = context;

    }

    @Override
    public RatingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RatingAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rating, parent, false));
    }

    @Override
    public void onBindViewHolder(RatingAdapter.ViewHolder holder, final int position) {
        final RatingItem ratingItem = ratingItems.get(position);
        holder.userName.setText(ratingItem.getName());
        holder.userComment.setText(ratingItem.getComment());
        holder.ratingBar.setRating((float)ratingItem.getStars());
    }

    public int getItemCount() {
        return ratingItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_rating_user_name)
        TextView userName;
        @BindView(R.id.tv_rating_user_comment)
        TextView userComment;
        @BindView(R.id.rb_rating_user_rate)
        MaterialRatingBar ratingBar;




        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
