package grand.healthplus.adapters.hplusadapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hplusmodels.BookingTimesItem;
import grand.healthplus.models.hplusmodels.BookingTimesResponse;
import grand.healthplus.models.hplusmodels.DaysTimesModel;
import grand.healthplus.models.hplusmodels.DoctorsItem;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class DaysTimesAdapter extends RecyclerView.Adapter<DaysTimesAdapter.ViewHolder> {
    private ArrayList<DaysTimesModel> daysTimesModels;
    Context context;
    DoctorsItem doctorsItem = null;
    int selectedPosition=-1;
    int patientId = -1;
    public DaysTimesAdapter(ArrayList<DaysTimesModel> daysTimesModels, Context context , DoctorsItem doctorsItem) {
        this.daysTimesModels = daysTimesModels;
        this.context = context;
        this.doctorsItem=doctorsItem;
    }

    public DaysTimesAdapter(ArrayList<DaysTimesModel> daysTimesModels, Context context , DoctorsItem doctorsItem , int patientId) {
        this.daysTimesModels = daysTimesModels;
        this.context = context;
        this.doctorsItem=doctorsItem;
        this.patientId=patientId;
    }

    @Override
    public DaysTimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DaysTimesAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_day, parent, false));
    }

    @Override
    public void onBindViewHolder(final DaysTimesAdapter.ViewHolder holder, final int position) {
        final DaysTimesModel daysTimesModel = daysTimesModels.get(position);
        holder.day.setText(daysTimesModel.getTimesItem().getDay()+" "+daysTimesModel.getTimesItem().getDate());


        if(daysTimesModels.get(position).getBookingTimesItems()!=null){
            holder.times.setVisibility(View.VISIBLE);
            setDetails(daysTimesModels.get(position).getBookingTimesItems(),  holder.times);
        }else {
            setDetails(null,  holder.times);
            holder.times.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDayTimes(position,holder.times);
            }
        });

    }
    public int getItemCount() {
        return daysTimesModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_booking_day_title)
        TextView day;
        @BindView(R.id.rcv_booking_day_times)
        RecyclerView times;
        @BindView(R.id.iv_booking_day_icon)
        ImageView arrow;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }




    private void getDayTimes(final int position, final RecyclerView timesRecycleView) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method", "booking_times");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(context));
            jsonObject.put("doctor_id", doctorsItem.getDoctorId());
            jsonObject.put("from", daysTimesModels.get(position).getTimesItem().getFromTime());
            jsonObject.put("to", daysTimesModels.get(position).getTimesItem().getToTime());
            jsonObject.put("waiting_time", daysTimesModels.get(position).getTimesItem().getWaitingTime());
            jsonObject.put("date", daysTimesModels.get(position).getTimesItem().getDate());
        } catch (Exception e) {
            e.getStackTrace();
        }


        ConnectionPresenter connectionPresenter = new ConnectionPresenter(context, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                BookingTimesResponse bookingTimesResponse = new Gson().fromJson(response.toString(), BookingTimesResponse.class);
                ArrayList<BookingTimesItem> bookingTimesItems =  new ArrayList<>(bookingTimesResponse.getBookingTimes());
                daysTimesModels.get(position).setBookingTimesItems(bookingTimesItems);
                selectedPosition = position;
                setDetails(bookingTimesItems,timesRecycleView);
                notifyDataSetChanged();
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true,false);
    }


    private void setDetails(ArrayList<BookingTimesItem> bookingTimesItems , RecyclerView timesRecycleView){
        if(bookingTimesItems==null){
            timesRecycleView.setAdapter(null);
        }
        else {

            DaysAdapter daysAdapter;

            if(patientId!=-1){
                daysAdapter = new DaysAdapter(bookingTimesItems, context, doctorsItem, daysTimesModels.get(selectedPosition),patientId);
            }else {
                daysAdapter =   new DaysAdapter(bookingTimesItems, context, doctorsItem, daysTimesModels.get(selectedPosition));
            }

            ViewOperations.setRVHorzontial(context, timesRecycleView);
            timesRecycleView.setAdapter(daysAdapter);
        }
    }

}
