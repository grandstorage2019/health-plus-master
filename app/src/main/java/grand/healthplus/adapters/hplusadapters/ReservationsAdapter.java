package grand.healthplus.adapters.hplusadapters;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.MapActivity;
import grand.healthplus.R;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.models.hplusmodels.ReservationsItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.ViewHolder> {
    private ArrayList<ReservationsItem> reservationsItems;
    Context context;

    public ReservationsAdapter(ArrayList<ReservationsItem> reservationsItems, Context context) {
        this.reservationsItems = reservationsItems;
        this.context = context;

    }

    @Override
    public ReservationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReservationsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_appointment, parent, false));
    }

    @Override
    public void onBindViewHolder(ReservationsAdapter.ViewHolder holder, final int position) {
        final ReservationsItem reservationsItem = reservationsItems.get(position);
        ConnectionPresenter.loadImage(holder.doctorImage, "doctor_profile/" + reservationsItem.getProfileImage());
        holder.doctorName.setText(reservationsItem.getTitle()+" "+reservationsItem.getDoctorFirstName() + " " + reservationsItem.getDoctorLastName());
        holder.time.setText(reservationsItem.getDate() + " " + reservationsItem.getTime());
        holder.clinicName.setText(reservationsItem.getClinicName());
        holder.address.setText(reservationsItem.getStreetName() + ", " + reservationsItem.getBlock() + ", " + reservationsItem.getBuildingNumber() + ", " + reservationsItem.getApartment() + ", " + reservationsItem.getFloor() + ", " + reservationsItem.getLandmark());
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 1000);
                } else {
                    MUT.makeCall(context, reservationsItem.getPhone());
                }
            }
        });

        holder.map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MapActivity.class);
                intent.putExtra("lat", reservationsItem.getLat());
                intent.putExtra("lang", reservationsItem.getLang());
                context.startActivity(intent);
            }
        });

        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reservationDialog(position);
            }
        });
    }


    private void deleteReservation(final int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "delete_reservation");
            jsonObject.put("id", reservationsItems.get(position).getReservationId());
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(context).getId());

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(context, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    Toast.makeText(context, "" + context.getResources().getString(R.string.deleted_successfully), Toast.LENGTH_SHORT).show();
                    reservationsItems.remove(position);
                    notifyDataSetChanged();

                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }


    public void reservationDialog(final int position) {
        final Dialog ratingDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar);
        ratingDialog.setCancelable(true);
        ratingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(context);
        View ratingView = inflater.inflate(R.layout.dialog_cancel_booking, null);
        ratingDialog.setContentView(ratingView);

        Button confirm = ratingView.findViewById(R.id.btn_cancel_booking_confirm);
        Button cancel = ratingView.findViewById(R.id.btn_cancel_booking_cancel);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteReservation(position);
                ratingDialog.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingDialog.cancel();
            }
        });

        ratingDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0e000000")));
        ratingDialog.show();
    }


    public int getItemCount() {
        return reservationsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_appointment_doctor_name)
        TextView doctorName;
        @BindView(R.id.tv_service_name)
        TextView time;
        @BindView(R.id.iv_item_appointment_doctor_image)
        ImageView doctorImage;
        @BindView(R.id.tv_item_appointment_clinic_name)
        TextView clinicName;
        @BindView(R.id.tv_item_appointment_address)
        TextView address;
        @BindView(R.id.ll_item_appointment_call)
        LinearLayout call;
        @BindView(R.id.ll_item_appointment_map)
        LinearLayout map;
        @BindView(R.id.ll_item_appointment_cancel)
        LinearLayout cancel;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
