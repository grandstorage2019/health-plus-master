package grand.healthplus.adapters.hplusadapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hplusmodels.HospitalsItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class HospitalsAdapter extends RecyclerView.Adapter<HospitalsAdapter.ViewHolder> {
    private ArrayList<HospitalsItem> hospitalsItems;
    Context context;
    RelativeLayout.LayoutParams imageLayoutParams,containerLayoutParams;
    int width = 0,hieght=0;

    public HospitalsAdapter(ArrayList<HospitalsItem> hospitalsItems, Context context) {
        this.hospitalsItems = hospitalsItems;
        this.context = context;
        width = ViewOperations.getScreenWidth(context);
        hieght=ViewOperations.getScreenHeight(context);
        //imageLayoutParams = new RelativeLayout.LayoutParams((int) (width / 2), hieght);
        containerLayoutParams = new RelativeLayout.LayoutParams((int) width - (width / 7), hieght);
    }

    @Override
    public HospitalsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HospitalsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hospital, parent, false));
    }

    @Override
    public void onBindViewHolder(HospitalsAdapter.ViewHolder holder, final int position) {
        final HospitalsItem hospitalsItem = hospitalsItems.get(position);
        holder.hospitalTitle.setText(hospitalsItem.getName());
        ConnectionPresenter.loadImage(holder.hospitalPhoto,"doctor_adverting/"+hospitalsItem.getImage());
        holder.hospitalContainer.setLayoutParams(containerLayoutParams);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startWebPage(context,hospitalsItems.get(position).getLink());
            }
        });

    }
    public int getItemCount() {
        return hospitalsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_hospital_photo)
        ImageView hospitalPhoto;
        @BindView(R.id.tv_hospital_title)
        TextView hospitalTitle;
        @BindView(R.id.rl_hpspital_container)
        RelativeLayout hospitalContainer;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
