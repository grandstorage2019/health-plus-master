package grand.healthplus.adapters.hplusadapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hplusactivities.DaysActivity;
import grand.healthplus.activities.hplusactivities.DoctorInformationActivity;
import grand.healthplus.models.hplusmodels.DaysTimesModel;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.models.hplusmodels.DoctorsItem;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class DoctorsAdapter extends RecyclerView.Adapter<DoctorsAdapter.ViewHolder> {
    private ArrayList<DoctorsItem> doctorsItems;
    Context context;


    public DoctorsAdapter(ArrayList<DoctorsItem> doctorsItems, Context context  ) {
        this.doctorsItems = doctorsItems;
        this.context = context;
    }

    @Override
    public DoctorsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DoctorsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doctor_details, parent, false));
    }

    @Override
    public void onBindViewHolder(DoctorsAdapter.ViewHolder holder, final int position) {
        final DoctorsItem doctorsItem = doctorsItems.get(position);
        holder.ratingBar.setRating((float)doctorsItem.getRate());
        holder.doctorClinicName.setText(doctorsItem.getTitle()+" "+StringUtils.capitalize(doctorsItem.getFirstName()+" "+doctorsItem.getLastName()));
        holder.clinicName.setText(StringUtils.capitalize(doctorsItem.getClinicName()));
        holder.aboutDoctor.setText(doctorsItem.getAbout());
        holder.price.setText(context.getResources().getString(R.string.detection_price)+": "+((int)doctorsItem.getPrice())+" "+context.getResources().getString(R.string.kd));
        holder.address.setText(doctorsItem.getAreaName()+", "+doctorsItem.getStreetName()+", "+doctorsItem.getBlock()+", "+doctorsItem.getBuildingNumber()+", "+doctorsItem.getFloor() +", "+doctorsItem.getApartment()+", "+doctorsItem.getLandmark());
        holder.specialization.setText(doctorsItem.getLevel());
        holder.avaliable.setText(context.getResources().getString(R.string.available_from)+" "+doctorsItem.getAvailableFrom());
        holder.ratingDetails.setText("(" + context.getResources().getString(R.string.of) + " " + doctorsItem.getRaters() + " " + context.getResources().getString(R.string.visited_doctor) + ")");

        if(doctorsItem.isFavourite()){
            holder.favourite.setImageResource(R.drawable.ic_heart_selected);
        }else {
            holder.favourite.setImageResource(R.drawable.ic_empty_heart);
        }

        holder.wait.setText(context.getResources().getString(R.string.detection_time)+": ");


        if(doctorsItem.getWaitingHours()>0 || doctorsItem.getWaitingMinutes() > 0) {
            if (doctorsItem.getWaitingHours() > 0) {
                holder.wait.setText(holder.wait.getText() + "" + doctorsItem.getWaitingHours() + " " + context.getResources().getString(R.string.hours) + " ");
            }
            if (doctorsItem.getWaitingMinutes() > 0) {
                holder.wait.setText(holder.wait.getText() + "" + doctorsItem.getWaitingMinutes() + " " + context.getResources().getString(R.string.minutes));
            }
        }else {
            holder.wait.setText(context.getResources().getString(R.string.book_first));
        }
        ConnectionPresenter.loadImage(holder.doctorImage,"doctor_profile/"+doctorsItem.getProfileImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(context, DoctorInformationActivity.class);
                intent.putExtra("doctorDetails",doctorsItem);
                context.startActivity(intent);*/
            }
        });

        holder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SharedPreferenceHelper.isLogined(context)) {
                    addDeleteFavourite(position, !doctorsItem.isFavourite());
                }
            }
        });

        holder.bookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DoctorInformationActivity.class);
                intent.putExtra("doctorDetails",doctorsItem);
                context.startActivity(intent);
            }
        });

    }


    private void addDeleteFavourite(final int position, final boolean isAdd) {

        JSONObject jsonObject = new JSONObject();
        try {
            String method =isAdd?"add_favourite":"delete_favourite";
            jsonObject.put("method",method);
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(context).getId());
            jsonObject.put("doctor_id", doctorsItems.get(position).getDoctorId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(context, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    doctorsItems.get(position).setFavourite(isAdd);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }

    public int getItemCount() {
        return doctorsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_health_care_name)
        TextView doctorClinicName;
        @BindView(R.id.tv_health_care_clinic_name)
        TextView clinicName;
        @BindView(R.id.tv_doctor_details_about)
        TextView aboutDoctor;
        @BindView(R.id.tv_doctor_details_specialization)
        TextView specialization;
        @BindView(R.id.tv_doctor_details_price)
        TextView price;
        @BindView(R.id.tv_doctor_details_wait)
        TextView wait;
        @BindView(R.id.tv_doctor_details_address)
        TextView address;
        @BindView(R.id.tv_doctor_details_avaliable)
        TextView avaliable;
        @BindView(R.id.iv_doctor_info_rating_details)
        TextView ratingDetails;
        @BindView(R.id.rb_doctor_info_rating)
        MaterialRatingBar ratingBar;
        @BindView(R.id.iv_doctor_details_favourite)
        ImageView favourite;
        @BindView(R.id.iv_health_care_photo)
        ImageView doctorImage;
        @BindView(R.id.btn_doctor_details_book)
        Button bookBtn;




        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
