package grand.healthplus.adapters.hplusadapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hplusactivities.ConfirmBookingActivity;
import grand.healthplus.models.hplusmodels.BaseResponse;
import grand.healthplus.models.hplusmodels.BookingTimesItem;
import grand.healthplus.models.hplusmodels.DaysTimesModel;
import grand.healthplus.models.hplusmodels.DoctorsItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.ViewHolder> {
    private ArrayList<BookingTimesItem> bookingTimesItems;
    private DaysTimesModel daysTimesModel;
    Context context;
    private int patientId = -1;
    private DoctorsItem doctorsItem;
    String time = "",language="";
    public DaysAdapter(ArrayList<BookingTimesItem> bookingTimesItems, Context context, DoctorsItem doctorsItem, DaysTimesModel daysTimesModel) {
        this.bookingTimesItems = bookingTimesItems;
        this.context = context;
        this.doctorsItem = doctorsItem;
        this.daysTimesModel = daysTimesModel;
        this.language = SharedPreferenceHelper.getCurrentLanguage(context);
    }

    public DaysAdapter(ArrayList<BookingTimesItem> bookingTimesItems, Context context, DoctorsItem doctorsItem, DaysTimesModel daysTimesModel, int patientId) {
        this.bookingTimesItems = bookingTimesItems;
        this.context = context;
        this.doctorsItem = doctorsItem;
        this.daysTimesModel = daysTimesModel;
        this.patientId = patientId;
        this.language = SharedPreferenceHelper.getCurrentLanguage(context);
    }

    @Override
    public DaysAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DaysAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_day, parent, false));
    }

    @Override
    public void onBindViewHolder(final DaysAdapter.ViewHolder holder, final int position) {
        final BookingTimesItem bookingTimesItem = bookingTimesItems.get(position);

        time = bookingTimesItem.getTime();

        if (language.equals("ar")) {
            String pmOrAm = ""+time.charAt(time.length()-2)+time.charAt(time.length()-1);

            if(pmOrAm.equals("AM")){
                time = time.replace(pmOrAm,"ص");
            }else {
                time = time.replace(pmOrAm,"م");
            }

        }else {
            time = bookingTimesItem.getTime();
        }

        holder.day.setText(time);


        if (bookingTimesItem.isIsBooked()) {
            holder.day.setAlpha(0.7f);
        } else {
            holder.day.setAlpha(1f);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!bookingTimesItem.isIsBooked()) {
                    if (patientId != -1) {
                        patientDialog(position,daysTimesModel.getTimesItem().getDay());
                    } else {
                        Intent intent = new Intent(context, ConfirmBookingActivity.class);
                        intent.putExtra("doctorDetails", doctorsItem);
                        intent.putExtra("time", bookingTimesItems.get(position).getTime());
                        intent.putExtra("dayDetails", daysTimesModel);

                        context.startActivity(intent);

                    }
                } else {
                    MUT.lToast(context, context.getResources().getString(R.string.booked_before));
                }
            }
        });
    }



    public  void patientDialog(final int position ,final String date) {
        final Dialog ratingDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar);
        ratingDialog.setCancelable(true);
        ratingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(context);
        View ratingView = inflater.inflate(R.layout.dialog_confirm_booking, null);


        ratingDialog.setContentView(ratingView);

        TextView confirmTextView = ratingView.findViewById(R.id.tv_confirm_booking_details);
        confirmTextView.setText(context.getResources().getString(R.string.want_confirm_for_day)+" "+date+" "+context.getResources().getString(R.string.want_confirm_at)+" "+bookingTimesItems.get(position).getTime());
        Button confirmBtn = ratingView.findViewById(R.id.btn_confirm_booking_confirm);
        Button cancelBtn = ratingView.findViewById(R.id.btn_confirm_booking_cancel);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmBooking(position,date);
                ratingDialog.cancel();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingDialog.cancel();
            }
        });

        ratingDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0e000000")));
        ratingDialog.show();
    }

    private void confirmBooking(final int position ,String date) {




        JSONObject sentData = new JSONObject();

        try {
            sentData.put("method", "confirm_booking");
            sentData.put("doctor_id", doctorsItem.getDoctorId());
            sentData.put("date", daysTimesModel.getTimesItem().getDate());
            sentData.put("time", bookingTimesItems.get(position).getTime());
            sentData.put("type", daysTimesModel.getTimesItem().getType());
            sentData.put("patient_id", patientId);

            System.out.println("data "+sentData.toString());


        } catch (Exception e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(context, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                BaseResponse baseResponse = new Gson().fromJson(response.toString(), BaseResponse.class);
                if (baseResponse.getState() == ConnectionPresenter.SUCCESS) {
                    MUT.successDialog(context);

                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(sentData, true, false);
    }


    public int getItemCount() {
        return bookingTimesItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_item_day_book_day)
        TextView day;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
