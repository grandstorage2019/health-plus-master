package grand.healthplus.adapters.hplusadapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hplusactivities.ConfirmBookingActivity;
import grand.healthplus.activities.hplusactivities.DaysActivity;
import grand.healthplus.models.hplusmodels.BaseResponse;
import grand.healthplus.models.hplusmodels.DaysTimesModel;
import grand.healthplus.models.hplusmodels.DoctorsItem;
import grand.healthplus.models.hplusmodels.TimesItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class TimesAdapter extends RecyclerView.Adapter<TimesAdapter.ViewHolder> {
    private ArrayList<TimesItem> timesItems;
    Context context;
    DoctorsItem doctorsItem = null;
    String language = "";
    String dayName = "", fromTime = "", toTime = "";

    public TimesAdapter(ArrayList<TimesItem> timesItems, Context context, DoctorsItem doctorsItem) {
        this.timesItems = timesItems;
        this.context = context;
        this.doctorsItem = doctorsItem;
        this.language = SharedPreferenceHelper.getCurrentLanguage(context);
    }

    @Override
    public TimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TimesAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time, parent, false));
    }

    @Override
    public void onBindViewHolder(TimesAdapter.ViewHolder holder, final int position) {
        final TimesItem timesItem = timesItems.get(position);
        fromTime = timesItem.getFromTime();
        toTime = timesItem.getToTime();

        if (language.equals("ar")) {
            if (timesItem.getDay().equals("Saturday")) {
                dayName = "السبت";
            } else if (timesItem.getDay().equals("Sunday")) {
                dayName = "الاحد";
            } else if (timesItem.getDay().equals("Monday")) {
                dayName = "الاثنين";
            } else if (timesItem.getDay().equals("Tuesday")) {
                dayName = "الثلاثاء";
            } else if (timesItem.getDay().equals("Wednesday")) {
                dayName = "الاربعاء";
            } else if (timesItem.getDay().equals("Thursday")) {
                dayName = "الخميس";
            } else if (timesItem.getDay().equals("Friday")) {
                dayName = "الجمعة";
            }
            fromTime = fromTime.replaceAll(""+fromTime.charAt(fromTime.length()-2)+fromTime.charAt(fromTime.length()-1),"ص");
            toTime = toTime.replaceAll(""+toTime.charAt(toTime.length()-2)+toTime.charAt(toTime.length()-1), "م");
        } else {
            dayName = timesItem.getDay();
        }


        holder.day.setText(dayName + "\n" + timesItem.getDate());
        holder.duration.setText(fromTime + " " + context.getResources().getString(R.string.to) + "\n" + toTime);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (timesItem == null || timesItem.getType().equals("specific time")) {
                    Intent intent = new Intent(context, DaysActivity.class);
                    intent.putExtra("doctorId", doctorsItem);
                    DaysTimesModel daysTimesModel = new DaysTimesModel();
                    daysTimesModel.setTimesItem(timesItem);
                    intent.putExtra("daysTimes", daysTimesModel);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, ConfirmBookingActivity.class);
                    intent.putExtra("doctorDetails", doctorsItem);
                    DaysTimesModel daysTimesModel = new DaysTimesModel();
                    daysTimesModel.setTimesItem(timesItem);
                    intent.putExtra("dayDetails", daysTimesModel);
                    context.startActivity(intent);
                }
            }
        });
    }

    public int getItemCount() {
        return timesItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_time_day)
        TextView day;
        @BindView(R.id.tv_time_duration)
        TextView duration;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
