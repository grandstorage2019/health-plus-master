package grand.healthplus.adapters.hplusadapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hplusmodels.HospitalsItem;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class BookingDaysAdapter extends RecyclerView.Adapter<BookingDaysAdapter.ViewHolder> {
    private ArrayList<HospitalsItem> hospitalsItems;
    Context context;


    public BookingDaysAdapter(ArrayList<HospitalsItem> hospitalsItems, Context context) {
        this.hospitalsItems = hospitalsItems;
        this.context = context;
    }

    @Override
    public BookingDaysAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BookingDaysAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_day, parent, false));
    }

    @Override
    public void onBindViewHolder(BookingDaysAdapter.ViewHolder holder, final int position) {
        final HospitalsItem hospitalsItem = hospitalsItems.get(position);
        holder.hospitalTitle.setText(hospitalsItem.getName());
        ConnectionPresenter.loadImage(holder.hospitalPhoto,"hospitals/"+hospitalsItem.getImage());

    }
    public int getItemCount() {
        return hospitalsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_hospital_photo)
        ImageView hospitalPhoto;
        @BindView(R.id.tv_hospital_title)
        TextView hospitalTitle;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
