package grand.healthplus.trash;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;


public class NotificationsFragment extends Fragment {
    @BindView(R.id.rv_search_result_collection)
    RecyclerView doctorItems;

    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_search_result, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }


}
