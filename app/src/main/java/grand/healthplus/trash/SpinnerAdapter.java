package grand.healthplus.trash;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.trashmodels.SpinnerModel;



public class SpinnerAdapter extends RecyclerView.Adapter<SpinnerAdapter.ViewHolder> {
    private ArrayList<SpinnerModel> items;
    Context context;
    int selectedItem = -1;
    SpinnerListener spinnerListener;

    public SpinnerAdapter(ArrayList<SpinnerModel> items, Context context , SpinnerListener spinnerListener) {
        this.items = items;
        this.context = context;
        this.spinnerListener = spinnerListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_spinner, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final SpinnerModel spinnerModel = items.get(position);
        holder.title.setText(spinnerModel.getTitle() + "");

        if(selectedItem==position){
            holder.spinnerHolder.setBackgroundResource(R.drawable.multi_color_square_background);
            holder.title.setTextColor(context.getResources().getColor(R.color.colorWhite));
        }else {
            holder.spinnerHolder.setBackgroundResource(R.color.colorWhite);
            holder.title.setTextColor(context.getResources().getColor(R.color.colorGrayLevel2));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedItem = position;
                spinnerListener.onItemClicked(position);
                notifyDataSetChanged();
            }
        });

    }
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_main_title)
        TextView title;
        @BindView(R.id.rl_spinner_holder)
        RelativeLayout spinnerHolder;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public int getSelectedItem(){
        return selectedItem;
    }


}
