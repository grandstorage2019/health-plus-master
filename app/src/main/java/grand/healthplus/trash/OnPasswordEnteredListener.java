package grand.healthplus.trash;

/**
 * Created by sameh on 1/14/18.
 */

public interface OnPasswordEnteredListener {

    void onPasswordEntered(String password);
}
