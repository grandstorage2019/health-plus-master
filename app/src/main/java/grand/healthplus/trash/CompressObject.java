package grand.healthplus.trash;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

public class CompressObject 
{
	private Bitmap image=null;
	private ByteArrayOutputStream byteStream=null;
	public Bitmap getImage() {
		return image;
	}



	public void setImage(Bitmap image) {
		this.image = image;
	}

	public ByteArrayOutputStream getByteStream() {
		return byteStream;
	}

	public void setByteStream(ByteArrayOutputStream byteStream) {
		this.byteStream = byteStream;
	}
	

}
