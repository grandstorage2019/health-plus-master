package grand.healthplus.trash;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import grand.healthplus.R;
import grand.healthplus.activities.hplusactivities.HPlusMainActivity;
import grand.healthplus.models.trashmodels.CountryModel;
import grand.healthplus.models.trashmodels.CountryResponse;
import grand.healthplus.models.trashmodels.SignUpResponse;
import grand.healthplus.models.trashmodels.SpecializationItem;
import grand.healthplus.models.trashmodels.SpecializationResponse;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.utils.Check;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.utils.WebServiceConstants;

public class DocotrSignUpActivity extends AppCompatActivity {

    @BindView(R.id.et_doctor_sign_up_first_name)
    EditText firstNameEditText;
    @BindView(R.id.et_doctor_sign_up_last_name)
    EditText lastNameEditText;
    @BindView(R.id.et_doctor_sign_up_password)
    EditText passwordEditText;
    @BindView(R.id.et_doctor_sign_up_cpassword)
    EditText cpasswordEditText;
    @BindView(R.id.et_doctor_sign_up_email)
    EditText emailEditText;
    @BindView(R.id.et_doctor_sign_up_mobile)
    EditText mobileEditText;
    @BindView(R.id.et_doctor_sign_up_birth_date)
    EditText birthDayEditText;
    @BindView(R.id.et_doctor_sign_up_gender)
    EditText genderEditText;
    @BindView(R.id.rl_doctor_sign_up_specialty)
    RelativeLayout specialtySpinner;
    @BindView(R.id.tv_doctor_sign_up_specialty)
    TextView specialtyText;
    @BindView(R.id.rl_doctor_sign_up_country)
    RelativeLayout countrySpinner;
    @BindView(R.id.tv_doctor_sign_up_country)
    TextView countryText;
    @BindView(R.id.btn_doctor_sign_up_enter)
    Button enterBtn;

    int selectedCountry = -1,selectedSpecialty=-1;
    Dialog countryDialog = null, genderDialog = null , specialtyDialog=null;
    ArrayList<SpinnerModel> countryItems,genderItems,specialtyItems;
    String language = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_sign_up);
        ButterKnife.bind(DocotrSignUpActivity.this);
        setEvents();
        language = SharedPreferenceHelper.getCurrentLanguage(this);
    }

    private boolean validate() {
        if (Check.isEmpty(lastNameEditText.getText() + "") || Check.isEmpty(firstNameEditText.getText() + "") || Check.isEmpty(passwordEditText.getText() + "")
                || Check.isEmpty(cpasswordEditText.getText() + "")
                || Check.isEmpty(mobileEditText.getText() + "") || Check.isEmpty(birthDayEditText.getText() + "")
                || Check.isEmpty(genderEditText.getText() + "") || Check.isEmpty(emailEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.fill_data));
            return false;
        } else if (!Check.isEqual(passwordEditText.getText() + "", cpasswordEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.passwords_not_matches));
            return false;
        } else if (!Check.isMail(emailEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.wrong_email));
            return false;
        } else if (selectedCountry == -1) {
            MUT.lToast(this, getResources().getString(R.string.select_country));
            return false;
        }

        return true;
    }


    private void setEvents() {
        enterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    signUp();
                }
            }
        });

        countrySpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (countryDialog != null) {
                    countryDialog.show();
                } else {

                    getCountries();
                }
            }
        });

        specialtySpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (specialtyDialog != null) {
                    specialtyDialog.show();
                } else {
                    getSpecializations();
                }
            }
        });

        birthDayEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });

        genderEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGenderSpinner();
            }
        });
    }

    private void getCountries() {
        countryItems = new ArrayList<>();
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                CountryResponse countryResponse = new Gson().fromJson(response.toString(), CountryResponse.class);
                List<CountryModel> data = countryResponse.getCountryItems();

                for (int i = 0; i < data.size(); i++) {
                    countryItems.add(new SpinnerModel(i, data.get(i).getName(language)));
                }


                countryDialog = ViewOperations.setSpinnerDialog(DocotrSignUpActivity.this, countryItems, getResources().getString(R.string.countries), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedCountry = position;
                        countryText.setText(countryItems.get(position).getTitle());
                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
       // connectionPresenter.connect(null, true, Request.Method.GET, WebServiceConstants.COUNTRIES);
    }


    private void getSpecializations() {
        specialtyItems = new ArrayList<>();
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                SpecializationResponse specializationResponse = new Gson().fromJson(response.toString(), SpecializationResponse.class);
                List<SpecializationItem> specializationItems = specializationResponse.getData();

                for (int i = 0; i < specializationItems.size(); i++) {
                    specialtyItems.add(new SpinnerModel( specializationItems.get(i).getId(), specializationItems.get(i).getName(language)));
                }


                specialtyDialog = ViewOperations.setSpinnerDialog(DocotrSignUpActivity.this, specialtyItems, getResources().getString(R.string.specialty), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedSpecialty = specialtyItems.get(position).getId();
                        specialtyText.setText(specialtyItems.get(position).getTitle());
                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        //connectionPresenter.connect(null, true, Request.Method.GET, WebServiceConstants.SPECIALIZATION_LIST);
    }

    private void signUp() {
        if (validate()) {
            JSONObject sentData = new JSONObject();

            try {
                sentData.put("fname_en", "" + firstNameEditText.getText());
                sentData.put("lname_en", "" + lastNameEditText.getText());
                sentData.put("email", "" + emailEditText.getText());
                sentData.put("password", "" + passwordEditText.getText());
                sentData.put("phone", "" + mobileEditText.getText());
                sentData.put("gender", "" + genderEditText.getText());
                sentData.put("birth_day", "" + birthDayEditText.getText());
                sentData.put("country_id", selectedCountry);
                sentData.put("specialization_id", selectedSpecialty);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {

                    SignUpResponse signUpResponse = new Gson().fromJson(response.toString(), SignUpResponse.class);

                    if (signUpResponse.getStatus().equals(ConnectionPresenter.SUCCESS)) {
                        MUT.startActivity(DocotrSignUpActivity.this, HPlusMainActivity.class);
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }

            });
           // connectionPresenter.connect(sentData, true, Request.Method.PATCH, WebServiceConstants.SIGN_UP + SharedPreferenceHelper.getUserId(this));
        }
    }


    private void selectDate() {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH) + 1;
        final DatePickerDialog datePickerDialog = new DatePickerDialog(DocotrSignUpActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                String selectedDate = i + "-" + (++i1) + "-" + i2;
                birthDayEditText.setText(selectedDate);
            }
        }, year, month, 0);

        datePickerDialog.show();
    }


    private void setGenderSpinner() {
        if (genderDialog == null) {
            genderItems = new ArrayList<>();
            genderItems.add(new SpinnerModel(0, getResources().getString(R.string.male)));
            genderItems.add(new SpinnerModel(1, getResources().getString(R.string.female)));
            genderDialog = ViewOperations.setSpinnerDialog(DocotrSignUpActivity.this, genderItems, getResources().getString(R.string.gender), new SpinnerListener() {
                @Override
                public void onItemClicked(int position) {
                    genderEditText.setText(genderItems.get(position).getTitle());
                }
            });
        } else {
            genderDialog.show();
        }
    }


}
