package grand.healthplus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    MapView mapView;
    GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        mapView = findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(getIntent().getDoubleExtra("lat", 0.0), getIntent().getDoubleExtra("lang", 0.0))).bearing(45).tilt(90).zoom(googleMap.getCameraPosition().zoom).build();
        MarkerOptions marker = new MarkerOptions().position(new LatLng(getIntent().getDoubleExtra("lat", 0.0), getIntent().getDoubleExtra("lang", 0.0)));
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.addMarker(marker);

       // Toast.makeText(this,  getIntent().getDoubleExtra("lat", 0.0)+" "+getIntent().getDoubleExtra("lang", 0.0), Toast.LENGTH_SHORT).show();

       // map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        float zoomLevel = 16.0f; //This goes up to 21
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(getIntent().getDoubleExtra("lat", 0.0),getIntent().getDoubleExtra("lang", 0.0)), zoomLevel));

    }

    public void goBack(View view) {
        finish();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
