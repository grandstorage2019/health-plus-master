package grand.healthplus.utils;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import grand.healthplus.R;
import grand.healthplus.SimpleToolBarListener;
import grand.healthplus.trash.SpinnerAdapter;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.models.trashmodels.SpinnerModel;


public class ViewOperations {


    public static void setGlideImage(Context context, int image, ImageView imageView) {
        Glide.with(context).load(image).into(imageView);
    }

    public static void setRVHorzontial(Context context, RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
    }


    public static void setRVHVertical(Context context, RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public static LinearLayoutManager setRVHVertical2(Context context, RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        return linearLayoutManager;
    }


    public static void setRVHVerticalGrid(Context context, RecyclerView recyclerView, int numOfColumns) {
        recyclerView.setLayoutManager(new GridLayoutManager(context, numOfColumns));
    }


    public static void setRelativeViewSize(View view, int width, int height) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
        view.setLayoutParams(layoutParams);
    }

    public static void setLinearViewSize(View view, int width, int height) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
        view.setLayoutParams(layoutParams);
    }


    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }


    public static int getScreenWidth(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;

    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }

    public static void setSimpleToolBar(final Context context, String title, final SimpleToolBarListener simpleToolBarListener) {
        ImageView backImageView = ((Activity) context).findViewById(R.id.iv_simple_tool_bar_back);
        TextView titleTextView = ((Activity) context).findViewById(R.id.tv_simple_tool_bar_title);
        TextView skipTextView = ((Activity) context).findViewById(R.id.tv_simple_tool_bar_skip);
        titleTextView.setText(title);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) context).finish();
                simpleToolBarListener.onBackClicked();
            }
        });

        skipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simpleToolBarListener.onSkipClicked();
            }
        });


    }


    public static Dialog setSpinnerDialog(final Context context, final ArrayList<SpinnerModel> items, String title, final SpinnerListener spinnerListener) {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        LayoutInflater inflater = LayoutInflater.from(context);
        View spinnerView = inflater.inflate(R.layout.dialog_spinner, null);
        RecyclerView spinnerItemsList = spinnerView.findViewById(R.id.rcv_spinner_items);
        Button nextBtn = spinnerView.findViewById(R.id.btn_spinner_next);
        ImageView backBtn = spinnerView.findViewById(R.id.iv_spinner_back);
        TextView titleTextView = spinnerView.findViewById(R.id.tv_main_title);

        titleTextView.setText(title);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });



        if(SharedPreferenceHelper.getCurrentLanguage(context).equals("ar")){
            backBtn.setRotation(180);
        }




        ViewOperations.setRVHVertical(context, spinnerItemsList);
        final SpinnerAdapter spinnerAdapter = new SpinnerAdapter(items, context, new SpinnerListener() {
            @Override
            public void onItemClicked(int position) {
                if (items != null  ) {
                    spinnerListener.onItemClicked(position);
                    dialog.cancel();
                } else {
                    MUT.lToast(context, context.getResources().getString(R.string.select_to_continue));
                }
            }
        });
        spinnerItemsList.setAdapter(spinnerAdapter);

        nextBtn.setVisibility(View.GONE);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (items != null && spinnerAdapter.getSelectedItem() > -1) {
                    spinnerListener.onItemClicked(spinnerAdapter.getSelectedItem());
                    dialog.cancel();
                } else {
                    MUT.lToast(context, context.getResources().getString(R.string.select_to_continue));
                }
            }
        });

        dialog.setContentView(spinnerView);
        dialog.show();

        return dialog;
    }

}
