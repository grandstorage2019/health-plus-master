package grand.healthplus.utils;

import com.google.gson.annotations.SerializedName;


public class AddLicenseResponse{

	@SerializedName("data")
	private String data;

	@SerializedName("status")
	private String status;

	public void setData(String data){
		this.data = data;
	}

	public String getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AddLicenseResponse{" + 
			"data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}