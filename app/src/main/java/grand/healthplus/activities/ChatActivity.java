package grand.healthplus.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hcareactivities.HCareMainActivity;
import grand.healthplus.adapters.hcareadapters.ChatAdapter;
import grand.healthplus.adapters.hcareadapters.ChattersAdapter;
import grand.healthplus.models.hcaremodels.ChatResponse;
import grand.healthplus.models.hcaremodels.ChattersItem;
import grand.healthplus.models.hcaremodels.ChattersResponse;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.models.hplusmodels.DoctorsItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import me.gujun.android.taggroup.TagGroup;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.rv_chat_collection)
    RecyclerView chatRecycleView;
    @BindView(R.id.et_chat_mesasage)
    EditText messageEditText;
    @BindView(R.id.iv_chat_send)
    ImageView sendImageView;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    ChattersItem chattersItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        chattersItem = ((ChattersItem) getIntent().getSerializableExtra("chattersItemDetails"));
        getChat();

        MUT.setSelectedLanguage(this);
        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }

        setEvents();

    }

    private void setEvents() {
        sendImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPreferenceHelper.isLogined(ChatActivity.this)) {
                    if (messageEditText.getText().toString().length() < 1) {
                        Toast.makeText(ChatActivity.this, "" + getResources().getString(R.string.type_something), Toast.LENGTH_SHORT).show();
                    } else {
                        sendMessage();
                    }
                }

            }
        });
    }


    public void goBack(View view) {
        finish();
    }

    private void getChat() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "chat");
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(ChatActivity.this).getId());
            jsonObject.put("doctor_id", chattersItem.getDoctorId());
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ChatResponse chatResponse = new Gson().fromJson(response.toString(), ChatResponse.class);
                if (chatResponse.getState() == ConnectionPresenter.SUCCESS) {
                    ChatAdapter timesAdapter = new ChatAdapter(new ArrayList<>(chatResponse.getChat()), ChatActivity.this, chattersItem.getProfileImage());
                    ViewOperations.setRVHVertical(ChatActivity.this, chatRecycleView);
                    chatRecycleView.setAdapter(timesAdapter);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }

    private void sendMessage() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "send_message");
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(ChatActivity.this).getId());
            jsonObject.put("doctor_id", chattersItem.getDoctorId());
            jsonObject.put("massage", messageEditText.getText() + "");
            jsonObject.put("who", "patient");
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    messageEditText.setText("");
                    getChat();
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }

}