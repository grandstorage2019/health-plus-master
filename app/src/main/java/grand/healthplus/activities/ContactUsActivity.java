package grand.healthplus.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.Check;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ContactUsActivity extends AppCompatActivity {

    @BindView(R.id.tv_contact_us_call)
    TextView callTextView;
    @BindView(R.id.et_contact_us_message)
    EditText messageEditText;
    @BindView(R.id.btn_contact_us_send)
    Button sendBtn;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    String phoneNumber = "+96595505014";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);

        MUT.setSelectedLanguage(this);
        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }

        setEvents();

    }


    private void setEvents() {
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Check.isEmpty(messageEditText.getText() + "")) {
                    Toast.makeText(ContactUsActivity.this, "" + getResources().getString(R.string.fill_data), Toast.LENGTH_SHORT).show();
                } else {
                    sendMessage();
                }
            }
        });

        callTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ContactUsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ContactUsActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 1084);
                } else {
                    try {
                        MUT.makeCall(ContactUsActivity.this, phoneNumber);
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                MUT.makeCall(ContactUsActivity.this, phoneNumber);
            } catch (Exception e) {
                e.getStackTrace();
            }
        } else {
            ActivityCompat.requestPermissions(ContactUsActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 1084);
        }

    }

    private void sendMessage() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "send_message");
            jsonObject.put("message", messageEditText.getText());

            if (getIntent().getBooleanExtra("isDoctor", false)) {
                jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            } else {
                jsonObject.put("patient_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            }

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(ContactUsActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);

                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    Toast.makeText(ContactUsActivity.this, "" + getResources().getString(R.string.sent_successfully), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onRequestError(Object error) {
            }

        });


        connectionPresenter.connect(jsonObject, true, true);
    }

    public void goBack(View view) {
        finish();
    }


}