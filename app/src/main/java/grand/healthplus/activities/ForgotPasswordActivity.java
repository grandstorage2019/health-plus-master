package grand.healthplus.activities;


import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hcaremodels.registeration.SignUpResponse;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class ForgotPasswordActivity extends AppCompatActivity {

    @BindView(R.id.et_forgot_password_email)
    EditText emailEditText;
    @BindView(R.id.btn_forgot_password_reset_password)
    Button resetPasswordBtn;

    @BindView(R.id.iv_spinner_back)
    ImageView backImageView;

    String generatedCode="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(ForgotPasswordActivity.this);

        MUT.setSelectedLanguage(this);
        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }

        setEvents();
    }

    private void setEvents(){
        resetPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCode();
            }
        });
    }

    private void sendCode() {

        JSONObject params = new JSONObject();
        try {
            generatedCode = MUT.GenRan();
            params.put("method", "send_code");
            params.put("email", emailEditText.getText() + "");
            params.put("code",generatedCode);
        } catch (Exception e) {
            e.getStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(ForgotPasswordActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                try {
                    DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                    if(defaultResponse.getState() == ConnectionPresenter.SUCCESS){
                        Intent intent = new Intent(ForgotPasswordActivity.this,ChangePasswordActivity.class);
                        intent.putExtra("code",generatedCode);

                        System.out.println("Code "+generatedCode);

                        intent.putExtra("email",emailEditText.getText()+"");
                         startActivity(intent);
                    }
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        });
        connectionPresenter.connect(params, true, false);


    }



    public void goBack(View view) {
        finish();
    }

}
