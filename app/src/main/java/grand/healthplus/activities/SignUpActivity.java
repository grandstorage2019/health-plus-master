package grand.healthplus.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hcaremodels.registeration.SignUpResponse;
import grand.healthplus.models.hcaremodels.registeration.UserItem;
import grand.healthplus.models.hplusmodels.CountriesItem;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.utils.Check;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;

public class SignUpActivity extends AppCompatActivity {
    public static final String SENDER_ID = "306498002272";
    String googleId = "";

    @BindView(R.id.et_user_sign_up_user_name)
    EditText userNameEditText;
    @BindView(R.id.et_user_sign_up_password)
    EditText passwordEditText;
    @BindView(R.id.et_user_sign_up_cpassword)
    EditText cpasswordEditText;
    @BindView(R.id.et_user_sign_up_email)
    EditText emailEditText;
    @BindView(R.id.et_user_sign_up_mobile)
    EditText mobileEditText;
    @BindView(R.id.et_user_sign_up_birth_date)
    EditText birthDayEditText;
    @BindView(R.id.et_user_sign_up_gender)
    EditText genderEditText;
    @BindView(R.id.rl_user_sign_up_country)
    RelativeLayout countrySpinner;
    @BindView(R.id.tv_user_sign_up_country)
    TextView countryText;
    @BindView(R.id.btn_user_sign_up_enter)
    Button enterBtn;
    @BindView(R.id.tv_sign_up_title)
    TextView titleTextView;
    @BindView(R.id.rl_have_account)
    RelativeLayout haveAccountLayout;

    int selectedCountry = -1;
    Dialog countryDialog = null, genderDialog = null;
    ArrayList<SpinnerModel> countryItems;
    ArrayList<SpinnerModel> genderItems;
    String language = "";
    boolean isEdit = false;
    UserItem userItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_sign_up);
        ButterKnife.bind(SignUpActivity.this);

        language = SharedPreferenceHelper.getCurrentLanguage(this);


        isEdit = getIntent().getBooleanExtra("isEdit", false);

        if (isEdit) {
            enterBtn.setText(getResources().getString(R.string.update));
            titleTextView.setText(getResources().getString(R.string.edit_profile));
            haveAccountLayout.setVisibility(View.GONE);
            setUserDetails();
        }

        setEvents();
    }

    private boolean validate() {
        if (Check.isEmpty(userNameEditText.getText() + "") || Check.isEmpty(passwordEditText.getText() + "")
                || Check.isEmpty(cpasswordEditText.getText() + "")
                || Check.isEmpty(mobileEditText.getText() + "") || Check.isEmpty(birthDayEditText.getText() + "")
                || Check.isEmpty(genderEditText.getText() + "") || Check.isEmpty(emailEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.fill_data));
            return false;
        } else if (!Check.isEqual(passwordEditText.getText() + "", cpasswordEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.passwords_not_matches));
            return false;
        } else if (!Check.isMail(emailEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.wrong_email));
            return false;
        } else if (selectedCountry == -1) {
            MUT.lToast(this, getResources().getString(R.string.select_country));
            return false;
        }

        return true;
    }


    private void setEvents() {
        enterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    getGCMTokenThenLogin();
                }
            }
        });

        haveAccountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(SignUpActivity.this, SignInActivity.class);
            }
        });

        countrySpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (countryDialog != null) {
                    countryDialog.show();
                } else {

                    getCountries();
                }
            }
        });

        birthDayEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });

        genderEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGenderSpinner();
            }
        });
    }

    private void getCountries() {
        countryItems = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "countries");
            jsonObject.put("language", language);
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(SignUpActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                grand.healthplus.models.hplusmodels.CountryResponse countryResponse = new Gson().fromJson(response.toString(), grand.healthplus.models.hplusmodels.CountryResponse.class);
                List<CountriesItem> data = countryResponse.getCountries();

                for (int i = 0; i < data.size(); i++) {
                    countryItems.add(new SpinnerModel(data.get(i).getId(), data.get(i).getName()));
                }


                countryDialog = ViewOperations.setSpinnerDialog(SignUpActivity.this, countryItems, getResources().getString(R.string.countries), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedCountry = countryItems.get(position).getId();
                        countryText.setText(countryItems.get(position).getTitle());

                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }

    private void signUp() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1000);
        } else {
            goSignUp();
        }

    }

    private void goSignUp() {
        if (validate()) {
            JSONObject sentData = new JSONObject();
            try {
                sentData.put("method", "register");
                if (isEdit) {
                    sentData.put("method", "update");
                    sentData.put("id", userItem.getId());
                }
                sentData.put("username", "" + userNameEditText.getText());
                sentData.put("email", "" + emailEditText.getText());
                sentData.put("password", "" + passwordEditText.getText());
                sentData.put("phone", "" + mobileEditText.getText());

                if (gender == 0) {
                    sentData.put("gender", "male");
                } else {
                    sentData.put("gender", "female");
                }


                sentData.put("birth_day", "" + birthDayEditText.getText());
                sentData.put("google_id", googleId + "");
                sentData.put("device_id", MUT.getDeviceId(SignUpActivity.this));
                sentData.put("country_id", selectedCountry);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    loadingBar.cancel();
                    SignUpResponse signUpResponse = new Gson().fromJson(response.toString(), SignUpResponse.class);
                    if (signUpResponse.getState() == ConnectionPresenter.SUCCESS) {
                        if (isEdit) {
                            MUT.lToast(SignUpActivity.this, getResources().getString(R.string.updated));
                            UserItem userItem = SharedPreferenceHelper.getUserDetails(SignUpActivity.this);
                            userItem.setPassword("" + passwordEditText.getText());
                            userItem.setBirthDay("" + birthDayEditText.getText());
                            userItem.setCountryId(selectedCountry);
                            userItem.setEmail("" + emailEditText.getText());
                            userItem.setUsername("" + userNameEditText.getText());
                            userItem.setPhone("" + mobileEditText.getText());
                            if ( gender == 0) {
                                userItem.setGender("male");
                                gender = 0;
                            } else {
                                userItem.setGender("female");
                                gender = 1;
                            }
                            SharedPreferenceHelper.saveUserDetails(SignUpActivity.this, userItem);
                            finish();

                        } else {
                            MUT.lToast(SignUpActivity.this, getResources().getString(R.string.registered));
                            signUpResponse.getUser().get(0).setPassword(passwordEditText.getText() + "");
                            SharedPreferenceHelper.saveUserDetails(SignUpActivity.this, signUpResponse.getUser().get(0));
                            finishAffinity();
                            MUT.startActivity(SignUpActivity.this, ChooseDepartmentActivity.class);
                        }
                    } else if (signUpResponse.getState() == ConnectionPresenter.USED_MAIL) {
                        MUT.lToast(SignUpActivity.this, getResources().getString(R.string.used_email));
                    }
                }

                @Override
                public void onRequestError(Object error) {
                    loadingBar.cancel();
                }

            });
            connectionPresenter.connect(sentData, false, false);
        }
    }

    Dialog loadingBar;

    private void getGCMTokenThenLogin() {
        loadingBar = MUT.createLoadingBar(this);
        loadingBar.show();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                InstanceID instanceID = InstanceID.getInstance(SignUpActivity.this);
                try {
                    instanceID.deleteInstanceID();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    googleId = instanceID.getToken(SENDER_ID,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void ok) {
                signUp();
            }

        }.execute();

    }

    private void selectDate() {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH) + 1;
        final DatePickerDialog datePickerDialog = new DatePickerDialog(SignUpActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                String selectedDate = i + "-" + (++i1) + "-" + i2;
                birthDayEditText.setText(selectedDate);
            }
        }, year, month, 0);

        datePickerDialog.show();
    }

    int gender = 0;

    private void setGenderSpinner() {
        if (genderDialog == null) {
            genderItems = new ArrayList<>();
            genderItems.add(new SpinnerModel(0, getResources().getString(R.string.male)));
            genderItems.add(new SpinnerModel(1, getResources().getString(R.string.female)));
            genderDialog = ViewOperations.setSpinnerDialog(SignUpActivity.this, genderItems, getResources().getString(R.string.gender), new SpinnerListener() {
                @Override
                public void onItemClicked(int position) {
                    genderEditText.setText(genderItems.get(position).getTitle());
                    gender = position;
                }
            });
        } else {
            genderDialog.show();
        }
    }


    private void setUserDetails() {

        userItem = SharedPreferenceHelper.getUserDetails(SignUpActivity.this);
        userNameEditText.setText(userItem.getUsername());
        emailEditText.setText(userItem.getEmail());
        passwordEditText.setText(userItem.getPassword());
        mobileEditText.setText(userItem.getPhone());

        genderEditText.setText(getResources().getString(userItem.getGender().equals("male") ? R.string.male : R.string.female));
        birthDayEditText.setText(userItem.getBirthDay());
        selectedCountry = userItem.getCountryId();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            goSignUp();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1000);
        }

    }


}
