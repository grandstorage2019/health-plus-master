package grand.healthplus.activities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hcareactivities.HCareMainActivity;
import grand.healthplus.activities.hplusactivities.HPlusMainActivity;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;

public class ChooseDepartmentActivity extends AppCompatActivity {

    @BindView(R.id.ll_choose_department_health_plus)
    LinearLayout healthPlusLayout;
    @BindView(R.id.ll_choose_department_health_care)
    LinearLayout healthCareLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_department);
        ButterKnife.bind(ChooseDepartmentActivity.this);
        MUT.setSelectedLanguage(this);
        setEvent();
    }


    private void setEvent(){
        healthCareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(ChooseDepartmentActivity.this, HCareMainActivity.class);
            }
        });

        healthPlusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(ChooseDepartmentActivity.this, HPlusMainActivity.class);
            }
        });
    }


}
