package grand.healthplus.activities.hcareactivities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hcareadapters.CaresAdapter;
import grand.healthplus.fragments.hplusfragment.HealthPlusSearchResultActivity;
import grand.healthplus.models.doctormodels.InsuranceCompaniesItem;
import grand.healthplus.models.doctormodels.InsuranceCompanyResponse;
import grand.healthplus.models.hcaremodels.CaresResponse;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class HealthCareSearchResultActivity extends AppCompatActivity {
    @BindView(R.id.rv_search_result_collection)
    RecyclerView caresRecycleView;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;
    @BindView(R.id.iv_actionbar_sort)
    ImageView sortImageView;

    boolean searchByName=false;

    boolean searchByInsurance = false;

    int   selectedInsurance = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);

        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }

        try{
            searchByName = getIntent().getBooleanExtra("isSearchByName",false);
        }catch (Exception e){
            e.getStackTrace();
        }

        sortImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getInsuranceCompanies();
            }
        });
    }

    @Override
    protected void onResume() {
        getHealthCares();
        super.onResume();
    }



    Dialog insuranceCompaniesDialog = null;
    ArrayList<SpinnerModel> insuranceCompaniesItems;

    private void getInsuranceCompanies() {
        insuranceCompaniesItems = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "insurances");
            jsonObject.put("language",SharedPreferenceHelper.getCurrentLanguage(this));
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(HealthCareSearchResultActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {


                InsuranceCompanyResponse countryResponse = new Gson().fromJson(response.toString(), InsuranceCompanyResponse.class);
                List<InsuranceCompaniesItem> data = countryResponse.getInsuranceCompanies();
                insuranceCompaniesItems.add(new SpinnerModel(-1, getResources().getString(R.string.all)));
                for (int i = 0; i < data.size(); i++) {
                    insuranceCompaniesItems.add(new SpinnerModel(data.get(i).getId(), data.get(i).getName()));
                }


                insuranceCompaniesDialog = ViewOperations.setSpinnerDialog(HealthCareSearchResultActivity.this, insuranceCompaniesItems, getResources().getString(R.string.insurance_company_title), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedInsurance = insuranceCompaniesItems.get(position).getId();
                        searchByInsurance = true;
                        getHealthCares();

                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }



    public void goBack(View view) {
        finish();
    }

    private void getHealthCares() {
        JSONObject jsonObject = new JSONObject();

        try {

           if(searchByName){
                jsonObject.put("method", "search_by_name");
                jsonObject.put("query", getIntent().getStringExtra("query"));
            }else {
                jsonObject.put("method", "search");
                jsonObject.put("area_id", getIntent().getIntExtra("selectedArea", -1));
                jsonObject.put("specialization_id", getIntent().getIntExtra("selectedSpecialty", -1));
            }
            jsonObject.put("insurance_id", selectedInsurance);
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));

            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(this).getId());

            System.out.println("Data : "+jsonObject.toString());

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                CaresResponse caresResponse = new Gson().fromJson(response.toString(), CaresResponse.class);
                if (caresResponse.getState() == ConnectionPresenter.SUCCESS) {
                    CaresAdapter caresAdapter = new CaresAdapter(new ArrayList<>(caresResponse.getHealthCare()), HealthCareSearchResultActivity.this);
                    ViewOperations.setRVHVertical(HealthCareSearchResultActivity.this, caresRecycleView);
                    caresRecycleView.setAdapter(caresAdapter);
                    caresRecycleView.setVisibility(View.VISIBLE);
                } else if (caresResponse.getState() == ConnectionPresenter.EMPTY) {
                    caresRecycleView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, true);
    }


}
