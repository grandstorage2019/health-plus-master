package grand.healthplus.activities.hcareactivities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hcareadapters.CareOffersAdapter;
import grand.healthplus.adapters.hcareadapters.HealthCareImagesAdapter;
import grand.healthplus.adapters.hcareadapters.ServicesAdapter;
import grand.healthplus.adapters.hplusadapters.RatingAdapter;
import grand.healthplus.models.hcaremodels.CareImageResponse;
import grand.healthplus.models.hcaremodels.CareOffersItem;
import grand.healthplus.models.hcaremodels.CareOffersResponse;
import grand.healthplus.models.hcaremodels.CarePicturesItem;
import grand.healthplus.models.hcaremodels.HealthCareItem;
import grand.healthplus.models.hcaremodels.ServicesResponse;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.models.hplusmodels.RatingItem;
import grand.healthplus.models.hplusmodels.RatingResponse;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class HCareMoreinfoActivity extends AppCompatActivity {

    @BindView(R.id.iv_health_care_info_image)
    ImageView careImageView;
    @BindView(R.id.tv_health_care_name)
    TextView nameTextView;
    @BindView(R.id.tv_health_care_info_type)
    TextView typeTextView;
    @BindView(R.id.tv_health_care_info_views)
    TextView viewsTextView;
    @BindView(R.id.rb_health_care_info_rating)
    MaterialRatingBar ratingBar;
    @BindView(R.id.iv_health_care_info_favourite)
    ImageView favouriteImageView;
    @BindView(R.id.tv_health_care_info_services_tab)
    TextView servicesTabTextView;
    @BindView(R.id.tv_health_care_info_offers_tab)
    TextView offersTabTextView;
    @BindView(R.id.tv_health_care_info_info_tab)
    TextView infoTabTextView;
    @BindView(R.id.tv_health_care_info_rating_tab)
    TextView ratingTabTextView;
    @BindView(R.id.tv_health_care_face_book)
    TextView faceBookTextView;
    @BindView(R.id.tv_health_care_instgram)
    TextView instgramTextView;
    @BindView(R.id.tv_health_care_twitter)
    TextView twitterTextView;
    @BindView(R.id.tv_health_care_snap_chat)
    TextView snapChatTextView;
    @BindView(R.id.rv_health_care_services)
    RecyclerView servicesRecyclerView;
    @BindView(R.id.rv_health_care_offers)
    RecyclerView offersRecyclerView;
    @BindView(R.id.rv_health_care_ratings)
    RecyclerView ratingsRecyclerView;
    @BindView(R.id.ll_health_care_info_container)
    LinearLayout infoContainerLinearLayout;
    @BindView(R.id.rv_health_care_pictures)
    RecyclerView picturesRecyclerView;
    @BindView(R.id.ll_health_care_ratings_container)
    LinearLayout ratingContainerLayout;
    @BindView(R.id.tv_health_care_phone)
    TextView phoneTextView;
    @BindView(R.id.tv_health_care_address)
    TextView addressTextView;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    @BindView(R.id.btn_health_care_rate)
    Button rateBtn;

    @BindView(R.id.tv_health_care_info_rating_details)
    TextView ratingDetailsTextView;

    HealthCareItem healthCareItem;
    ArrayList<RatingItem> ratingItems;
    ArrayList<CareOffersItem> careOffersItems = null;
    ArrayList<CarePicturesItem> carePicturesItems = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_care_info);
        ButterKnife.bind(this);
        healthCareItem = ((HealthCareItem) getIntent().getSerializableExtra("healthCareInfo"));
        MUT.setSelectedLanguage(this);


        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }

        setTabsEvents();
        setCareDetails();
        getServices();
        setEvents();


        if (healthCareItem.isFavourite()) {
            favouriteImageView.setImageResource(R.drawable.ic_heart_selected);
        } else {
            favouriteImageView.setImageResource(R.drawable.ic_empty_heart);
        }
    }


    public void share(View view) {
        int applicationNameId = getApplicationInfo().labelRes;
        final String appPackageName = getPackageName();
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, getString(applicationNameId));
        String text = getResources().getString(R.string.instal_this);
        String link = "https://play.google.com/store/apps/details?id=" + appPackageName;
        i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.join_gym) + " " + healthCareItem.getCareName() + " \n " + text + " \n " + link);
        startActivity(Intent.createChooser(i, "Share link:"));
    }

    private void setEvents() {
        favouriteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SharedPreferenceHelper.isLogined(HCareMoreinfoActivity.this)) {
                    addDeleteFavourite(!healthCareItem.isFavourite());
                }
            }
        });
    }

    private void addDeleteFavourite(final boolean isAdd) {

        JSONObject jsonObject = new JSONObject();
        try {
            String method = isAdd ? "add_favourite" : "delete_favourite";
            jsonObject.put("method", method);
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(HCareMoreinfoActivity.this).getId());
            jsonObject.put("care_id", healthCareItem.getCareId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(HCareMoreinfoActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    healthCareItem.setFavourite(!healthCareItem.isFavourite());
                    if (healthCareItem.isFavourite()) {
                        favouriteImageView.setImageResource(R.drawable.ic_heart_selected);
                    } else {
                        favouriteImageView.setImageResource(R.drawable.ic_empty_heart);
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, true);
    }


    private void setTabsEvents() {

        servicesTabTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicesRecyclerView.setVisibility(View.VISIBLE);
                offersRecyclerView.setVisibility(View.GONE);
                ratingContainerLayout.setVisibility(View.GONE);
                infoContainerLinearLayout.setVisibility(View.GONE);
                servicesTabTextView.setBackgroundResource(R.drawable.border_selected_tab_background);
                offersTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
                infoTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
                ratingTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
            }
        });

        offersTabTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (careOffersItems == null) {
                    getOffers();
                }

                servicesRecyclerView.setVisibility(View.GONE);
                offersRecyclerView.setVisibility(View.VISIBLE);
                ratingContainerLayout.setVisibility(View.GONE);
                infoContainerLinearLayout.setVisibility(View.GONE);
                servicesTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
                offersTabTextView.setBackgroundResource(R.drawable.border_selected_tab_background);
                infoTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
                ratingTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
            }
        });

        infoTabTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (carePicturesItems == null) {
                    getPictures();
                }
                servicesRecyclerView.setVisibility(View.GONE);
                offersRecyclerView.setVisibility(View.GONE);
                ratingContainerLayout.setVisibility(View.GONE);
                infoContainerLinearLayout.setVisibility(View.VISIBLE);
                servicesTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
                offersTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
                infoTabTextView.setBackgroundResource(R.drawable.border_selected_tab_background);
                ratingTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
            }
        });

        ratingTabTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ratingItems == null) {
                    getRating();
                }
                servicesRecyclerView.setVisibility(View.GONE);
                offersRecyclerView.setVisibility(View.GONE);
                infoContainerLinearLayout.setVisibility(View.GONE);
                ratingContainerLayout.setVisibility(View.VISIBLE);
                servicesTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
                offersTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
                infoTabTextView.setBackgroundResource(R.drawable.border_un_selected_tab_background);
                ratingTabTextView.setBackgroundResource(R.drawable.border_selected_tab_background);
            }
        });

        rateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SharedPreferenceHelper.isLogined(HCareMoreinfoActivity.this)) {
                    ratingDialog();
                }
            }
        });
    }


    public void goBack(View view) {
        finish();
    }

    private void setCareDetails() {
        ConnectionPresenter.loadImage(careImageView, "care/" + healthCareItem.getCareLogo());
        nameTextView.setText(healthCareItem.getCareName());
        viewsTextView.setText("" + healthCareItem.getViews() + " " + getResources().getString(R.string.views));
        ratingBar.setRating((float) healthCareItem.getRate());
        faceBookTextView.setText(healthCareItem.getFacebook());
        instgramTextView.setText(healthCareItem.getInstagram());
        twitterTextView.setText(healthCareItem.getTwitter());
        snapChatTextView.setText(healthCareItem.getSnapChat());
        phoneTextView.setText(healthCareItem.getPhone());
        addressTextView.setText(healthCareItem.getGovernorateName() + " - " + healthCareItem.getAreaName() + " - " + healthCareItem.getLocationComment());
        typeTextView.setText(healthCareItem.getAbout());
    }

    private void getServices() {


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "services");
            jsonObject.put("care_id", healthCareItem.getCareId());
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ServicesResponse servicesResponse = new Gson().fromJson(response.toString(), ServicesResponse.class);
                if (servicesResponse.getState() == ConnectionPresenter.SUCCESS) {
                    ServicesAdapter servicesAdapter = new ServicesAdapter(new ArrayList<>(servicesResponse.getServices()), HCareMoreinfoActivity.this);
                    ViewOperations.setRVHVertical(HCareMoreinfoActivity.this, servicesRecyclerView);
                    servicesRecyclerView.setAdapter(servicesAdapter);
                    ratingDetailsTextView.setText("(" + getResources().getString(R.string.of) + " " + servicesResponse.getRaters()+" "+getResources().getString(R.string.visited_care)+ ")");
                } else {
                }
            }

            @Override
            public void onRequestError(Object error) {
            }

        });


        connectionPresenter.connect(jsonObject, true, true);
    }

    private void getOffers() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "care_offers");
            jsonObject.put("care_id", healthCareItem.getCareId());
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                CareOffersResponse careOffersResponse = new Gson().fromJson(response.toString(), CareOffersResponse.class);
                if (careOffersResponse.getState() == ConnectionPresenter.SUCCESS) {
                    careOffersItems = new ArrayList<>(careOffersResponse.getCareOffers());
                    CareOffersAdapter careOffersAdapter = new CareOffersAdapter(careOffersItems, HCareMoreinfoActivity.this);
                    ViewOperations.setRVHVertical(HCareMoreinfoActivity.this, offersRecyclerView);
                    offersRecyclerView.setAdapter(careOffersAdapter);
                } else {
                }
            }

            @Override
            public void onRequestError(Object error) {
            }

        });


        connectionPresenter.connect(jsonObject, true, true);
    }


    private void getPictures() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "care_pictures");
            jsonObject.put("care_id", healthCareItem.getCareId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                CareImageResponse careOffersResponse = new Gson().fromJson(response.toString(), CareImageResponse.class);
                if (careOffersResponse.getState() == ConnectionPresenter.SUCCESS) {
                    carePicturesItems = new ArrayList<>(careOffersResponse.getCarePictures());
                    HealthCareImagesAdapter healthCareImagesAdapter = new HealthCareImagesAdapter(carePicturesItems, HCareMoreinfoActivity.this);
                    ViewOperations.setRVHorzontial(HCareMoreinfoActivity.this, picturesRecyclerView);
                    picturesRecyclerView.setAdapter(healthCareImagesAdapter);

                } else {
                }
            }

            @Override
            public void onRequestError(Object error) {
            }

        });


        connectionPresenter.connect(jsonObject, true, true);
    }

    private void getRating() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "rating");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
            jsonObject.put("care_id", healthCareItem.getCareId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                RatingResponse ratingResponse = new Gson().fromJson(response.toString(), RatingResponse.class);
                if (ratingResponse.getState() == ConnectionPresenter.SUCCESS) {

                    ratingItems = new ArrayList<>(ratingResponse.getRating());
                    RatingAdapter ratingAdapter = new RatingAdapter(ratingItems, HCareMoreinfoActivity.this);
                    ViewOperations.setRVHVertical(HCareMoreinfoActivity.this, ratingsRecyclerView);
                    ratingsRecyclerView.setAdapter(ratingAdapter);
                    ratingBar.setRating((float) ratingResponse.getTotalRating());
                    ratingDetailsTextView.setText("(" + getResources().getString(R.string.of) + " " + ratingResponse.getRaters() + " " + getResources().getString(R.string.visited_doctor) + ")");

                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, true);
    }


    private void rateNow(double rate, String review) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "rate");
            jsonObject.put("stars", rate);
            jsonObject.put("comment", review);
            jsonObject.put("care_id", healthCareItem.getCareId());
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(HCareMoreinfoActivity.this).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse ratingResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (ratingResponse.getState() == ConnectionPresenter.SUCCESS) {
                    getRating();
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, true);
    }

    public void ratingDialog() {
        final Dialog loadingBar = new Dialog(HCareMoreinfoActivity.this, android.R.style.Theme_Black_NoTitleBar);
        loadingBar.setCancelable(true);
        loadingBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(HCareMoreinfoActivity.this);
        View ratingView = inflater.inflate(R.layout.dialog_rate_doctor, null);
        loadingBar.setContentView(ratingView);

        final EditText review = ratingView.findViewById(R.id.et_rate_doctor_review);
        final MaterialRatingBar ratingBar = ratingView.findViewById(R.id.rb_rate_doctor_rating);
        Button confirm = ratingView.findViewById(R.id.btn_rate_doctor_confirm);
        Button cancel = ratingView.findViewById(R.id.btn_rate_doctor_cancel);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateNow(ratingBar.getRating(), review.getText() + "");
                loadingBar.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingBar.cancel();
            }
        });

        loadingBar.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0e000000")));
        loadingBar.show();
    }



}
