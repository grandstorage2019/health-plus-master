package grand.healthplus.activities.doctoractivities;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hcareactivities.HCareMainActivity;
import grand.healthplus.adapters.doctoradapters.ClinicPicturesAdapter;
import grand.healthplus.adapters.doctoradapters.MedicalSubSpecializationsAdapter;
import grand.healthplus.adapters.doctoradapters.ServicesAdapter;
import grand.healthplus.models.doctormodels.ClinicPicturesResponse;
import grand.healthplus.models.doctormodels.ServiceResponse;
import grand.healthplus.models.doctormodels.SpecializationResponse;
import grand.healthplus.models.doctormodels.SpecializationsItem;
import grand.healthplus.models.doctormodels.SubSpecializationResponse;
import grand.healthplus.models.doctormodels.SubSpecializationsItem;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.trash.CompressObject;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.FileOperations;
import grand.healthplus.utils.ImageCompression;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class ClinicPicturesActivity extends AppCompatActivity {

    @BindView(R.id.tv_clinic_pictures_add)
    TextView addPicturesTextView;

    @BindView(R.id.tv_clinic_pictures_counter)
    TextView counterTextView;

    @BindView(R.id.rv_clinic_pictures_collection)
    RecyclerView picturesRecyclerView;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    int SELECT_PHOTO_CONSTANT = 10001;

    int imagesSize = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_pictures);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
        fileOperations = new FileOperations();
        setEvents();
        getClinicPictures();
    }


    private void setEvents() {

        addPicturesTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (imagesSize > 9) {
                    Toast.makeText(ClinicPicturesActivity.this, ""+getResources().getString(R.string.only_ten), Toast.LENGTH_SHORT).show();
                } else {
                    if (ActivityCompat.checkSelfPermission(ClinicPicturesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ClinicPicturesActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        selectImage();
                    } else {
                        ActivityCompat.requestPermissions(ClinicPicturesActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_PHOTO_CONSTANT);
                    }
                }

            }
        });


        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void selectImage() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.select_image));
        builder.setItems(new CharSequence[]{getResources().getString(R.string.gallery),
                        getResources().getString(R.string.camera)},
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent();
                                galleryIntent.setType("image/*");
                                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(galleryIntent, 1024);
                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, 1023);
                                break;

                            default:
                                break;
                        }
                    }
                });

        builder.show();
    }


    private void getClinicPictures() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "clinic_pictures");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ClinicPicturesResponse servicesResponse = new Gson().fromJson(response.toString(), ClinicPicturesResponse.class);
                if (servicesResponse.getState() == ConnectionPresenter.SUCCESS) {
                    ClinicPicturesAdapter servicesAdapter = new ClinicPicturesAdapter(new ArrayList<>(servicesResponse.getClinicPictures()), ClinicPicturesActivity.this);
                    ViewOperations.setRVHorzontial(ClinicPicturesActivity.this, picturesRecyclerView);
                    picturesRecyclerView.setAdapter(servicesAdapter);
                    imagesSize = servicesResponse.getClinicPictures().size();
                    counterTextView.setText("(" + servicesResponse.getClinicPictures().size() + "/10)");
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }

    private void uploadImage() {

        JSONObject params = new JSONObject();
        try {
            params.put("method", "upload_clinic_picture");
            params.put("image_name", imageName + "");
            params.put("image_data", image);
            params.put("id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(ClinicPicturesActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    try {

                        if (response.getInt("state") == ConnectionPresenter.SUCCESS) {
                            MUT.lToast(ClinicPicturesActivity.this, getResources().getString(R.string.image_uploaded));
                            imageName = null;
                            image = null;
                            compressObject = null;
                            file = null;
                            bytes = null;
                            getClinicPictures();
                        } else {
                            MUT.lToast(ClinicPicturesActivity.this, getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }
            });
            connectionPresenter.doctorConnect(params, true, false);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    CompressObject compressObject;
    String file;
    byte[] bytes;
    String image = "", imageName = "";
    FileOperations fileOperations;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        try {
            Uri lastData = data.getData();

            if (lastData == null || data == null) {
                lastData = FileOperations.specialCameraSelector(ClinicPicturesActivity.this, (Bitmap) data.getExtras().get("data"));
            }

            if (lastData == null) {
                return;
            } else {
                file = fileOperations.getPath(ClinicPicturesActivity.this, lastData);
                final Dialog dialog = new Dialog(ClinicPicturesActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_attach_image);
                try {
                    dialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(Color.TRANSPARENT));
                } catch (Exception e) {
                    e.getStackTrace();
                }
                final ImageView attachImage = dialog.findViewById(R.id.iv_attach_image_photo);
                Button send = dialog.findViewById(R.id.btn_attach_image_confirm);
                Button cancel = dialog.findViewById(R.id.btn_attach_image_cancel);
                final File f = new File(file);
                if (f.exists()) {

                    compressObject = new ImageCompression(ClinicPicturesActivity.this).compressImage(file, f.getName());
                    attachImage.setImageBitmap(compressObject.getImage());
                    bytes = compressObject.getByteStream().toByteArray();

                }

                send.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        image = Base64.encodeToString(bytes, 0);
                        imageName = f.getName();
                        uploadImage();
                        dialog.cancel();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (ActivityCompat.checkSelfPermission(ClinicPicturesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ClinicPicturesActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            selectImage();
        } else {
            ActivityCompat.requestPermissions(ClinicPicturesActivity.this, new String[]{android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.GET_ACCOUNTS}, SELECT_PHOTO_CONSTANT);
        }
    }


}
