package grand.healthplus.activities.doctoractivities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.doctormodels.AboutDoctor;
import grand.healthplus.models.doctormodels.AboutDoctorResponse;
import grand.healthplus.trash.CompressObject;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.FileOperations;
import grand.healthplus.utils.ImageCompression;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


/**
 * Created by sameh on 5/2/18.
 */

public class AboutDoctorActivity extends AppCompatActivity {

    @BindView(R.id.et_about_doctor_info)
    EditText englishInfoEditText;
    @BindView(R.id.et_about_doctor_info_arabic)
    EditText arabicInfoEditText;
    @BindView(R.id.et_about_doctor_first_name)
    MaterialEditText englishFirstNameEditText;
    @BindView(R.id.et_about_doctor_last_name)
    MaterialEditText englishLastNameEditText;
    @BindView(R.id.et_about_doctor_first_name_arabic)
    MaterialEditText arabicFirstNameEditText;
    @BindView(R.id.et_about_doctor_last_name_arabic)
    MaterialEditText arabicLastNameEditText;
    @BindView(R.id.et_about_doctor_date)
    MaterialEditText dateEditText;
    @BindView(R.id.rg_about_doctor_gender)
    RadioGroup genderRadioGroup;
    @BindView(R.id.et_about_doctor_address)
    MaterialEditText englishAddressEditText;
    @BindView(R.id.et_about_doctor_arabic_address)
    MaterialEditText arabicAddressEditText;
    @BindView(R.id.tv_about_doctor_licence)
    TextView licenceTextView;
    @BindView(R.id.iv_about_doctor_licence)
    ImageView licenceImageView;
    @BindView(R.id.tv_actionbar_save)
    TextView saveTextView;
    @BindView(R.id.tv_about_doctor_specialization)
    TextView specializationTextView;
    @BindView(R.id.tv_about_doctor_name)
    TextView nameTextView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;
    @BindView(R.id.iv_about_doctor_photo)
    ImageView photoImageView;
    @BindView(R.id.et_about_doctor_title)
    TextView titleEditText;
    @BindView(R.id.et_about_doctor_title_ar)
    TextView titleArEditText;

    @BindView(R.id.tv_about_doctor_title)
    TextView titleTextView;
    @BindView(R.id.tv_about_doctor_title_ar)
    TextView titleArTextView;

    String filePath, imageName, image;
    FileOperations fileOperations;

    int SELECT_PHOTO_CONSTANT = 10001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_doctor);
        fileOperations = new FileOperations();
        ButterKnife.bind(AboutDoctorActivity.this);
        MUT.setSelectedLanguage(this);
        setEvents();
        aboutDoctor();
    }

    private void setEvents() {
        licenceTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MUT.startActivity(AboutDoctorActivity.this, AddLicenseActivity.class);
                /*if (ActivityCompat.checkSelfPermission(AboutDoctorActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AboutDoctorActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(AboutDoctorActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_PHOTO_CONSTANT);
                }*/
            }
        });

        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });

        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editDoctorInfo();
            }
        });

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }


    }

    private void selectDate() {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH) + 1;
        final DatePickerDialog datePickerDialog = new DatePickerDialog(AboutDoctorActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                String selectedDate = i + "-" + (++i1) + "-" + i2;
                dateEditText.setText(selectedDate);
            }
        }, year, month, 0);

        datePickerDialog.show();
    }


    public void selectImage() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.select_image));
        builder.setItems(new CharSequence[]{getResources().getString(R.string.gallery),
                        getResources().getString(R.string.camera)},
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent();
                                galleryIntent.setType("image/*");
                                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(galleryIntent, 1024);
                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, 1023);
                                break;

                            default:
                                break;
                        }
                    }
                });

        builder.show();
    }


    private void editDoctorInfo() {

        JSONObject params = new JSONObject();
        try {
            params.put("method", "edit_doctor_info");
            params.put("about_en", englishInfoEditText.getText() + "");
            params.put("about_ar", arabicInfoEditText.getText() + "");
            params.put("fname_en", englishFirstNameEditText.getText() + "");
            params.put("lname_en", englishLastNameEditText.getText() + "");
            params.put("fname_ar", arabicFirstNameEditText.getText() + "");
            params.put("lname_ar", arabicLastNameEditText.getText() + "");
            params.put("birth_day", dateEditText.getText() + "");
            params.put("title_en", englishAddressEditText.getText() + "");
            params.put("title_ar", arabicAddressEditText.getText() + "");
            params.put("level_ar", titleArEditText.getText() + "");
            params.put("level_en", titleEditText.getText() + "");

            params.put("gender", genderRadioGroup.getCheckedRadioButtonId() == R.id.rb_about_doctor_male ? "male" : "female");
            params.put("id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(AboutDoctorActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    try {
                        if (response.getInt("state") == ConnectionPresenter.SUCCESS) {
                            MUT.lToast(AboutDoctorActivity.this, getResources().getString(R.string.updated));
                        } else {

                        }

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }
            });
            connectionPresenter.doctorConnect(params, true, false);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    private void uploadImage() {


        JSONObject params = new JSONObject();
        try {
            params.put("method", "upload_licence");
            params.put("image_name", imageName + "");
            params.put("image_data", image);
            params.put("id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(AboutDoctorActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    try {
                        if (response.getInt("state") == ConnectionPresenter.SUCCESS) {

                        } else {

                        }

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }
            });
            connectionPresenter.doctorConnect(params, true, false);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    CompressObject compressObject;
    byte[] bytes;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == RESULT_OK) {
                Uri lastData = data.getData();
                if (lastData == null || data == null) {
                    lastData = FileOperations.specialCameraSelector(AboutDoctorActivity.this, (Bitmap) data.getExtras().get("data"));
                }
                {
                    filePath = fileOperations.getPath(AboutDoctorActivity.this, lastData);

                    final File f = new File(filePath);
                    if (f.exists()) {
                        imageName = f.getName();
                        compressObject = new ImageCompression(AboutDoctorActivity.this).compressImage(filePath, f.getName());
                        licenceImageView.setImageBitmap(compressObject.getImage());
                        bytes = compressObject.getByteStream().toByteArray();
                        image = Base64.encodeToString(bytes, 0);
                        imageName = f.getName();
                    }

                    final Dialog dialog = new Dialog(AboutDoctorActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_attach_image);
                    try {
                        dialog.getWindow().setBackgroundDrawable(
                                new ColorDrawable(Color.TRANSPARENT));
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                    final ImageView attachImage = dialog.findViewById(R.id.iv_attach_image_photo);
                    Button send = dialog.findViewById(R.id.btn_attach_image_confirm);
                    Button cancel = dialog.findViewById(R.id.btn_attach_image_cancel);
                    attachImage.setImageBitmap(compressObject.getImage());


                    send.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            image = Base64.encodeToString(bytes, 0);
                            imageName = f.getName();
                            uploadImage();
                            dialog.cancel();
                        }
                    });

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });
                    dialog.show();

                }
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }


    private void aboutDoctor() {


        JSONObject params = new JSONObject();
        try {
            params.put("method", "about_doctor");
            params.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(AboutDoctorActivity.this).getId());


            params.put("language", SharedPreferenceHelper.getCurrentLanguage(AboutDoctorActivity.this));

            ConnectionPresenter connectionPresenter = new ConnectionPresenter(AboutDoctorActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    try {
                        AboutDoctorResponse aboutDoctorResponse = new Gson().fromJson(response.toString(), AboutDoctorResponse.class);

                        if (aboutDoctorResponse.getState() == ConnectionPresenter.SUCCESS) {
                            AboutDoctor aboutDoctor = aboutDoctorResponse.getAboutDoctor();

                            englishInfoEditText.setText(aboutDoctor.getAboutEn());
                            arabicInfoEditText.setText(aboutDoctor.getAboutAr());
                            englishFirstNameEditText.setText(aboutDoctor.getFnameEn());
                            englishLastNameEditText.setText(aboutDoctor.getLnameEn());
                            arabicFirstNameEditText.setText(aboutDoctor.getFnameAr());
                            arabicLastNameEditText.setText(aboutDoctor.getLnameAr());


                            titleArEditText.setText(aboutDoctor.getLevelAr());
                            titleEditText.setText(aboutDoctor.getLevelEn());



                            englishInfoEditText.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void afterTextChanged(Editable s) {

                                }

                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    titleTextView.setText(getResources().getString(R.string.about_doctor)+" ("+englishInfoEditText.getText().length()+"/500)");
                                }
                            });

                            arabicInfoEditText.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void afterTextChanged(Editable s) {

                                }

                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    titleArTextView.setText(getResources().getString(R.string.about_doctor_arabic)+" ("+arabicInfoEditText.getText().length()+"/500)");
                                }
                            });


                            titleTextView.setText(getResources().getString(R.string.about_doctor)+" ("+englishInfoEditText.getText().length()+"/500)");
                            titleArTextView.setText(getResources().getString(R.string.about_doctor_arabic)+" ("+arabicInfoEditText.getText().length()+"/500)");

                            try {
                                dateEditText.setText(aboutDoctor.getBirthDay().split(" ")[0]);
                            } catch (Exception e) {
                                e.getStackTrace();
                            }

                            //Toast.makeText(AboutDoctorActivity.this, ""+aboutDoctor.getGender().equals("male"), Toast.LENGTH_SHORT).show();
                            genderRadioGroup.check(aboutDoctor.getGender().equals("male") ? R.id.rb_about_doctor_male : R.id.rb_about_doctor_female);
                            englishAddressEditText.setText(aboutDoctor.getTitleEn());
                            arabicAddressEditText.setText(aboutDoctor.getTitleAr());
                            specializationTextView.setText(aboutDoctor.getSpecialization());

                            if(SharedPreferenceHelper.getCurrentLanguage(AboutDoctorActivity.this).equals("en")) {
                                nameTextView.setText(aboutDoctor.getFnameEn() + " " + aboutDoctor.getLnameEn());
                            }else {
                                if(aboutDoctor.getFnameAr()==null) nameTextView.setText("");
                                else
                                nameTextView.setText(aboutDoctor.getFnameAr() + " " + aboutDoctor.getLnameAr());
                            }
                            ConnectionPresenter.loadImage(licenceImageView, "license/" + aboutDoctor.getLicense());

                            if (aboutDoctor.getProfileImage() == null || aboutDoctor.getProfileImage().equals("NULL") || aboutDoctor.getProfileImage().equals("null") || aboutDoctor.getProfileImage().equals("")) {
                                photoImageView.setImageResource(R.drawable.ic_user_temp);
                            } else {
                                ConnectionPresenter.loadImage(photoImageView, "doctor_profile/" + aboutDoctor.getProfileImage());
                            }


                        } else {

                        }

                    } catch (Exception e) {

                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }
            });
            connectionPresenter.doctorConnect(params, true, false);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (ActivityCompat.checkSelfPermission(AboutDoctorActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AboutDoctorActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            selectImage();
        } else {
            ActivityCompat.requestPermissions(AboutDoctorActivity.this, new String[]{android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.GET_ACCOUNTS}, SELECT_PHOTO_CONSTANT);
        }
    }


}