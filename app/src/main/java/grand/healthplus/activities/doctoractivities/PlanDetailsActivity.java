package grand.healthplus.activities.doctoractivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.SimpleToolBarListener;
import grand.healthplus.models.doctormodels.PlansItem;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;



public class PlanDetailsActivity extends AppCompatActivity {


    @BindView(R.id.tv_plan_details_info)
    TextView infoTextView;
    @BindView(R.id.btn_plan_details_next)
    Button nextBtn;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    PlansItem plansItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_details);
        ButterKnife.bind(PlanDetailsActivity.this);
        MUT.setSelectedLanguage(this);

        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }

        plansItem = ((PlansItem) getIntent().getSerializableExtra("planItem"));


        setPlanDetails();
        setEvent();
    }


    private void setPlan() {

        JSONObject params = new JSONObject();
        try {
            params.put("method", "set_plan");
            params.put("plan_id", plansItem.getId());
            params.put("id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(PlanDetailsActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    try {
                        DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                        if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                            MUT.lToast(PlanDetailsActivity.this, getResources().getString(R.string.plan_added));
                            MUT.startActivity(PlanDetailsActivity.this,DoctorMainActivity.class);
                        }

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }
            });
            connectionPresenter.doctorConnect(params, true, false);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    private void setEvent(){
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlan();
            }
        });
    }

    private void setPlanDetails() {
        infoTextView.setText(plansItem.getDetails());
    }

    public void goBack(View view){
        finish();
    }

}