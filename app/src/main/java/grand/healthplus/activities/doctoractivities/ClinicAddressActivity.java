package grand.healthplus.activities.doctoractivities;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hcareactivities.HealthCareSearchResultActivity;
import grand.healthplus.models.doctormodels.ClinicAddressItem;
import grand.healthplus.models.doctormodels.ClinicAddressResponse;
import grand.healthplus.models.hplusmodels.AreaResponse;
import grand.healthplus.models.hplusmodels.AreasItem;
import grand.healthplus.models.hplusmodels.CountriesItem;
import grand.healthplus.models.hplusmodels.CountryResponse;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.models.hplusmodels.GovernorateResponse;
import grand.healthplus.models.hplusmodels.GovernoratesItem;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.utils.Check;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class ClinicAddressActivity extends AppCompatActivity {

    @BindView(R.id.tv_actionbar_save)
    TextView saveTextView;
    @BindView(R.id.et_clinic_details_country)
    MaterialEditText countryEditText;
    @BindView(R.id.et_clinic_details_city)
    MaterialEditText cityEditText;
    @BindView(R.id.et_clinic_details_area)
    MaterialEditText areaEditText;
    @BindView(R.id.et_clinic_details_building_number_arabic)
    EditText buildingNumberEditTextArabic;
    @BindView(R.id.et_clinic_details_building_number)
    EditText buildingNumberEditText;
    @BindView(R.id.et_clinic_details_street_name_arabic)
    EditText streetNameEditTextArabic;
    @BindView(R.id.et_clinic_details_street_name)
    EditText streetNameEditText;
    @BindView(R.id.et_clinic_details_floor_arabic)
    EditText floorEditTextArabic;
    @BindView(R.id.et_clinic_details_floor)
    EditText floorEditText;
    @BindView(R.id.et_clinic_details_flat_arabic)
    EditText flatEditTextArabic;
    @BindView(R.id.et_clinic_details_flat)
    EditText flatEditText;
    @BindView(R.id.et_clinic_details_land_mark_arabic)
    EditText landMarkArabicEditText;
    @BindView(R.id.et_clinic_details_land_mark)
    EditText landMarkEditText;
    @BindView(R.id.et_clinic_details_select_location)
    EditText selectLocationEditText;
    @BindView(R.id.et_clinic_details_block)
    EditText blockEditText;
    @BindView(R.id.et_clinic_details_block_ar)
    EditText blockArEditText;
    @BindView(R.id.et_clinic_details_flat_number)
    EditText flatNumberEditText;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    int selectedCountry = -1, selectedCity = -1, selectedGovernorate = -1, selectedArea = -1;
     double lat=0.0,lang=0.0;

    ArrayList<SpinnerModel> governorateItems, areaItems;


    Dialog countryDialog, governorateDialog = null, areaDialog = null;

    String language = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_details);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
        language = SharedPreferenceHelper.getCurrentLanguage(ClinicAddressActivity.this);
        setEvents();
        getClinicDetails();
    }

    private void setEvents() {

        selectLocationEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ClinicAddressActivity.this,SelectLocationActivity.class);
                intent.putExtra("lat",lat);
                intent.putExtra("lang",lang);
                startActivity(intent);
            }
        });

        countryEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCountries();
            }
        });

        cityEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getGovernerates();
            }
        });

        areaEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAreas();
            }
        });

        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedArea == -1 || Check.isEmpty(buildingNumberEditTextArabic.getText() + "")
                        || Check.isEmpty(buildingNumberEditText.getText() + "")
                        || Check.isEmpty(streetNameEditTextArabic.getText() + "")
                        || Check.isEmpty(streetNameEditText.getText() + "")
                        || Check.isEmpty(floorEditTextArabic.getText() + "")
                        || Check.isEmpty(floorEditText.getText() + "")
                        || Check.isEmpty(flatEditText.getText() + "")
                        || Check.isEmpty(landMarkArabicEditText.getText() + "")
                        || Check.isEmpty(landMarkEditText.getText() + "")) {
                    Toast.makeText(ClinicAddressActivity.this, "" + getResources().getString(R.string.fill_data), Toast.LENGTH_SHORT).show();

                } else {
                    saveClinicDetails();
                }
            }
        });
    }

    private void saveClinicDetails() {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("method", "edit_clinic_address");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            jsonObject.put("area_id", "" + selectedArea);
            jsonObject.put("building_number_ar", "" + buildingNumberEditTextArabic.getText());
            jsonObject.put("building_number_en", "" + buildingNumberEditText.getText());
            jsonObject.put("street_name_ar", "" + streetNameEditTextArabic.getText());
            jsonObject.put("street_name_en", "" + streetNameEditText.getText());
            jsonObject.put("floor_ar", "" + floorEditTextArabic.getText());
            jsonObject.put("floor_en", "" + floorEditText.getText());
            jsonObject.put("apartment_ar", "" + flatEditTextArabic.getText());
            jsonObject.put("apartment_en", "" + flatEditText.getText());
            jsonObject.put("landmark_ar", "" + landMarkArabicEditText.getText());
            jsonObject.put("landmark_en", "" + landMarkEditText.getText());
            jsonObject.put("block_ar",""+blockArEditText.getText());
            jsonObject.put("block_en",""+blockEditText.getText());
            jsonObject.put("flat_number",""+flatNumberEditText.getText());


        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    Toast.makeText(ClinicAddressActivity.this, getResources().getString(R.string.updated), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }


    private void getClinicDetails() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_clinic_address");
            jsonObject.put("language", language);
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ClinicAddressResponse clinicAddressResponse = new Gson().fromJson(response.toString(), ClinicAddressResponse.class);
                if (clinicAddressResponse.getState() == ConnectionPresenter.SUCCESS) {



                    ClinicAddressItem clinicAddressItem = clinicAddressResponse.getClinicAddress().get(0);
                    selectedArea = clinicAddressItem.getAreaId();
                    selectedCountry = clinicAddressItem.getCountryId();
                    selectedGovernorate = clinicAddressItem.getGovernorateId();
                    countryEditText.setText(clinicAddressItem.getCountriesName());
                    cityEditText.setText(clinicAddressItem.getGovernorateName());
                    areaEditText.setText(clinicAddressItem.getAreaName());
                    buildingNumberEditTextArabic.setText(clinicAddressItem.getBuildingNumberAr());
                    buildingNumberEditText.setText(clinicAddressItem.getBuildingNumberEn());
                    streetNameEditTextArabic.setText(clinicAddressItem.getStreetNameAr());
                    streetNameEditText.setText(clinicAddressItem.getStreetNameEn());
                    floorEditTextArabic.setText(clinicAddressItem.getFloorAr());
                    floorEditText.setText(clinicAddressItem.getFloorEn());
                    flatEditTextArabic.setText(clinicAddressItem.getApartmentAr());
                    flatEditText.setText(clinicAddressItem.getApartmentEn());
                    landMarkArabicEditText.setText(clinicAddressItem.getLandmarkAr());
                    landMarkEditText.setText(clinicAddressItem.getLandmarkEn());
                    blockEditText.setText(clinicAddressItem.getBlock());
                    blockArEditText.setText(clinicAddressItem.getBlockAr());
                    flatNumberEditText.setText(clinicAddressItem.getFlatNumber());

                    try{
                        lat = Double.parseDouble(clinicAddressItem.getLat());
                        lang = Double.parseDouble(clinicAddressItem.getLang());
                    }catch (Exception e){
                        e.getStackTrace();
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }


    ArrayList<SpinnerModel> countryItems;

    private void getCountries() {
        selectedCity = -1;
        selectedArea = -1;
        countryItems = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "countries");
            jsonObject.put("language", language);
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(ClinicAddressActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                grand.healthplus.models.hplusmodels.CountryResponse countryResponse = new Gson().fromJson(response.toString(), grand.healthplus.models.hplusmodels.CountryResponse.class);
                List<CountriesItem> data = countryResponse.getCountries();

                for (int i = 0; i < data.size(); i++) {
                    countryItems.add(new SpinnerModel(data.get(i).getId(), data.get(i).getName()));
                }


                countryDialog = ViewOperations.setSpinnerDialog(ClinicAddressActivity.this, countryItems, getResources().getString(R.string.countries), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedCountry = countryItems.get(position).getId();
                        countryEditText.setText(countryItems.get(position).getTitle());

                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }


    private void getGovernerates() {
        governorateItems = new ArrayList<>();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "governorates");
            jsonObject.put("language", language);
            jsonObject.put("country_id", selectedCountry);
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(ClinicAddressActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                GovernorateResponse governorateResponse = new Gson().fromJson(response.toString(), GovernorateResponse.class);
                List<GovernoratesItem> governoratesItems = governorateResponse.getGovernorates();

                for (int i = 0; i < governoratesItems.size(); i++) {

                    governorateItems.add(new SpinnerModel(governoratesItems.get(i).getId(), governoratesItems.get(i).getName()));
                }

                governorateDialog = ViewOperations.setSpinnerDialog(ClinicAddressActivity.this, governorateItems, getResources().getString(R.string.governorates), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedGovernorate = governorateItems.get(position).getId();
                        cityEditText.setText(governorateItems.get(position).getTitle());
                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });


        connectionPresenter.connect(jsonObject, true, false);
    }


    private void getAreas() {
        areaItems = new ArrayList<>();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "areas");
            jsonObject.put("language", language);
            jsonObject.put("governorate_id", selectedGovernorate);
        } catch (Exception e) {
            e.getStackTrace();
        }


        ConnectionPresenter connectionPresenter = new ConnectionPresenter(ClinicAddressActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                AreaResponse areaResponse = new Gson().fromJson(response.toString(), AreaResponse.class);
                List<AreasItem> data = areaResponse.getAreas();

                for (int i = 0; i < data.size(); i++) {
                    areaItems.add(new SpinnerModel(data.get(i).getId(), data.get(i).getName()));
                }


                areaDialog = ViewOperations.setSpinnerDialog(ClinicAddressActivity.this, areaItems, getResources().getString(R.string.area), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedArea = areaItems.get(position).getId();
                        areaEditText.setText(areaItems.get(position).getTitle());
                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }

    public void goBack(View view) {
        finish();
    }
}
