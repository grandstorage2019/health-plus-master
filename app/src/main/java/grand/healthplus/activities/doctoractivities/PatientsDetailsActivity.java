package grand.healthplus.activities.doctoractivities;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hplusactivities.DaysActivity;
import grand.healthplus.activities.hplusactivities.DoctorInformationActivity;
import grand.healthplus.models.doctormodels.ChattersItem;
import grand.healthplus.models.doctormodels.PatientsItem;
import grand.healthplus.models.doctormodels.registrationmodel.UserItem;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.models.hplusmodels.DoctorsItem;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class PatientsDetailsActivity extends AppCompatActivity {

    @BindView(R.id.tv_patient_name)
    TextView nameTextView;
    @BindView(R.id.tv_patient_phone)
    TextView phoneTextView;
    @BindView(R.id.tv_patient_gender)
    TextView genderTextView;
    @BindView(R.id.tv_patient_insurance)
    TextView insuranceTextView;
    @BindView(R.id.tv_patient_notes)
    TextView notesTextView;
    @BindView(R.id.iv_patient_phone)
    ImageView phoneImageView;
    @BindView(R.id.iv_patient_chat)
    ImageView chatImageView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;
    @BindView(R.id.iv_patient_book)
    ImageView bookTextView;

    @BindView(R.id.iv_patient_delete)
    ImageView deleteImageView;


    PatientsItem patientsItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        ButterKnife.bind(PatientsDetailsActivity.this);
        MUT.setSelectedLanguage(this);
        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }
        patientsItem = (PatientsItem) getIntent().getSerializableExtra("PatientsDetails");
        setEvents();
        setPatientDetails();
    }

    private void setEvents() {

        phoneImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.makeCall(PatientsDetailsActivity.this, patientsItem.getPhone());
            }
        });

        chatImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChattersItem chattersItem = new ChattersItem();
                chattersItem.setPatientId(patientsItem.getId());
                chattersItem.setWho("doctor");
                chattersItem.setMassage("");
                Intent intent = new Intent(PatientsDetailsActivity.this, ChatActivity.class);
                intent.putExtra("chattersItemDetails", chattersItem);
                startActivity(intent);
            }
        });

        bookTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DoctorsItem doctorsItem = new DoctorsItem();
                UserItem userItem = DoctorSharedPreferenceHelper.getUserDetails(PatientsDetailsActivity.this);
                doctorsItem.setDoctorId(userItem.getId());
                Intent intent = new Intent(PatientsDetailsActivity.this, DaysActivity.class);
                intent.putExtra("doctorId", doctorsItem);
                intent.putExtra("patient_id", patientsItem.getId());
                intent.putExtra("isDoctor", true);
                startActivity(intent);
            }
        });

        deleteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                patientDialog();
            }
        });
    }

    private void deletePatient() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "delete_patient");


            jsonObject.put("id", patientsItem.getId());

        } catch (Exception e) {
            e.getStackTrace();
        }
        System.out.println("details "+jsonObject.toString());

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(PatientsDetailsActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    finish();
                    Toast.makeText(PatientsDetailsActivity.this, "" + getResources().getString(R.string.patient_deleted_successfully), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, true);
    }

    public void patientDialog() {
        final Dialog ratingDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar);
        ratingDialog.setCancelable(true);
        ratingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(this);
        View ratingView = inflater.inflate(R.layout.dialog_cancel_booking, null);
        ratingDialog.setContentView(ratingView);

        Button confirm = ratingView.findViewById(R.id.btn_cancel_booking_confirm);
        Button cancel = ratingView.findViewById(R.id.btn_cancel_booking_cancel);

        TextView title = ratingView.findViewById(R.id.tv_cancel_booking_title);
        TextView details = ratingView.findViewById(R.id.tv_cancel_booking_details);

        title.setText(getResources().getString(R.string.delete_patint));
        details.setText(getResources().getString(R.string.sure_delete_patint));



        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletePatient();
                ratingDialog.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingDialog.cancel();
            }
        });

        ratingDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0e000000")));
        ratingDialog.show();
    }


    private void setPatientDetails() {
        nameTextView.setText(getResources().getString(R.string.patient_name) + " : " +patientsItem.getName());
        phoneTextView.setText(getResources().getString(R.string.mobile) + " : " +patientsItem.getPhone());
        genderTextView.setText(getResources().getString(R.string.sex) + " : " +patientsItem.getGender());

        if(patientsItem.getInsuranceCompany()==null||patientsItem.getInsuranceCompany().equals(""))insuranceTextView.setVisibility(View.GONE);
        insuranceTextView.setText(getResources().getString(R.string.insurance_company) + " : " +patientsItem.getInsuranceCompany());
        if(patientsItem.getNotes()==null||patientsItem.getNotes().equals(""))notesTextView.setVisibility(View.GONE);
        notesTextView.setText(getResources().getString(R.string.notes) + " : " +patientsItem.getNotes());

    }

    public void goBack(View view) {
        finish();
    }

}