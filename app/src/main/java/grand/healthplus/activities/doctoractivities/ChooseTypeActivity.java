package grand.healthplus.activities.doctoractivities;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ChooseDepartmentActivity;
import grand.healthplus.activities.hplusactivities.ExplainActivity;
import grand.healthplus.models.hcaremodels.registeration.SignUpResponse;
import grand.healthplus.models.hcaremodels.registeration.UserItem;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


/**
 * Created by sameh on 5/2/18.
 */

public class ChooseTypeActivity extends AppCompatActivity {

    @BindView(R.id.ll_choose_department_health_plus)
    LinearLayout healthPlusLayout;
    @BindView(R.id.ll_choose_department_health_care)
    LinearLayout healthCareLayout;

    public static final String SENDER_ID = "306498002272";
    String googleId = "";
    Dialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_type);
        ButterKnife.bind(ChooseTypeActivity.this);
        MUT.setSelectedLanguage(this);
        setEvent();
        loadingBar = MUT.createLoadingBar(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(loadingBar!=null)
        loadingBar.cancel();
    }

    private void setEvent() {
        healthCareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SharedPreferenceHelper.isFirstTime(ChooseTypeActivity.this)) {
                    finish();
                    MUT.startActivity(ChooseTypeActivity.this, ExplainActivity.class);
                } else {
                    if (SharedPreferenceHelper.getUserDetails(ChooseTypeActivity.this) != null && SharedPreferenceHelper.isRemember(ChooseTypeActivity.this)) {
                        getGCMTokenThenLogin();
                    } else {
                        finish();
                        SharedPreferenceHelper.clearUserDetails(ChooseTypeActivity.this);
                        MUT.startActivity(ChooseTypeActivity.this, ChooseDepartmentActivity.class);
                    }
                }


            }
        });

        healthPlusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getGCMTokenThenLogin2();
            }
        });
    }

    private void logIn2() {

        final grand.healthplus.models.doctormodels.registrationmodel.UserItem userItem = DoctorSharedPreferenceHelper.getUserDetails(ChooseTypeActivity.this);


        JSONObject params = new JSONObject();
        try {
            params.put("method", "login");
            params.put("email", userItem.getEmail() + "");
            params.put("password", userItem.getPassword() + "");
            params.put("google_id", googleId + "");
            params.put("device_id", MUT.getDeviceId(ChooseTypeActivity.this));

            ConnectionPresenter connectionPresenter = new ConnectionPresenter(ChooseTypeActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    loadingBar.cancel();

                    try {
                        grand.healthplus.models.doctormodels.registrationmodel.SignUpResponse signUpResponse = new Gson().fromJson(response.toString(), grand.healthplus.models.doctormodels.registrationmodel.SignUpResponse.class);
                        if (signUpResponse.getState() == ConnectionPresenter.SUCCESS) {
                            signUpResponse.getUser().get(0).setPassword(userItem.getPassword() + "");
                            DoctorSharedPreferenceHelper.saveUserDetails(ChooseTypeActivity.this, signUpResponse.getUser().get(0));
                            MUT.startActivity(ChooseTypeActivity.this, DoctorMainActivity.class);
                        } else if (signUpResponse.getState() == ConnectionPresenter.WRONG_PASSWORD) {
                            MUT.startActivity(ChooseTypeActivity.this, DoctorSignUpActivity.class);
                            DoctorSharedPreferenceHelper.clearUserDetails(ChooseTypeActivity.this);
                        }
                        finish();

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {
                    loadingBar.cancel();
                }
            });
            connectionPresenter.doctorConnect(params, false, false);
        } catch (Exception e) {

                loadingBar.cancel();

            e.getStackTrace();
        }

    }


    private void getGCMTokenThenLogin2() {
        try {
            loadingBar.show();
        } catch (Exception e) {
            e.getStackTrace();
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                InstanceID instanceID = InstanceID.getInstance(ChooseTypeActivity.this);
                try {
                    instanceID.deleteInstanceID();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    googleId = instanceID.getToken(SENDER_ID,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void ok) {

                grand.healthplus.models.doctormodels.registrationmodel.UserItem userItem = DoctorSharedPreferenceHelper.getUserDetails(ChooseTypeActivity.this);
                if (!DoctorSharedPreferenceHelper.isRemember(ChooseTypeActivity.this) || userItem == null) {
                    MUT.startActivity(ChooseTypeActivity.this, DoctorSignInActivity.class);
                } else {
                    logIn2();
                }
            }

        }.execute();

    }

    private void logIn() {
        final UserItem userItem = SharedPreferenceHelper.getUserDetails(ChooseTypeActivity.this);

        JSONObject params = new JSONObject();
        try {
            params.put("email", userItem.getEmail() + "");
            params.put("password", userItem.getPassword() + "");
            params.put("google_id", googleId + "");
            params.put("device_id", MUT.getDeviceId(ChooseTypeActivity.this));
            params.put("method", "login");


            ConnectionPresenter connectionPresenter = new ConnectionPresenter(ChooseTypeActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {

                    try {
                        SignUpResponse signUpResponse = new Gson().fromJson(response.toString(), SignUpResponse.class);
                        if (signUpResponse.getState() == ConnectionPresenter.SUCCESS) {
                            signUpResponse.getUser().get(0).setPassword(userItem.getPassword() + "");
                            SharedPreferenceHelper.saveUserDetails(ChooseTypeActivity.this, signUpResponse.getUser().get(0));
                            MUT.startActivity(ChooseTypeActivity.this, ChooseDepartmentActivity.class);
                        } else if (signUpResponse.getState() == ConnectionPresenter.WRONG_PASSWORD) {
                            MUT.startActivity(ChooseTypeActivity.this, ChooseDepartmentActivity.class);
                            SharedPreferenceHelper.clearUserDetails(ChooseTypeActivity.this);
                        }
                        finish();

                    } catch (Exception e) {
                        Toast.makeText(ChooseTypeActivity.this, "" + e.getStackTrace() + " " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }
            });
            connectionPresenter.connect(params, false, false);
        } catch (Exception e) {
            e.getStackTrace();
        }

    }

    private void getGCMTokenThenLogin() {
        loadingBar.show();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                InstanceID instanceID = InstanceID.getInstance(ChooseTypeActivity.this);
                try {
                    instanceID.deleteInstanceID();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    googleId = instanceID.getToken(SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void ok) {
                logIn();
            }

        }.execute();

    }


}
