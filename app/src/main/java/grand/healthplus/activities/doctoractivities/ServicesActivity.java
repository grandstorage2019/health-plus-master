package grand.healthplus.activities.doctoractivities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.doctoradapters.ServicesAdapter;
import grand.healthplus.models.doctormodels.ServiceResponse;
import grand.healthplus.models.doctormodels.ServicesItem;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class ServicesActivity extends AppCompatActivity {

    @BindView(R.id.rl_services_collection)
    RecyclerView servicesRecyclerView;
    @BindView(R.id.tv_actionbar_save)
    TextView saveTextView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    @BindView(R.id.iv_services_add)
    ImageView addServiceImageView;

    ArrayList<ServicesItem>servicesItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);

        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
        setEvent();
    }


    private void setEvent() {
        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveServices();
            }
        });
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        addServiceImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(ServicesActivity.this,NewServiceActivity.class);
            }
        });
    }


    @Override
    protected void onResume() {
        getServices();
        super.onResume();
    }

    private void saveServices() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "edit_services");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());

            String addedServices="";
            boolean firstAdded = false;

            for(int i=0; i<servicesItems.size(); i++){

                if(servicesItems.get(i).isChecked()) {
                    if (firstAdded)
                        addedServices += ",";
                    addedServices += servicesItems.get(i).getId();
                    firstAdded = true;
                }

            }

            jsonObject.put("services", addedServices);

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);

                if(defaultResponse.getState()==ConnectionPresenter.SUCCESS){
                    finish();
                    MUT.lToast(ServicesActivity.this,getResources().getString(R.string.updated));
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }





    private void getServices() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "services");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ServiceResponse servicesResponse = new Gson().fromJson(response.toString(), ServiceResponse.class);
                if (servicesResponse.getState() == ConnectionPresenter.SUCCESS) {
                    servicesItems = new ArrayList<>(servicesResponse.getServices());
                    ServicesAdapter servicesAdapter = new ServicesAdapter(servicesItems, ServicesActivity.this);
                    ViewOperations.setRVHVertical(ServicesActivity.this, servicesRecyclerView);
                    servicesRecyclerView.setAdapter(servicesAdapter);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }
}
