package grand.healthplus.activities.doctoractivities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.doctoradapters.MedicalSubSpecializationsAdapter;
import grand.healthplus.adapters.doctoradapters.PlansAdapter;
import grand.healthplus.models.doctormodels.PlanResponse;
import grand.healthplus.models.doctormodels.SpecializationResponse;
import grand.healthplus.models.doctormodels.SpecializationsItem;
import grand.healthplus.models.doctormodels.SubSpecializationResponse;
import grand.healthplus.models.doctormodels.SubSpecializationsItem;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class MedicalSpecialtiesActivity extends AppCompatActivity {

    @BindView(R.id.rl_medical_specialties_collection)
    RecyclerView medicalSpecialtiesRecyclerView;
    @BindView(R.id.tv_medical_specialties)
    TextView medicalSpecializationTextView;
    @BindView(R.id.tv_actionbar_save)
    TextView saveTextView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;
    @BindView(R.id.ll_medical_specialties_specialization)
    LinearLayout specializationLayout;

    PopupMenu countryPopUp = null;
    ArrayList<SpecializationsItem> specialzationItems;
    ArrayList<SubSpecializationsItem>subSpecializationsItems;
    int selectedService = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_specialties);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);

        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }

        getSpecializations();
        setEvent();
    }


    private void setEvent() {
        medicalSpecializationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSpecialtiesSpinner();
            }
        });

        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveSubSpecializations();
            }
        });

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }



    private void saveSubSpecializations() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "edit_specializations");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());

            String addedServices="";

            boolean firstAdded = false;

            for(int i=0; i<subSpecializationsItems.size(); i++){
                if(subSpecializationsItems.get(i).isChecked()) {
                    if (firstAdded)
                        addedServices += ",";
                    addedServices += subSpecializationsItems.get(i).getId();
                    firstAdded=true;
                }

            }
            
            jsonObject.put("specializations", addedServices);
            jsonObject.put("specialization_id", selectedService);

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);

                if(defaultResponse.getState()==ConnectionPresenter.SUCCESS){
                    MUT.lToast(MedicalSpecialtiesActivity.this,getResources().getString(R.string.updated));
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }

    private void showSpecialtiesSpinner() {

        if (countryPopUp == null) {
            countryPopUp = new PopupMenu(this, specializationLayout);
            for (int i = 0; i < specialzationItems.size(); i++) {
                countryPopUp.getMenu().add(i, i, i, specialzationItems.get(i).getName());
            }

            int selectedPosition = -1;
            int selectedSpecialization = getIntent().getIntExtra("specialization_id",0);



            for(int i=0; i<specialzationItems.size(); i++){
                if(selectedSpecialization==specialzationItems.get(i).getId()){
                    selectedPosition = i ;
                    break;
                }
            }

            if(selectedPosition!=-1){
                medicalSpecializationTextView.setOnClickListener(null);
            }

            selectedPosition = selectedPosition==-1?0:selectedPosition;

                selectedService = specialzationItems.get(selectedPosition).getId();
                medicalSpecializationTextView.setText(specialzationItems.get(selectedPosition).getName());


            getSubSpecializations();
            countryPopUp.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    selectedService = specialzationItems.get(item.getItemId()).getId();
                    medicalSpecializationTextView.setText(specialzationItems.get(item.getItemId()).getName());
                    medicalSpecialtiesRecyclerView.setAdapter(null);
                    getSubSpecializations();
                    return false;
                }
            });
        } else {
            countryPopUp.show();
        }
    }

    private void getSpecializations() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "specializations");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                SpecializationResponse specializationResponse = new Gson().fromJson(response.toString(), SpecializationResponse.class);
                if (specializationResponse.getState() == ConnectionPresenter.SUCCESS) {
                    specialzationItems = new ArrayList<>(specializationResponse.getSpecializations());


                    showSpecialtiesSpinner();
                }
            }
            @Override
            public void onRequestError(Object error) {
            }
        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }


    private void getSubSpecializations() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "sub_sepcializations");
            jsonObject.put("specialization_id", selectedService);
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                SubSpecializationResponse subSpecializationResponse = new Gson().fromJson(response.toString(), SubSpecializationResponse.class);
                if (subSpecializationResponse.getState() == ConnectionPresenter.SUCCESS) {
                    subSpecializationsItems = new ArrayList<>(subSpecializationResponse.getSubSepcializations());
                    MedicalSubSpecializationsAdapter medicalSubSpecializationsAdapter = new MedicalSubSpecializationsAdapter(subSpecializationsItems, MedicalSpecialtiesActivity.this);
                    ViewOperations.setRVHVertical(MedicalSpecialtiesActivity.this, medicalSpecialtiesRecyclerView);
                    medicalSpecialtiesRecyclerView.setAdapter(medicalSubSpecializationsAdapter);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }
}
