package grand.healthplus.activities.doctoractivities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.doctoradapters.InsuranceCompaniesAdapter;
import grand.healthplus.models.doctormodels.InsuranceCompaniesItem;
import grand.healthplus.models.doctormodels.InsuranceCompanyResponse;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class InsuranceCompaniesActivity extends AppCompatActivity {

    @BindView(R.id.rl_insurance_companies_collection)
    RecyclerView insuranceCompaniesRecyclerView;
    @BindView(R.id.tv_actionbar_save)
    TextView saveTextView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    ArrayList<InsuranceCompaniesItem>insuranceCompaniesItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance_companies);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
        getInsuranceCompanies();
        setEvent();
    }


    private void setEvent() {
        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveInsuranceCompanies();
            }
        });

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }



    private void saveInsuranceCompanies() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "edit_insurance_companies");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());

            String addedServices="";
            boolean firstAdded = false;

            for(int i=0; i<insuranceCompaniesItems.size(); i++){

                if(insuranceCompaniesItems.get(i).isChecked()) {
                    if (firstAdded)
                        addedServices += ",";
                    addedServices += insuranceCompaniesItems.get(i).getId();
                    firstAdded = true;
                }

            }

            jsonObject.put("insurance_companies", addedServices);

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);

                if(defaultResponse.getState()==ConnectionPresenter.SUCCESS){
                    MUT.lToast(InsuranceCompaniesActivity.this,getResources().getString(R.string.updated));
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }





    private void getInsuranceCompanies() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "insurance_companies");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                InsuranceCompanyResponse insuranceCompanyResponse = new Gson().fromJson(response.toString(), InsuranceCompanyResponse.class);
                if (insuranceCompanyResponse.getState() == ConnectionPresenter.SUCCESS) {
                    insuranceCompaniesItems = new ArrayList<>(insuranceCompanyResponse.getInsuranceCompanies());
                    InsuranceCompaniesAdapter insuranceCompaniesAdapter = new InsuranceCompaniesAdapter(insuranceCompaniesItems, InsuranceCompaniesActivity.this);
                    ViewOperations.setRVHVertical(InsuranceCompaniesActivity.this, insuranceCompaniesRecyclerView);
                    insuranceCompaniesRecyclerView.setAdapter(insuranceCompaniesAdapter);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }
}
