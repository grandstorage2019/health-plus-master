package grand.healthplus.activities.doctoractivities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.DefaultResponse;
import grand.healthplus.models.doctormodels.registrationmodel.SignUpResponse;
import grand.healthplus.utils.Check;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class NewServiceActivity extends AppCompatActivity {

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;


    @BindView(R.id.tv_actionbar_save)
    TextView saveTextView;
    @BindView(R.id.et_new_service_name)
    EditText serviceNameEditText;
    @BindView(R.id.et_new_service_arabic_name)
    EditText arabicServiceNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_service);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
        setEvent();
    }


    private void setEvent() {
        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addService();
            }
        });
    }

    private boolean validate() {
        if (Check.isEmpty(serviceNameEditText.getText() + "")||Check.isEmpty(arabicServiceNameEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.fill_data));
            return false;
        }
        return true;
    }

    private void addService() {
        if (validate()) {
            JSONObject sentData = new JSONObject();
            try {
                sentData.put("method", "add_service");
                sentData.put("name_en",serviceNameEditText.getText());
                sentData.put("name_ar", arabicServiceNameEditText.getText());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {


                    DefaultResponse userResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);

                    if (userResponse.getState() == ConnectionPresenter.SUCCESS) {
                        Toast.makeText(NewServiceActivity.this, ""+getResources().getString(R.string.service_added), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }

            });
            connectionPresenter.doctorConnect(sentData, true, false);
        }
    }


}
