package grand.healthplus.activities.doctoractivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hplusactivities.DaysActivity;
import grand.healthplus.adapters.doctoradapters.PlansAdapter;
import grand.healthplus.adapters.hplusadapters.DaysTimesAdapter;
import grand.healthplus.models.doctormodels.PlanResponse;
import grand.healthplus.models.hplusmodels.DaysTimesModel;
import grand.healthplus.models.hplusmodels.DoctorInfoResponse;
import grand.healthplus.models.hplusmodels.TimesItem;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

/**
 * Created by sameh on 5/2/18.
 */

public class PlansActivity extends AppCompatActivity {


    @BindView(R.id.rv_plans_collection)
    RecyclerView plansRecyclerView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;
    String language = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plans);
        ButterKnife.bind(PlansActivity.this);
        MUT.setSelectedLanguage(this);
        language = SharedPreferenceHelper.getCurrentLanguage(this);
        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }
        getPlans();
    }


    public void goBack(View view){
        finish();
    }

    private void getPlans() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "plans");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                PlanResponse planResponse = new Gson().fromJson(response.toString(), PlanResponse.class);
                if (planResponse.getState() == ConnectionPresenter.SUCCESS) {
                    PlansAdapter plansAdapter = new PlansAdapter(new ArrayList<>(planResponse.getPlans()), PlansActivity.this);
                    ViewOperations.setRVHVertical(PlansActivity.this, plansRecyclerView);
                    plansRecyclerView.setAdapter(plansAdapter);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true , false);
    }

}