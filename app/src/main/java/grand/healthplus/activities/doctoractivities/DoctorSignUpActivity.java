package grand.healthplus.activities.doctoractivities;


import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.SignInActivity;
import grand.healthplus.activities.SignUpActivity;
import grand.healthplus.models.doctormodels.registrationmodel.SignUpResponse;
import grand.healthplus.models.doctormodels.registrationmodel.UserItem;
import grand.healthplus.models.hplusmodels.CountriesItem;
import grand.healthplus.models.hplusmodels.SpecializationResponse;
import grand.healthplus.models.hplusmodels.SpecializationsItem;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.utils.Check;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

/**
 * Created by sameh on 5/2/18.
 */

public class DoctorSignUpActivity extends AppCompatActivity {
    public static final String SENDER_ID = "306498002272";
    String googleId = "";
    Dialog loadingBar;

    @BindView(R.id.tv_sign_up_title)
    TextView titleTextView;
    @BindView(R.id.et_doctor_sign_up_first_name)
    EditText firstNameEditText;
    @BindView(R.id.et_doctor_sign_up_last_name)
    EditText lastNameEditText;
    @BindView(R.id.et_doctor_sign_up_password)
    EditText passwordEditText;
    @BindView(R.id.et_doctor_sign_up_cpassword)
    EditText cPasswordEditText;
    @BindView(R.id.et_doctor_sign_up_birth_date)
    EditText birthDayEditText;
    @BindView(R.id.et_doctor_sign_up_gender)
    EditText genderEditText;
    @BindView(R.id.et_doctor_sign_up_email)
    EditText emailEditText;
    @BindView(R.id.rl_doctor_sign_up_country)
    RelativeLayout countrySpinner;
    @BindView(R.id.tv_doctor_sign_up_country)
    TextView countryText;
    @BindView(R.id.rl_doctor_sign_up_specialty)
    RelativeLayout specialtySpinner;
    @BindView(R.id.tv_doctor_sign_up_specialty)
    TextView specialtyText;
    @BindView(R.id.et_doctor_sign_up_mobile)
    EditText mobileEditText;
    @BindView(R.id.rl_doctor_sign_up_have_account)
    RelativeLayout haveAccountLayout;
    @BindView(R.id.btn_doctor_sign_up_enter)
    Button enterBtn;

    boolean isEdit = false;
    UserItem userItem;
    int selectedSpecialty = -1, selectedCountry = -1;
    Dialog countryDialog = null, specialtyDialog = null, genderDialog = null;
    ArrayList<SpinnerModel> countryItems;
    ArrayList<SpinnerModel> genderItems, specialtyItems;
    String language = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_sign_up);
        ButterKnife.bind(DoctorSignUpActivity.this);
        MUT.setSelectedLanguage(this);
        language = SharedPreferenceHelper.getCurrentLanguage(this);
        setEvents();

        isEdit = getIntent().getBooleanExtra("isEdit", false);

        if (isEdit) {
            enterBtn.setText(getResources().getString(R.string.update));
            titleTextView.setText(getResources().getString(R.string.edit_profile));
            haveAccountLayout.setVisibility(View.GONE);
            setUserDetails();
        }

    }


    private void setEvents() {
        enterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    getGCMTokenThenLogin();
                }
            }
        });

        haveAccountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(DoctorSignUpActivity.this, DoctorSignInActivity.class);
            }
        });

        countrySpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (countryDialog != null) {
                    countryDialog.show();
                } else {

                    getCountries();
                }
            }
        });

        specialtySpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSpecializations();
            }
        });

        birthDayEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });

        genderEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGenderSpinner();
            }
        });
    }


    private void getGCMTokenThenLogin() {
        loadingBar = MUT.createLoadingBar(this);
        loadingBar.show();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                InstanceID instanceID = InstanceID.getInstance(DoctorSignUpActivity.this);
                try {
                    instanceID.deleteInstanceID();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    googleId = instanceID.getToken(SENDER_ID,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void ok) {
                signUp();
            }

        }.execute();

    }


    private boolean validate() {
        if (Check.isEmpty(firstNameEditText.getText() + "")
                || Check.isEmpty(lastNameEditText.getText() + "")
                || Check.isEmpty(passwordEditText.getText() + "")
                || Check.isEmpty(cPasswordEditText.getText() + "")
                || Check.isEmpty(mobileEditText.getText() + "")
                || Check.isEmpty(birthDayEditText.getText() + "")
                || Check.isEmpty(genderEditText.getText() + "")
                || Check.isEmpty(emailEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.fill_data));
            return false;
        } else if (!Check.isEqual(passwordEditText.getText() + "", cPasswordEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.passwords_not_matches));
            return false;
        } else if (!Check.isMail(emailEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.wrong_email));
            return false;
        } else if (selectedCountry == -1) {
            MUT.lToast(this, getResources().getString(R.string.select_country));
            return false;
        }

        return true;
    }


    private void signUp() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1000);
        } else {
            goSignUp();
        }
    }

    private void goSignUp() {
        if (validate()) {
            JSONObject sentData = new JSONObject();
            try {
                sentData.put("method", "register");

                if (isEdit) {
                    sentData.put("method", "update");
                    sentData.put("id", userItem.getId());
                }

                sentData.put("fname_en", firstNameEditText.getText() + "");
                sentData.put("lname_en", lastNameEditText.getText() + "");
                sentData.put("password", passwordEditText.getText() + "");
                sentData.put("birth_day", birthDayEditText.getText() + "");
                sentData.put("gender", selectedGender == 0 ? "male" : "female");
                sentData.put("email", emailEditText.getText() + "");
                sentData.put("google_id", googleId + "");
                sentData.put("phone", mobileEditText.getText() + "");
                sentData.put("specialization_id", selectedSpecialty);
                sentData.put("device_id", MUT.getDeviceId(DoctorSignUpActivity.this));
                sentData.put("country_id", selectedCountry);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    loadingBar.cancel();
                    SignUpResponse userResponse = new Gson().fromJson(response.toString(), SignUpResponse.class);
                    if (userResponse.getState() == ConnectionPresenter.SUCCESS) {
                        if (isEdit) {
                            MUT.lToast(DoctorSignUpActivity.this, getResources().getString(R.string.updated));
                            userItem = DoctorSharedPreferenceHelper.getUserDetails(DoctorSignUpActivity.this);
                            userItem.setPassword("" + passwordEditText.getText());
                            userItem.setBirthDay("" + birthDayEditText.getText());
                            userItem.setCountryId(selectedCountry);
                            userItem.setSpecializationId(selectedSpecialty);
                            userItem.setEmail("" + emailEditText.getText());
                            userItem.setFnameEn("" + firstNameEditText.getText());
                            userItem.setLnameAr("" + lastNameEditText.getText());
                            userItem.setPhone("" + mobileEditText.getText());
                            if (genderEditText.getText().equals(getResources().getString(R.string.male))) {
                                userItem.setGender("male");
                            } else {
                                userItem.setGender("female");
                            }
                            DoctorSharedPreferenceHelper.saveUserDetails(DoctorSignUpActivity.this, userItem);
                            finish();

                        } else {
                            DoctorSharedPreferenceHelper.saveUserDetails(DoctorSignUpActivity.this, userResponse.getUser().get(0));
                            MUT.startActivity(DoctorSignUpActivity.this, UploadLicenseActivity.class);
                        }
                        finish();
                    } else if (userResponse.getState() == ConnectionPresenter.USED_MAIL) {
                        MUT.lToast(DoctorSignUpActivity.this, getResources().getString(R.string.used_email));
                    } else {
                        System.out.println("Hm " + response.toString());
                    }
                }

                @Override
                public void onRequestError(Object error) {
                    loadingBar.cancel();
                }

            });
            connectionPresenter.doctorConnect(sentData, false, false);
        }
    }

    private void getCountries() {
        countryItems = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "countries");
            jsonObject.put("language", language);
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(DoctorSignUpActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                grand.healthplus.models.hplusmodels.CountryResponse countryResponse = new Gson().fromJson(response.toString(), grand.healthplus.models.hplusmodels.CountryResponse.class);
                List<CountriesItem> data = countryResponse.getCountries();

                for (int i = 0; i < data.size(); i++) {
                    countryItems.add(new SpinnerModel(data.get(i).getId(), data.get(i).getName()));
                }


                countryDialog = ViewOperations.setSpinnerDialog(DoctorSignUpActivity.this, countryItems, getResources().getString(R.string.countries), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedCountry = countryItems.get(position).getId();
                        countryText.setText(countryItems.get(position).getTitle());

                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }

    private void getSpecializations() {
        specialtyItems = new ArrayList<>();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "specializations");
            jsonObject.put("language", language);
        } catch (Exception e) {
            e.getStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(DoctorSignUpActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                SpecializationResponse specializationResponse = new Gson().fromJson(response.toString(), SpecializationResponse.class);
                List<SpecializationsItem> specializationItems = specializationResponse.getSpecializations();

                for (int i = 0; i < specializationItems.size(); i++) {
                    specialtyItems.add(new SpinnerModel(specializationItems.get(i).getId(), specializationItems.get(i).getName()));
                }
                specialtyDialog = ViewOperations.setSpinnerDialog(DoctorSignUpActivity.this, specialtyItems, getResources().getString(R.string.specialty), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedSpecialty = specialtyItems.get(position).getId();
                        specialtyText.setText(specialtyItems.get(position).getTitle());
                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }

    private void selectDate() {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH) + 1;
        final DatePickerDialog datePickerDialog = new DatePickerDialog(DoctorSignUpActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                String selectedDate = i + "-" + (++i1) + "-" + i2;
                birthDayEditText.setText(selectedDate);
            }
        }, year, month, 0);

        datePickerDialog.show();
    }

    int selectedGender = -1;

    private void setGenderSpinner() {
        if (genderDialog == null) {
            genderItems = new ArrayList<>();
            genderItems.add(new SpinnerModel(0, getResources().getString(R.string.male)));
            genderItems.add(new SpinnerModel(1, getResources().getString(R.string.female)));
            genderDialog = ViewOperations.setSpinnerDialog(DoctorSignUpActivity.this, genderItems, getResources().getString(R.string.gender), new SpinnerListener() {
                @Override
                public void onItemClicked(int position) {
                    genderEditText.setText(genderItems.get(position).getTitle());
                    selectedGender = genderItems.get(position).getId();
                }
            });
        } else {
            genderDialog.show();
        }
    }


    private void setUserDetails() {
        userItem = DoctorSharedPreferenceHelper.getUserDetails(DoctorSignUpActivity.this);
        firstNameEditText.setText(userItem.getFnameEn());
        lastNameEditText.setText(userItem.getLnameEn());
        passwordEditText.setText(userItem.getPassword());
        birthDayEditText.setText(userItem.getBirthDay());
        genderEditText.setText(getResources().getString(userItem.getGender().equals("male") ? R.string.male : R.string.female));
        emailEditText.setText(userItem.getEmail());
        mobileEditText.setText(userItem.getPhone());
        firstNameEditText.setEnabled(false);
        lastNameEditText.setEnabled(false);
        passwordEditText.setText(userItem.getPassword());
        birthDayEditText.setEnabled(false);
        genderEditText.setEnabled(false);
        emailEditText.setText(userItem.getEmail());
        mobileEditText.setEnabled(false);
        countrySpinner.setEnabled(false);
        specialtySpinner.setEnabled(false);
        enterBtn.setText(getResources().getString(R.string.update));
        selectedCountry = userItem.getCountryId();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            goSignUp();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1000);
        }

    }

}