package grand.healthplus.activities.doctoractivities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;


public class AddInsuranceCompanyActivity extends AppCompatActivity {
    @BindView(R.id.tv_add_insurance_company_add)
    TextView addInsuranceCompanyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);

        setEvent();

    }


    private void setEvent() {
        addInsuranceCompanyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(AddInsuranceCompanyActivity.this,ServicesActivity.class);
            }
        });
    }




}
