package grand.healthplus.activities.doctoractivities;


import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.fragments.doctorfragments.AboutDoctorFragment;
import grand.healthplus.fragments.doctorfragments.AddPatientFragment;
import grand.healthplus.fragments.doctorfragments.ReservationsFragment;
import grand.healthplus.fragments.doctorfragments.SettingsFragment;
import grand.healthplus.trash.AppointmentFragment;
import grand.healthplus.trash.DoctorDetailsFragment;
import grand.healthplus.utils.FragmentHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;


public class DoctorMainActivity extends AppCompatActivity {


    @BindView(R.id.ll_main_doctor_info)
    LinearLayout doctorInfoLayout;
    @BindView(R.id.ll_main_patient)
    LinearLayout patientLayout;
    @BindView(R.id.ll_main_appointment)
    LinearLayout appointmentLayout;
    @BindView(R.id.ll_main_more)
    LinearLayout moreLayout;
    @BindView(R.id.iv_main_doctor_info)
    ImageView doctorInfoImageView;
    @BindView(R.id.iv_main_patient)
    ImageView appointmentImageView;
    @BindView(R.id.iv_main_appointment)
    ImageView patientImageView;
    @BindView(R.id.iv_main_more)
    ImageView moreImageView;
    @BindView(R.id.tv_main_title)
    TextView titleTextView;
    @BindView(R.id.iv_main_message)
    ImageView messagesImageView;
    @BindView(R.id.iv_main_menu)
    ImageView backImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_main);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);

        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
        FragmentHelper.addFragment(this, new AboutDoctorFragment(), "AboutDoctorFragment");
        setEvents();
    }


    private void aboutDoctorFragment(){
        titleTextView.setText(getResources().getString(R.string.about_dr));
        FragmentHelper.popLastFragment(DoctorMainActivity.this);
        FragmentHelper.addFragment(DoctorMainActivity.this, new AboutDoctorFragment(), "AboutDoctorFragment");
        setDefaults();
    }
    private void setEvents() {
        doctorInfoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aboutDoctorFragment();
            }
        });

        patientLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                titleTextView.setText(getResources().getString(R.string.patients));
                FragmentHelper.popLastFragment(DoctorMainActivity.this);
                FragmentHelper.addFragment(DoctorMainActivity.this, new AddPatientFragment(), "AddPatientFragment");
                doctorInfoImageView.setImageResource(R.mipmap.ic_un_selected_doctors);
                appointmentImageView.setImageResource(R.mipmap.ic_selected_patient);
                patientImageView.setImageResource(R.mipmap.ic_un_colored_appointment);
                moreImageView.setImageResource(R.mipmap.ic_un_selected_more);
            }
        });

        appointmentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titleTextView.setText(getResources().getString(R.string.appoinments));
                FragmentHelper.popLastFragment(DoctorMainActivity.this);
                FragmentHelper.addFragment(DoctorMainActivity.this, new ReservationsFragment(), "ReservationsFragment");
                doctorInfoImageView.setImageResource(R.mipmap.ic_un_selected_doctors);
                appointmentImageView.setImageResource(R.mipmap.ic_un_selected_patient);
                patientImageView.setImageResource(R.mipmap.ic_colored_appointment);
                moreImageView.setImageResource(R.mipmap.ic_un_selected_more);
            }
        });

        moreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titleTextView.setText(getResources().getString(R.string.more));
                FragmentHelper.popLastFragment(DoctorMainActivity.this);
                FragmentHelper.addFragment(DoctorMainActivity.this, new SettingsFragment(), "SettingsFragment");
                doctorInfoImageView.setImageResource(R.mipmap.ic_un_selected_doctors);
                appointmentImageView.setImageResource(R.mipmap.ic_un_selected_patient);
                patientImageView.setImageResource(R.mipmap.ic_un_colored_appointment);
                moreImageView.setImageResource(R.mipmap.ic_selected_more);
            }
        });


    }



    public void goBack(View view){
        aboutDoctorFragment();
    }

    boolean doubleBackToExitPressedOnce = true;

    @Override
    public void onBackPressed() {


        if (!doubleBackToExitPressedOnce) {
            finish();
        } else {

            this.doubleBackToExitPressedOnce = false;
            Toast.makeText(this, getResources().getString(R.string.double_click_to_close), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = true;
                }
            }, 2000);
            setDefaults();
            titleTextView.setText(getResources().getString(R.string.about_doctor));
            FragmentHelper.popLastFragment(DoctorMainActivity.this);
            FragmentHelper.addFragment(DoctorMainActivity.this, new AboutDoctorFragment(), "AboutDoctorFragment");
        }

    }

    private void setDefaults() {
        doctorInfoImageView.setImageResource(R.mipmap.ic_selected_doctors);
        appointmentImageView.setImageResource(R.mipmap.ic_un_selected_patient);
        patientImageView.setImageResource(R.mipmap.ic_un_colored_appointment);
        moreImageView.setImageResource(R.mipmap.ic_un_selected_more);
    }


}
