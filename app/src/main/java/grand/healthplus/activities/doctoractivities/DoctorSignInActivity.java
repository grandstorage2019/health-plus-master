package grand.healthplus.activities.doctoractivities;


import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;
import org.json.JSONObject;
import java.io.IOException;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ChooseDepartmentActivity;
import grand.healthplus.activities.ForgotPasswordActivity;
import grand.healthplus.activities.SignUpActivity;
import grand.healthplus.models.doctormodels.registrationmodel.SignUpResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class DoctorSignInActivity extends AppCompatActivity {
    public static final String SENDER_ID = "306498002272";

    @BindView(R.id.et_sign_in_email)
    EditText emailEditText;
    @BindView(R.id.et_sign_in_password)
    EditText passwordEditText;
    @BindView(R.id.btn_sign_in_login)
    Button signInBtn;

    @BindView(R.id.tv_sign_in_forgot_password)
    TextView forgotPasswordTextView;

    @BindView(R.id.rl_have_account)
    RelativeLayout haveAccountLayout;

    @BindView(R.id.tv_sign_in_remember_me)
    TextView rememberMeTextView;

    String googleId = "";

    boolean rememberMe=false;

    Dialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(DoctorSignInActivity.this);
        MUT.setSelectedLanguage(this);
        setEvents();
    }

    private void setEvents(){
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getGCMTokenThenLogin();
            }
        });

        rememberMeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rememberMe) {
                    rememberMeTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_remember_un_selected, 0, 0, 0);
                }
                else {
                    rememberMeTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_remeber_selected, 0, 0, 0);
                }
                rememberMe=!rememberMe;
            }
        });

        haveAccountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                MUT.startActivity(DoctorSignInActivity.this,DoctorSignUpActivity.class);
            }
        });

        forgotPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(DoctorSignInActivity.this, grand.healthplus.activities.doctoractivities.ForgotPasswordActivity.class);
            }
        });
    }

    private void logIn() {
        JSONObject params = new JSONObject();
        try {
            params.put("method", "login");
            params.put("email", emailEditText.getText() + "");
            params.put("password", passwordEditText.getText() + "");
            params.put("google_id", googleId + "");
            params.put("device_id", MUT.getDeviceId(DoctorSignInActivity.this));

            } catch (Exception e) {
            e.getStackTrace();
             }
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(DoctorSignInActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    loadingBar.cancel();
                    try {
                        SignUpResponse signUpResponse = new Gson().fromJson(response.toString(), SignUpResponse.class);
                        if (signUpResponse.getState() == ConnectionPresenter.SUCCESS) {
                            signUpResponse.getUser().get(0).setPassword(passwordEditText.getText() + "");
                            DoctorSharedPreferenceHelper.setRememberMe(DoctorSignInActivity.this,rememberMe);
                            DoctorSharedPreferenceHelper.saveUserDetails(DoctorSignInActivity.this, signUpResponse.getUser().get(0));
                            finishAffinity();
                            MUT.startActivity(DoctorSignInActivity.this, DoctorMainActivity.class);
                        }else if (signUpResponse.getState() == ConnectionPresenter.WRONG_PASSWORD) {
                            MUT.lToast(DoctorSignInActivity.this,getResources().getString(R.string.wrong_password));
                        }

                    } catch (Exception e) {

                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {
                    loadingBar.cancel();
                }
            });
            connectionPresenter.doctorConnect(params, true, false);


    }


    private void getGCMTokenThenLogin() {
        loadingBar = MUT.createLoadingBar(this);
        loadingBar.show();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                InstanceID instanceID = InstanceID.getInstance(DoctorSignInActivity.this);
                try {
                    instanceID.deleteInstanceID();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    googleId = instanceID.getToken(SENDER_ID,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void ok) {
                logIn();
            }

        }.execute();

    }


}
