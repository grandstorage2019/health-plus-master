package grand.healthplus.activities.doctoractivities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.doctormodels.InsuranceCompaniesItem;
import grand.healthplus.models.doctormodels.InsuranceCompanyResponse;
import grand.healthplus.models.doctormodels.registrationmodel.SignUpResponse;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.utils.Check;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class PatientsActivity extends AppCompatActivity {



    @BindView(R.id.tv_actionbar_save)
    TextView saveTextView;
    @BindView(R.id.et_patient_data_name)
    EditText nameEditText;
    @BindView(R.id.et_patient_data_mobile_number)
    EditText mobileEditText;
    @BindView(R.id.et_patient_data_patient_number)
    EditText numberEditText;
    @BindView(R.id.et_patient_data_patient_email)
    EditText emailEditText;
    @BindView(R.id.rg_patient_data_gender)
    RadioGroup genderRadioGroup;
    @BindView(R.id.et_patient_data_date)
    EditText dateEditText;
    @BindView(R.id.et_patient_data_identification_number)
    EditText identificationNumberEditText;
    @BindView(R.id.et_patient_data_notes)
    EditText notesEditText;
    @BindView(R.id.et_patient_data_insurance_company)
    EditText insuranceCompanyEditText;
    @BindView(R.id.tv_patient_data_link)
    TextView patientLinkTextView;

    @BindView(R.id.switch_patient_data_add_insurance)
    Switch addInsuranceSwitch;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    int selectedInsurance = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_data);
        ButterKnife.bind(PatientsActivity.this);
        MUT.setSelectedLanguage(this);

        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }

        setEvents();

    }

    private void setEvents() {
        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPatient();
            }
        });

        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });

        insuranceCompanyEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (insuranceCompaniesDialog == null) {
                    getInsuranceCompanies();
                } else {
                    insuranceCompaniesDialog.show();
                }

            }
        });
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addInsuranceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!b)selectedInsurance = 0;
                insuranceCompanyEditText.setVisibility(b?View.VISIBLE:View.GONE);
            }
        });

        patientLinkTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLink();
            }
        });
    }

    PopupMenu linkPopUp=null;
    ArrayList<SpinnerModel>linksList;

    private void setLink() {
        if(linkPopUp==null) {
            linksList = new ArrayList<>();

            linksList.add(new SpinnerModel(0,getResources().getString(R.string.husband)));
            linksList.add(new SpinnerModel(0,getResources().getString(R.string.wife)));
            linksList.add(new SpinnerModel(0,getResources().getString(R.string.mother)));
            linksList.add(new SpinnerModel(0,getResources().getString(R.string.father)));
            linksList.add(new SpinnerModel(0,getResources().getString(R.string.sister)));
            linksList.add(new SpinnerModel(0,getResources().getString(R.string.brother)));
            linksList.add(new SpinnerModel(0,getResources().getString(R.string.other)));

            linkPopUp = new PopupMenu(PatientsActivity.this, patientLinkTextView);
            for (int i = 0; i < linksList.size(); i++) {
                linkPopUp.getMenu().add(i, i, i, linksList.get(i).getTitle());
            }

            linkPopUp.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    patientLinkTextView.setText(linksList.get(item.getItemId()).getTitle());
                    return false;
                }
            });
        }
        else {
            linkPopUp.show();
        }

    }


    private boolean validate() {
        if (Check.isEmpty(nameEditText.getText() + "")
                || Check.isEmpty(mobileEditText.getText() + "")
                || Check.isEmpty(numberEditText.getText() + "")
                || Check.isEmpty(emailEditText.getText() + "")
                || Check.isEmpty(mobileEditText.getText() + "")
                || Check.isEmpty(dateEditText.getText() + "")
                || Check.isEmpty(identificationNumberEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.fill_data));
            return false;
        } else if (!Check.isMail(emailEditText.getText() + "")) {
            MUT.lToast(this, getResources().getString(R.string.wrong_email));
            return false;
        }
        return true;
    }


    private void addPatient() {
        if (validate()) {
            JSONObject sentData = new JSONObject();
            try {
                sentData.put("method", "add_patient");
                sentData.put("name", nameEditText.getText() + "");
                sentData.put("phone", mobileEditText.getText() + "");
                sentData.put("password", mobileEditText.getText() + "");
                sentData.put("birth_day", dateEditText.getText() + "");
                sentData.put("notes",notesEditText.getText()+"");
                sentData.put("gender", (genderRadioGroup.getCheckedRadioButtonId() == R.id.rb_patient_data_male) ? "male" : "female");
                sentData.put("email", emailEditText.getText() + "");
                sentData.put("device_id", identificationNumberEditText.getText() + "");
                sentData.put("insurance_id", selectedInsurance);
                sentData.put("added_by_doctor_id", DoctorSharedPreferenceHelper.getUserDetails(PatientsActivity.this).getId());
                ;

            } catch (JSONException e) {
                e.printStackTrace();
            }
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {


                    SignUpResponse userResponse = new Gson().fromJson(response.toString(), SignUpResponse.class);

                    if (userResponse.getState() == ConnectionPresenter.SUCCESS) {
                        finish();
                    } else if (userResponse.getState() == ConnectionPresenter.USED_MAIL) {
                        MUT.lToast(PatientsActivity.this, getResources().getString(R.string.used_email));
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }

            });
            connectionPresenter.doctorConnect(sentData, true, false);
        }
    }


    private void selectDate() {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH) + 1;
        final DatePickerDialog datePickerDialog = new DatePickerDialog(PatientsActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                String selectedDate = i + "-" + (++i1) + "-" + i2;
                dateEditText.setText(selectedDate);
            }
        }, year, month, 0);

        datePickerDialog.show();
    }

    Dialog insuranceCompaniesDialog = null;
    ArrayList<SpinnerModel> insuranceCompaniesItems;

    private void getInsuranceCompanies() {
        insuranceCompaniesItems = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "insurances");
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(PatientsActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {


                InsuranceCompanyResponse countryResponse = new Gson().fromJson(response.toString(), InsuranceCompanyResponse.class);
                List<InsuranceCompaniesItem> data = countryResponse.getInsuranceCompanies();

                for (int i = 0; i < data.size(); i++) {
                    insuranceCompaniesItems.add(new SpinnerModel(data.get(i).getId(), data.get(i).getName()));
                }


                insuranceCompaniesDialog = ViewOperations.setSpinnerDialog(PatientsActivity.this, insuranceCompaniesItems, getResources().getString(R.string.insurance_company_title), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedInsurance = insuranceCompaniesItems.get(position).getId();
                        insuranceCompanyEditText.setText(insuranceCompaniesItems.get(position).getTitle());

                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }


}