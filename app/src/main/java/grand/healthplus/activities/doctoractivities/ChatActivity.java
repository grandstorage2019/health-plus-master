package grand.healthplus.activities.doctoractivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;


import grand.healthplus.adapters.doctoradapters.ChatAdapter;
import grand.healthplus.models.doctormodels.ChatResponse;
import grand.healthplus.models.doctormodels.ChattersItem;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.rv_chat_collection)
    RecyclerView chatRecycleView;
    @BindView(R.id.et_chat_mesasage)
    EditText messageEditText;
    @BindView(R.id.iv_chat_send)
    ImageView sendImageView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    ChattersItem chattersItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }
        chattersItem = ((ChattersItem) getIntent().getSerializableExtra("chattersItemDetails"));
        getChat();
        setEvents();

    }

    private void setEvents() {
        sendImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (messageEditText.getText().toString().length() < 1) {
                    Toast.makeText(ChatActivity.this, "" + getResources().getString(R.string.type_something), Toast.LENGTH_SHORT).show();
                } else {
                    sendMessage();
                }


            }
        });
    }


    public void goBack(View view) {
        finish();
    }

    private void getChat() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "chat");
            jsonObject.put("patient_id", chattersItem.getPatientId());
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(ChatActivity.this).getId());
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ChatResponse chatResponse = new Gson().fromJson(response.toString(), ChatResponse.class);
                if (chatResponse.getState() == ConnectionPresenter.SUCCESS) {
                    ChatAdapter timesAdapter = new ChatAdapter(new ArrayList<>(chatResponse.getChat()), ChatActivity.this, chattersItem.getImage());
                    ViewOperations.setRVHVertical(ChatActivity.this, chatRecycleView);
                    chatRecycleView.setAdapter(timesAdapter);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }

    private void sendMessage() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "send_message");
            jsonObject.put("patient_id", chattersItem.getPatientId());
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(ChatActivity.this).getId());
            jsonObject.put("massage", messageEditText.getText() + "");
            jsonObject.put("who", "doctor");
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    messageEditText.setText("");
                    getChat();
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }

}