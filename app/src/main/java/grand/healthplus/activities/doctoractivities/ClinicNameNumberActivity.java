package grand.healthplus.activities.doctoractivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.doctormodels.ClinicNameNumber;
import grand.healthplus.models.doctormodels.ClinicNameNumberResponse;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ClinicNameNumberActivity extends AppCompatActivity {

    @BindView(R.id.et_clinic_name_number_name)
    EditText nameEditText;
    @BindView(R.id.et_clinic_name_number_name_ar)
    EditText nameArEditText;
    @BindView(R.id.et_clinic_name_number_number)
    EditText numberEditText;
    @BindView(R.id.et_clinic_name_number_price)
    EditText priceEditText;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;
    @BindView(R.id.tv_actionbar_save)
    TextView saveTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_name_number);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
        setEvents();
        getClinicDetails();
    }

    private void setEvents(){
        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveClinicDetails();
            }
        });
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void saveClinicDetails() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "edit_clinic_details");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            jsonObject.put("clinic_name_en",""+nameEditText.getText());
            jsonObject.put("clinic_name_ar",""+nameArEditText.getText());
            jsonObject.put("clinic_number",""+numberEditText.getText());
            jsonObject.put("price",priceEditText.getText()+"");
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse  defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    Toast.makeText(ClinicNameNumberActivity.this, getResources().getString(R.string.updated), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }


    private void getClinicDetails() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_clinic_name_number");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ClinicNameNumberResponse clinicNameNumberResponse = new Gson().fromJson(response.toString(), ClinicNameNumberResponse.class);
                if (clinicNameNumberResponse.getState() == ConnectionPresenter.SUCCESS) {
                    nameEditText.setText(clinicNameNumberResponse.getClinicNameNumber().getClinicName());
                    nameArEditText.setText(clinicNameNumberResponse.getClinicNameNumber().getClinicNameAr());
                    numberEditText.setText(clinicNameNumberResponse.getClinicNameNumber().getClinicNumber());
                    priceEditText.setText((int)clinicNameNumberResponse.getClinicNameNumber().getPrice()+"");
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }


}