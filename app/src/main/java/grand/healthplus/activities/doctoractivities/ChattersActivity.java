package grand.healthplus.activities.doctoractivities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;

import grand.healthplus.adapters.doctoradapters.ChattersAdapter;
import  grand.healthplus.models.doctormodels.ChattersResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ChattersActivity extends AppCompatActivity {

    @BindView(R.id.rv_chat_collection)
    RecyclerView chattersRecycleView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatters);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
        getChatters();
    }


    public void goBack(View view) {
        finish();
    }

    private void getChatters() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "chatters");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(ChattersActivity.this));
            jsonObject.put("doctor_id",  DoctorSharedPreferenceHelper.getUserDetails(ChattersActivity.this).getId());

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(ChattersActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
               ChattersResponse chattersResponse = new Gson().fromJson(response.toString(), ChattersResponse.class);
                if (chattersResponse.getState() == ConnectionPresenter.SUCCESS) {
                    ChattersAdapter timesAdapter = new ChattersAdapter(new ArrayList<>(chattersResponse.getChatters()), ChattersActivity.this);
                    ViewOperations.setRVHVertical(ChattersActivity.this, chattersRecycleView);
                    chattersRecycleView.setAdapter(timesAdapter);
                }else if (chattersResponse.getState() == ConnectionPresenter.EMPTY) {
                    chattersRecycleView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true,true);
    }

}