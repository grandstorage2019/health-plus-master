package grand.healthplus.activities.doctoractivities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.google.gson.Gson;
import org.json.JSONObject;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.MUT;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class ChangePasswordActivity extends AppCompatActivity {

    @BindView(R.id.et_change_password_code)
    EditText codeEditText;
    @BindView(R.id.et_change_password_password)
    EditText passwordEditText;
    @BindView(R.id.et_change_password_cpassword)
    EditText cPasswordEditText;
    @BindView(R.id.btn_change_password_update)
    Button updatePasswordBtn;

    String code = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(ChangePasswordActivity.this);
        code =  getIntent().getStringExtra("code");



        setEvents();
    }





    private void setEvents(){
        updatePasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((""+codeEditText.getText()).equals(code)){
                    updatePassword();
                }else {
                    MUT.lToast(ChangePasswordActivity.this,getResources().getString(R.string.wrong_code));
                }

            }
        });
    }

    private void updatePassword() {
        JSONObject params = new JSONObject();
        try {
            params.put("method", "update_password");
            params.put("password", passwordEditText.getText() + "");
            params.put("email", getIntent().getStringExtra("email") + "");


        } catch (Exception e) {
            e.getStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(ChangePasswordActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                try {
                    DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                    if(defaultResponse.getState() == ConnectionPresenter.SUCCESS){
                        MUT.lToast(ChangePasswordActivity.this,getResources().getString(R.string.password_updated));
                        finish();
                    }

                } catch (Exception e) {

                    e.getStackTrace();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        });
        connectionPresenter.doctorConnect(params, true, false);


    }


    public void goBack(View view) {
        finish();
    }


}
