package grand.healthplus.activities.doctoractivities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.activities.hplusactivities.HPlusMainActivity;
import grand.healthplus.trash.CompressObject;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.R;
import grand.healthplus.utils.AddLicenseResponse;
import grand.healthplus.utils.FileOperations;
import grand.healthplus.utils.ImageCompression;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.AppHelper;
import grand.healthplus.vollyutils.ConnectionView;
import grand.healthplus.vollyutils.VolleyMultipartRequest;
import grand.healthplus.vollyutils.VolleySingleton;


public class UploadLicenseActivity extends AppCompatActivity {
    @BindView(R.id.ll_upload_license_container)
    LinearLayout uploadLicenseContainer;
    @BindView(R.id.iv_upload_license_photo)
    ImageView licensePhoto;
    @BindView(R.id.btn_upload_license_confirmation)
    Button confirmationBtn;
    String filePath, imageName, image;
    FileOperations fileOperations;

    int SELECT_PHOTO_CONSTANT = 10001;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_license);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        fileOperations = new FileOperations();
        setEvents();
    }


    private void setEvents() {

        uploadLicenseContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ActivityCompat.checkSelfPermission(UploadLicenseActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UploadLicenseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(UploadLicenseActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_PHOTO_CONSTANT);
                }

            }
        });

        confirmationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (filePath != null) {

                    imageUpload();
                }
            }
        });
    }

    public void selectImage() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.select_image));
        builder.setItems(new CharSequence[]{getResources().getString(R.string.gallery),
                        getResources().getString(R.string.camera)},
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent();
                                galleryIntent.setType("image/*");
                                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(galleryIntent, 1024);
                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, 1023);
                                break;

                            default:
                                break;
                        }
                    }
                });

        builder.show();
    }


    private void imageUpload() {



        JSONObject params = new JSONObject();
        try {
            params.put("method", "upload_licence");
            params.put("image_name", imageName + "");
            params.put("image_data", image);
            params.put("id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(UploadLicenseActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    try {
                        if (response.getInt("state") == ConnectionPresenter.SUCCESS) {
                            MUT.lToast(UploadLicenseActivity.this, getResources().getString(R.string.image_uploaded));
                            finish();
                            MUT.startActivity(UploadLicenseActivity.this,PlansActivity.class);
                        } else {
                            MUT.lToast(UploadLicenseActivity.this, getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }
            });
            connectionPresenter.doctorConnect(params, true, false);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    CompressObject compressObject;
    byte[] bytes;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == RESULT_OK) {
                Uri lastData = data.getData();
                if (lastData == null || data == null) {
                    lastData = FileOperations.specialCameraSelector(UploadLicenseActivity.this, (Bitmap) data.getExtras().get("data"));
                }
                {
                    filePath = fileOperations.getPath(UploadLicenseActivity.this, lastData);
                    int screenWidth = ViewOperations.getScreenWidth(this);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(screenWidth-(screenWidth/4), (int) (ViewOperations.getScreenWidth(this) / 2.5));
                    licensePhoto.setLayoutParams(layoutParams);
                    final File f = new File(filePath);
                    if (f.exists()) {
                        imageName = f.getName();
                        compressObject = new ImageCompression(UploadLicenseActivity.this).compressImage(filePath, f.getName());
                        licensePhoto.setImageBitmap(compressObject.getImage());
                        bytes = compressObject.getByteStream().toByteArray();
                        image = Base64.encodeToString(bytes, 0);
                        imageName = f.getName();
                    }

                }
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (ActivityCompat.checkSelfPermission(UploadLicenseActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UploadLicenseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            selectImage();
        } else {
            ActivityCompat.requestPermissions(UploadLicenseActivity.this, new String[]{android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.GET_ACCOUNTS}, SELECT_PHOTO_CONSTANT);
        }
    }



           /* VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, BASE_URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                AddLicenseResponse addLicenseResponse = new Gson().fromJson(response.toString(), AddLicenseResponse.class);
                if(addLicenseResponse.getStatus().equals(ConnectionPresenter.SUCCESS)){
                    MUT.lToast(UploadLicenseActivity.this,getResources().getString(R.string.photo_updated_successfully));
                }
                loadingBar.cancel();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingBar.cancel();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("doctor_id", "1");
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("image", new DataPart(imageName, AppHelper.getFileDataFromDrawable(getBaseContext(), licensePhoto.getDrawable()), ""));
                return params;
            }
        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);*/
}
