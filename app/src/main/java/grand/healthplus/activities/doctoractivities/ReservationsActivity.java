package grand.healthplus.activities.doctoractivities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.doctoradapters.PatientAppointmentAdapter;
import grand.healthplus.models.doctormodels.PatientReservationResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ReservationsActivity extends AppCompatActivity {


    @BindView(R.id.rv_reservations_collection)
    RecyclerView reservationsRecyclerView;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    String language = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_reservations);
        ButterKnife.bind(ReservationsActivity.this);
        MUT.setSelectedLanguage(this);
        language = SharedPreferenceHelper.getCurrentLanguage(this);
        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }
        getPatientReservations();
    }


    private void getPatientReservations() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "patient_reservations");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(ReservationsActivity.this).getId());
            jsonObject.put("date", getIntent().getStringExtra("date"));
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));




        } catch (Exception e) {
            e.getStackTrace();
        }




        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                PatientReservationResponse patientReservationResponse = new Gson().fromJson(response.toString(), PatientReservationResponse.class);
                if (patientReservationResponse.getState() == ConnectionPresenter.SUCCESS) {
                    patientReservationResponse.getPatientReservation().get(0).setDate(getIntent().getStringExtra("date"));
                    PatientAppointmentAdapter plansAdapter = new PatientAppointmentAdapter(new ArrayList<>(patientReservationResponse.getPatientReservation()), ReservationsActivity.this);
                    ViewOperations.setRVHVertical(ReservationsActivity.this, reservationsRecyclerView);
                    reservationsRecyclerView.setAdapter(plansAdapter);
                    reservationsRecyclerView.setVisibility(View.VISIBLE);
                }else if(patientReservationResponse.getState() == ConnectionPresenter.EMPTY){
                    reservationsRecyclerView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }

    public void goBack(View view) {
        finish();
    }

}