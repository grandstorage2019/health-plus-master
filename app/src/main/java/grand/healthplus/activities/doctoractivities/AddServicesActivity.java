package grand.healthplus.activities.doctoractivities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.utils.MUT;


public class AddServicesActivity extends AppCompatActivity {
    @BindView(R.id.tv_add_service_add)
    TextView addServiceTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        setEvent();
    }


    private void setEvent() {
        addServiceTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(AddServicesActivity.this,ServicesActivity.class);
            }
        });
    }




}
