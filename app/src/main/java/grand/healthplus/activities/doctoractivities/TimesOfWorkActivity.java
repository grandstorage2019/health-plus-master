package grand.healthplus.activities.doctoractivities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.doctoradapters.TimesAdapter;
import grand.healthplus.models.DefaultResponse;
import grand.healthplus.models.doctormodels.TimeModel;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.utils.Check;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class TimesOfWorkActivity extends AppCompatActivity {


    @BindView(R.id.rv_work_times_collection)
    RecyclerView timesRecyclerView;
    @BindView(R.id.tv_work_times_detection)
    TextView detectionTypeTextView;
    @BindView(R.id.tv_actionbar_save)
    TextView saveTextView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;
    @BindView(R.id.ll_times_container)
    LinearLayout timesContainerLayout;
    @BindView(R.id.et_work_times_from)
    EditText fromEditText;
    @BindView(R.id.et_work_times_to)
    EditText toEditText;
    TimesAdapter timesAdapter;
    ArrayList<TimeModel> timeModels;
    String colNames[] = {"Sat", "Sun", "Mon", "Tue", "Thu", "Wed", "Fri"};
    int dayNames[] = {R.string.saturday,R.string.sunday,R.string.monday,R.string.tuesday,R.string.wednesday,R.string.thursday ,R.string.friday};
    int isLifeTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_times_of_work_2);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
        setTimesAdapter();
        setEvents();
        getTimes();
    }


    private void setEvents() {

        fromEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate(fromEditText);
            }
        });

        toEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate(toEditText);
            }
        });

        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTimes();
            }
        });

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        detectionTypeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTypeOfDetectionDialogSpinner();
            }
        });
    }

    private void setTimesAdapter() {
        timeModels = new ArrayList<>();


        for (int i = 0; i < dayNames.length; i++) {
            TimeModel timeModel = new TimeModel();
            timeModel.setDayOpened(false);
            timeModel.setDay(getResources().getString(dayNames[i]));
            timeModel.setFromTime("");
            timeModel.setToTime("");
            timeModel.setDate("");
            timeModel.setReservations("");
            timeModels.add(timeModel);
        }

         timesAdapter = new TimesAdapter(timeModels, TimesOfWorkActivity.this);
        ViewOperations.setRVHVertical(TimesOfWorkActivity.this, timesRecyclerView);
        timesRecyclerView.setAdapter(timesAdapter);
    }

    Dialog typeOfDetectionDialog = null;
    ArrayList<SpinnerModel> typeOfDetectionItems;

    private void setTypeOfDetectionDialogSpinner() {
        if (typeOfDetectionDialog == null) {
            typeOfDetectionItems = new ArrayList<>();

            typeOfDetectionItems.add(new SpinnerModel(0, "" + getResources().getString(R.string.specific_date)));
            typeOfDetectionItems.add(new SpinnerModel(1, "" + getResources().getString(R.string.life_long)));

            typeOfDetectionDialog = ViewOperations.setSpinnerDialog(TimesOfWorkActivity.this, typeOfDetectionItems, getResources().getString(R.string.waiting_time), new SpinnerListener() {
                @Override
                public void onItemClicked(int position) {
                    isLifeTime = position;
                    detectionTypeTextView.setText(typeOfDetectionItems.get(position).getTitle());
                    timesContainerLayout.setVisibility(position == 1 ? View.GONE : View.VISIBLE);
                }
            });
        } else {
            typeOfDetectionDialog.show();
        }
    }

    private void saveTimes() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "save_times");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
            jsonObject.put("is_life_time", isLifeTime);

            if (isLifeTime == 0) {
                jsonObject.put("from_date", fromEditText.getText());
                jsonObject.put("to_date", toEditText.getText());
            }




            for (int i = 0; i < colNames.length; i++) {

                if (timeModels.get(i).isDayOpened()) {


                    jsonObject.put(colNames[i] + "_from_time", timeModels.get(i).getFromTime()+":00");
                    jsonObject.put(colNames[i] + "_to_time", timeModels.get(i).getToTime()+":00");


                    if (!Check.isEndTimeLarger(timeModels.get(i).getFromTime(), timeModels.get(i).getToTime())) {
                        Toast.makeText(this, "" + getResources().getString(R.string.end_time_must_be_larger) + " " + timeModels.get(i).getDay(), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (timeModels.get(i).isBookFirst()) {

                        if (timeModels.get(i).getReservations().equals("")) {
                            Toast.makeText(this, "" + getResources().getString(R.string.add_reservations) + " " + timeModels.get(i).getDay(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        jsonObject.put(colNames[i] + "_type", "first reservation");
                        jsonObject.put(colNames[i] + "_num_resrvation", timeModels.get(i).getReservations());
                    } else {

                        if (timeModels.get(i).getWaitingTime().equals("")) {
                            Toast.makeText(this, "" + getResources().getString(R.string.add_waiting_time) + " " + timeModels.get(i).getDay(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        jsonObject.put(colNames[i] + "_type", "specific time");
                        jsonObject.put(colNames[i] + "_wating_time", "00:"+timeModels.get(i).getWaitingTime()+":00");
                    }
                }
            }


        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    MUT.lToast(TimesOfWorkActivity.this, getResources().getString(R.string.updated));
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }


    private void getTimes() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "work_times");
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(this).getId());
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
        } catch (Exception e) {
            e.getStackTrace();
        }


        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                try {
                    JSONObject jsonObject = response.getJSONArray("work_times").getJSONObject(0);

                    fromEditText.setText(jsonObject.getString("from_date").split(" ")[0]);

                    toEditText.setText(jsonObject.getString("to_date").split(" ")[0]);

                    timesContainerLayout.setVisibility(jsonObject.getInt("is_life_time") == 1 ? View.GONE : View.VISIBLE);

                    timeModels.clear();
                    for (int i = 0; i < dayNames.length; i++) {
                        TimeModel timeModel = new TimeModel();
                        timeModel.setDay(getResources().getString(dayNames[i]));



                        timeModel.setDayOpened(!(jsonObject.getString(colNames[i] + "_from_time")==null||jsonObject.getString(colNames[i] + "_from_time").equals("null")||jsonObject.getString(colNames[i] + "_from_time").equals("NULL")));
                        if(timeModel.isDayOpened()) {

                            timeModel.setFromTime(jsonObject.getString(colNames[i] + "_from_time"));
                            timeModel.setToTime(jsonObject.getString(colNames[i] + "_to_time"));
                            timeModel.setType(jsonObject.getString(colNames[i] + "_type"));
                            timeModel.setReservations(jsonObject.getString(colNames[i] + "_num_resrvation"));

                            if(jsonObject.getString(colNames[i] + "_type").equals("specific time")){

                                String WaitingTime = jsonObject.getString(colNames[i] + "_wating_time").split(":")[1];

                                timeModel.setWaitingTime(WaitingTime);
                                timeModel.setBookFirst(false);
                                timeModel.setReservations("");
                            }else {
                                timeModel.setBookFirst(true);
                                timeModel.setReservations(jsonObject.getString(colNames[i] + "_num_resrvation"));
                                timeModel.setWaitingTime("");
                            }


                        }else {
                            timeModel.setDayOpened(false);
                            timeModel.setDay(getResources().getString(dayNames[i]));
                            timeModel.setFromTime("");
                            timeModel.setToTime("");
                            timeModel.setWaitingTime("");
                            timeModel.setBookFirst(false);
                            timeModel.setDate("");
                            timeModel.setReservations("");
                        }
                        timeModels.add(timeModel);
                    }
                    timesAdapter.notifyDataSetChanged();
                }
                catch (Exception e){
                    e.getStackTrace();
                }



            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }

    private void selectDate(final EditText editText) {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH) + 1;
        final DatePickerDialog datePickerDialog = new DatePickerDialog(TimesOfWorkActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                String selectedDate = i + "-" + (++i1) + "-" + i2;
                editText.setText(selectedDate);
            }
        }, year, month, 0);

        datePickerDialog.show();
    }


}