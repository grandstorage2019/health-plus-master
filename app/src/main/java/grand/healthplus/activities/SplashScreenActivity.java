package grand.healthplus.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.doctoractivities.ChooseTypeActivity;
import grand.healthplus.models.DefaultResponse;
import grand.healthplus.utils.MUT;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class SplashScreenActivity extends AppCompatActivity {

    @BindView(R.id.iv_splash_screen)
    ImageView splashScreenImage;

    @BindView(R.id.iv_splash_screen_advertising)
    ImageView advertisingImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getBaseContext()));
        Glide.with(this).load(R.raw.splash_screen).into(splashScreenImage);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1000);
        } else {
            startTimer();
        }


        getAd();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startTimer();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1000);
        }

    }


    private void getAd() {
        JSONObject params = new JSONObject();
        try {
            params.put("method", "get_ad");


            ConnectionPresenter connectionPresenter = new ConnectionPresenter(SplashScreenActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {

                    try {
                        final DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                        ConnectionPresenter.loadImage(advertisingImage, "setting/" + defaultResponse.getResult().get(0).getValue());
                        advertisingImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                MUT.startWebPage(SplashScreenActivity.this, defaultResponse.getResult().get(2).getValue());
                            }
                        });
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }
            });
            connectionPresenter.connect(params, false, false);
        } catch (Exception e) {
            e.getStackTrace();
        }

    }


    private void startTimer() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void ok) {
                finishAffinity();
                MUT.startActivity(SplashScreenActivity.this, ChooseTypeActivity.class);
            }

        }.execute();
    }


}
