package grand.healthplus.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;


public class SingleNotificationActivity extends AppCompatActivity {


    @BindView(R.id.et_single_notification_details)
    TextView titleTextView;
    @BindView(R.id.iv_single_notification_photo)
    ImageView sendImageView;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    boolean isNotification=false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_notification);
        ButterKnife.bind(this);

        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }

        try{
            isNotification = getIntent().getBooleanExtra("is_notifications",false);
        }
        catch (Exception e){
            e.getStackTrace();
        }


        if(isNotification){
            titleTextView.setText(getIntent().getBundleExtra("bundle").getString("details"));
            ConnectionPresenter.loadImage2(sendImageView,  getIntent().getBundleExtra("bundle").getString("image"));
        }
        else {
            ConnectionPresenter.loadImage(sendImageView,getIntent().getStringExtra("image"));
            titleTextView.setText(getIntent().getStringExtra("title"));
        }



    }



    public void goBack(View view) {
        finish();
    }




}