package grand.healthplus.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import java.net.InetAddress;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;

public class NoConnectionActivity extends Activity {
    LongOperation lon = null;
    @BindView(R.id.btn_no_connection_reload)
    Button reLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);
        ButterKnife.bind(this);

        reLoad.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    lon.cancel(true);
                } catch (Exception e) {
                    e.getStackTrace();
                }
                lon = new LongOperation();
                lon.execute(true);
            }
        });
    }

    public void reLoad(View view) {
        try {
            lon.cancel(true);
        } catch (Exception e) {
            e.getStackTrace();
        }

        lon = new LongOperation();
        lon.execute(true);
    }

    private class LongOperation extends AsyncTask<Boolean, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Boolean... params) {
            Boolean available = false;
            try {
                InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
                available = ipAddr.equals("");
            } catch (Exception e) {
                available = true;
            }
            return available;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result) {
                finish();
                Intent intent = new Intent(NoConnectionActivity.this, SplashScreenActivity.class);
                startActivity(intent);

            }
            super.onPostExecute(result);
        }


    }

}
