package grand.healthplus.activities.hplusactivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import com.google.gson.Gson;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hplusadapters.ReservationsAdapter;
import grand.healthplus.models.hplusmodels.ReservationsResponse;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ReservationsActivity extends AppCompatActivity {
    @BindView(R.id.rv_reservations_collection)
    RecyclerView reservationsRecyclicView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations);
        ButterKnife.bind(this);
        //getReservations();
    }


   /* private void getReservations() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "reservations");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
            jsonObject.put("patient_id", 2);
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ReservationsResponse reservationsResponse = new Gson().fromJson(response.toString(), ReservationsResponse.class);
                if (reservationsResponse.getState() == ConnectionPresenter.SUCCESS) {
                    ReservationsAdapter reservationsAdapter = new ReservationsAdapter(new ArrayList<>(reservationsResponse.getReservations()), ReservationsActivity.this);
                    ViewOperations.setRVHVertical(ReservationsActivity.this, reservationsRecyclicView);
                    reservationsRecyclicView.setAdapter(reservationsAdapter);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true , false);
    }*/
}
