package grand.healthplus.activities.hplusactivities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hcareactivities.HCareMoreinfoActivity;
import grand.healthplus.adapters.hplusadapters.RatingAdapter;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.models.hplusmodels.RatingResponse;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class RatingActivity extends AppCompatActivity {
    @BindView(R.id.rv_rating_collection)
    RecyclerView ratingRecyclicView;
    @BindView(R.id.rb_rating_user_rate)
    MaterialRatingBar ratingBar;
    @BindView(R.id.tv_rating_views)
    TextView viewsTextView;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    @BindView(R.id.btn_rating_confirm)
    Button rateDoctorBtn;

    boolean isDoctor=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        ButterKnife.bind(this);
        getRating();
        setEvent();

        try{
            isDoctor = getIntent().getBooleanExtra("doctor",false);
        }catch (Exception e){
            e.getStackTrace();
        }

        if(isDoctor){
            rateDoctorBtn.setVisibility(View.GONE);
        }
        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }
    }

    private void setEvent() {
        rateDoctorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPreferenceHelper.isLogined(RatingActivity.this)) {
                    ratingDialog();
                }
            }
        });
    }

    public void goBack(View view) {
        finish();
    }

    private void getRating() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "rating");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
            jsonObject.put("doctor_id", getIntent().getIntExtra("doctorId", 0));
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                RatingResponse ratingResponse = new Gson().fromJson(response.toString(), RatingResponse.class);
                if (ratingResponse.getState() == ConnectionPresenter.SUCCESS) {
                    RatingAdapter ratingAdapter = new RatingAdapter(new ArrayList<>(ratingResponse.getRating()), RatingActivity.this);
                    ViewOperations.setRVHVertical(RatingActivity.this, ratingRecyclicView);
                    ratingRecyclicView.setAdapter(ratingAdapter);

                    ratingBar.setRating((float)
                            ratingResponse.getTotalRating());
                    viewsTextView.setText("(" + getResources().getString(R.string.of) + " " + ratingResponse.getRaters() + " " + getResources().getString(R.string.visited_doctor) + ")");
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }


    private void rateNow(double rate, String review) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "rate");
            jsonObject.put("stars", rate);
            jsonObject.put("comment", review);
            jsonObject.put("doctor_id", getIntent().getIntExtra("doctorId", 0));
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(RatingActivity.this).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse ratingResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (ratingResponse.getState() == ConnectionPresenter.SUCCESS) {
                    getRating();
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }

    public void ratingDialog() {
        final Dialog ratingDialog = new Dialog(RatingActivity.this, android.R.style.Theme_Black_NoTitleBar);
        ratingDialog.setCancelable(true);
        ratingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(RatingActivity.this);
        View ratingView = inflater.inflate(R.layout.dialog_rate_doctor, null);
        ratingDialog.setContentView(ratingView);

        final EditText review = ratingView.findViewById(R.id.et_rate_doctor_review);
        final MaterialRatingBar ratingBar = ratingView.findViewById(R.id.rb_rate_doctor_rating);
        Button confirm = ratingView.findViewById(R.id.btn_rate_doctor_confirm);
        Button cancel = ratingView.findViewById(R.id.btn_rate_doctor_cancel);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateNow(ratingBar.getRating(), review.getText() + "");
                ratingDialog.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingDialog.cancel();
            }
        });

        ratingDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0e000000")));
        ratingDialog.show();
    }
}
