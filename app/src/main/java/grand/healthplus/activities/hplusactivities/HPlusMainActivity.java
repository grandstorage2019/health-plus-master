package grand.healthplus.activities.hplusactivities;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.SignInActivity;
import grand.healthplus.activities.SingleNotificationActivity;
import grand.healthplus.adapters.MenuAdapter;
import grand.healthplus.fragments.AboutUsFragment;
import grand.healthplus.fragments.ChattersFragment;
import grand.healthplus.fragments.ContactUsFragment;
import grand.healthplus.fragments.SettingsFragment;
import grand.healthplus.fragments.hplusfragment.FavouritesFragment;
import grand.healthplus.fragments.hplusfragment.NotificationsFragment;
import grand.healthplus.fragments.hplusfragment.ReservationsFragment;
import grand.healthplus.models.MenuModel;
import grand.healthplus.models.hcaremodels.registeration.UserItem;
import grand.healthplus.trash.CompressObject;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.FileOperations;
import grand.healthplus.utils.FragmentHelper;
import grand.healthplus.fragments.hplusfragment.HealthPlusHomeFragment;
import grand.healthplus.fragments.hplusfragment.HPlusSearchFragment;
import grand.healthplus.utils.ImageCompression;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class HPlusMainActivity extends AppCompatActivity {

    private ArrayList<MenuModel> navDrawerItems;
    private MenuAdapter adapter;
    @BindView(R.id.dl_main_page_menu)
    DrawerLayout drawerLayout;
    @BindView(R.id.lv_main_page_menu_items)
    ListView drawerList;
    @BindView(R.id.ll_main_doctor_info)
    LinearLayout notificationsLayout;
    @BindView(R.id.ll_main_patient)
    LinearLayout appointmentLayout;
    @BindView(R.id.ll_main_appointment)
    LinearLayout favoriteLayout;
    @BindView(R.id.ll_main_more)
    LinearLayout searchLayout;
    @BindView(R.id.iv_main_doctor_info)
    ImageView notificationsImageView;
    @BindView(R.id.iv_main_patient)
    ImageView appointmentImageView;
    @BindView(R.id.iv_main_appointment)
    ImageView favoriteImageView;
    @BindView(R.id.iv_main_more)
    ImageView searchImageView;
    @BindView(R.id.iv_main_user_image)
    ImageView userImageView;
    @BindView(R.id.iv_main_user_name)
    TextView userNameTextView;
    @BindView(R.id.iv_main_user_id)
    TextView userIdTextView;
    @BindView(R.id.tv_main_title)
    TextView titleTextView;
    @BindView(R.id.iv_main_message)
    ImageView messagesImageView;
    @BindView(R.id.iv_main_icon)
    ImageView iconImageView;
    @BindView(R.id.iv_main_menu)
    ImageView menuImageView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h_plus_main);
        ButterKnife.bind(this);
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getBaseContext()));
        setEvents();
        createMenu();
        setUserInfo();
        FragmentHelper.addFragment(this, new HealthPlusHomeFragment(), "HealthPlusHomeFragment");
        fileOperations = new FileOperations();


        try {
            if (getIntent().getBooleanExtra("is_notifications", false)) {
                Intent intent = new Intent(this, SingleNotificationActivity.class);
                intent.putExtra("is_notifications", getIntent().getBooleanExtra("is_notifications", false));
                intent.putExtra("bundle", getIntent().getBundleExtra("bundle"));
                startActivity(intent);
            }
        }
        catch (Exception e){
            e.getStackTrace();
        }

    }

    private void setEvents() {


        menuImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });

        notificationsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                iconImageView.setVisibility(View.GONE);
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(getResources().getString(R.string.notifications));
                FragmentHelper.popLastFragment(HPlusMainActivity.this);
                FragmentHelper.addFragment(HPlusMainActivity.this, new NotificationsFragment(), "NotificationsFragment");
                notificationsImageView.setImageResource(R.mipmap.ic_colored_notification);
                appointmentImageView.setImageResource(R.mipmap.ic_un_colored_appoinment);
                favoriteImageView.setImageResource(R.mipmap.ic_un_colored_favorites);
                searchImageView.setImageResource(R.mipmap.ic_un_colored_search);
            }
        });

        appointmentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                if (SharedPreferenceHelper.isLogined(HPlusMainActivity.this)) {
                    iconImageView.setVisibility(View.GONE);
                    titleTextView.setVisibility(View.VISIBLE);
                    titleTextView.setText(getResources().getString(R.string.my_appoinments));
                    FragmentHelper.popLastFragment(HPlusMainActivity.this);
                    FragmentHelper.addFragment(HPlusMainActivity.this, new ReservationsFragment(), "ReservationsFragment");
                    notificationsImageView.setImageResource(R.mipmap.ic_un_colored_notification);
                    appointmentImageView.setImageResource(R.mipmap.ic_colored_appointment);
                    favoriteImageView.setImageResource(R.mipmap.ic_un_colored_favorites);
                    searchImageView.setImageResource(R.mipmap.ic_un_colored_search);
                }
            }
        });

        favoriteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                if (SharedPreferenceHelper.isLogined(HPlusMainActivity.this)) {
                    iconImageView.setVisibility(View.GONE);
                    titleTextView.setVisibility(View.VISIBLE);
                    titleTextView.setText(getResources().getString(R.string.favourites));
                    FragmentHelper.popLastFragment(HPlusMainActivity.this);
                    FragmentHelper.addFragment(HPlusMainActivity.this, new FavouritesFragment(), "FavouritesFragment");
                    notificationsImageView.setImageResource(R.mipmap.ic_un_colored_notification);
                    appointmentImageView.setImageResource(R.mipmap.ic_un_colored_appoinment);
                    favoriteImageView.setImageResource(R.mipmap.ic_colored_favorites);
                    searchImageView.setImageResource(R.mipmap.ic_un_colored_search);
                }
            }
        });

        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                iconImageView.setVisibility(View.GONE);
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(getResources().getString(R.string.search));
                    FragmentHelper.popLastFragment(HPlusMainActivity.this);
                    FragmentHelper.addFragment(HPlusMainActivity.this, new HPlusSearchFragment(), "HPlusSearchFragment");
                    notificationsImageView.setImageResource(R.mipmap.ic_un_colored_notification);
                    appointmentImageView.setImageResource(R.mipmap.ic_un_colored_appoinment);
                    favoriteImageView.setImageResource(R.mipmap.ic_un_colored_favorites);
                    searchImageView.setImageResource(R.mipmap.ic_colored_search);

            }
        });

        messagesImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                if (SharedPreferenceHelper.isLogined(HPlusMainActivity.this)) {
                    FragmentHelper.popLastFragment(HPlusMainActivity.this);
                    FragmentHelper.addFragment(HPlusMainActivity.this, new ChattersFragment(), "ChattersFragment");
                }
            }
        });

        userImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(HPlusMainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(HPlusMainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(HPlusMainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1000);
                    ActivityCompat.requestPermissions(HPlusMainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1001);
                } else {
                    selectImage();
                }

            }
        });
    }


    private void createMenu() {

        final int menuTitles[] = {
                R.string.home,
                R.string.settings,
                R.string.contact_us,
                R.string.about_health_plus,
                R.string.share_app,
                R.string.sign_in};

        if (SharedPreferenceHelper.getUserDetails(HPlusMainActivity.this) != null) {
            menuTitles[5] = R.string.sign_out;
        }
        navDrawerItems = new ArrayList<>();
        for (int i = 0; i < menuTitles.length; i++) {
            MenuModel menuItemDetails = new MenuModel();
            menuItemDetails.setTitle(getResources().getString(menuTitles[i]));
            menuItemDetails.setId(i);
            navDrawerItems.add(menuItemDetails);
        }


        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                iconImageView.setVisibility(View.GONE);
                titleTextView.setVisibility(View.VISIBLE);
                setDefaults();
                if (position != 4) {
                    titleTextView.setText(menuTitles[position]);
                    FragmentHelper.popLastFragment(HPlusMainActivity.this);
                }

                if (position == 0) {
                    iconImageView.setVisibility(View.VISIBLE);
                    titleTextView.setVisibility(View.GONE);
                    FragmentHelper.addFragment(HPlusMainActivity.this, new HealthPlusHomeFragment(), "HealthPlusHomeFragment");
                } else if (position == 1) {
                    FragmentHelper.addFragment(HPlusMainActivity.this, new SettingsFragment(), "AboutUsFragment");
                } else if (position == 2) {
                    ContactUsFragment contactUsFragment = new ContactUsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isDoctor",true);
                    contactUsFragment.setArguments(bundle);

                    FragmentHelper.addFragment(HPlusMainActivity.this, contactUsFragment, "SettingsFragment");
                } else if (position == 3) {
                    FragmentHelper.addFragment(HPlusMainActivity.this, new AboutUsFragment(), "ChattersFragment");
                } else if (position == 4) {
                    int applicationNameId = getApplicationInfo().labelRes;
                    final String appPackageName = getPackageName();
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, getString(applicationNameId));
                    String text = getResources().getString(R.string.instal_this);
                    String link = "https://play.google.com/store/apps/details?id=" + appPackageName;
                    i.putExtra(Intent.EXTRA_TEXT, text + " " + link);
                    startActivity(Intent.createChooser(i, "Share link:"));
                } else if (position == 5) {
                    titleTextView.setText(menuTitles[0]);
                    if (navDrawerItems.get(position).getTitle().equals(getResources().getString(R.string.sign_out))) {
                        SharedPreferenceHelper.clearUserDetails(HPlusMainActivity.this);

                        navDrawerItems.get(position).setTitle(getResources().getString(R.string.sign_in));
                        adapter.notifyDataSetChanged();
                        FragmentHelper.addFragment(HPlusMainActivity.this, new HealthPlusHomeFragment(), "HealthPlusHomeFragment");
                        setUserInfo();
                    } else {
                        MUT.startActivity(HPlusMainActivity.this, SignInActivity.class);
                    }
                }

                drawerLayout.closeDrawers();
            }

        });
        adapter = new MenuAdapter(getApplicationContext(), navDrawerItems);

        drawerList.setAdapter(adapter);


    }


    boolean doubleBackToExitPressedOnce = true;

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawers();
        } else {

            if (!doubleBackToExitPressedOnce) {
                finish();
            } else {

                this.doubleBackToExitPressedOnce = false;
                Toast.makeText(this, getResources().getString(R.string.double_click_to_close), Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = true;
                    }
                }, 2000);

                setDefaults();
                iconImageView.setVisibility(View.VISIBLE);
                titleTextView.setVisibility(View.GONE);
                FragmentHelper.popLastFragment(HPlusMainActivity.this);
                FragmentHelper.addFragment(HPlusMainActivity.this, new HealthPlusHomeFragment(), "HealthPlusHomeFragment");


            }
        }
    }

    private void setUserInfo() {

        UserItem userItem = SharedPreferenceHelper.getUserDetails(this);
        if (userItem != null) {
            userNameTextView.setText(getResources().getString(R.string.user_name) + " : " + userItem.getUsername());
            userIdTextView.setText(getResources().getString(R.string.id) + " " + userItem.getId());
            if(userItem.getImage()==null||userItem.getImage().equals("")){
                userImageView.setImageResource(R.drawable.ic_user_temp);

            }else {
                ConnectionPresenter.loadImage(userImageView, "patient_profile/" + userItem.getImage());
            }
        } else {
            userImageView.setVisibility(View.GONE);
            userNameTextView.setVisibility(View.GONE);
            userIdTextView.setVisibility(View.GONE);
        }
    }



    CompressObject compressObject;
    String file;
    byte[] bytes;
    String image = "", imageName = "";
    FileOperations fileOperations;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
        Uri lastData = data.getData();

        if (lastData == null || data == null) {
            lastData = FileOperations.specialCameraSelector(HPlusMainActivity.this, (Bitmap) data.getExtras().get("data"));
        }
        {
            file = fileOperations.getPath(HPlusMainActivity.this, lastData);
            final Dialog dialog = new Dialog(HPlusMainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_attach_image);
            try {
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
            } catch (Exception e) {
                e.getStackTrace();
            }
            final ImageView attachImage = dialog.findViewById(R.id.iv_attach_image_photo);
            Button send = dialog.findViewById(R.id.btn_attach_image_confirm);
            Button cancel = dialog.findViewById(R.id.btn_attach_image_cancel);
            final File f = new File(file);
            if (f.exists()) {

                compressObject = new ImageCompression(HPlusMainActivity.this).compressImage(file, f.getName());
                attachImage.setImageBitmap(compressObject.getImage());
                bytes = compressObject.getByteStream().toByteArray();

            }

            send.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    image = Base64.encodeToString(bytes, 0);

                    imageName = f.getName();
                    uploadImage();
                    dialog.cancel();
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.show();
        }
        }
        catch (Exception e){
            e.getStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadImage() {
        JSONObject params = new JSONObject();
        try {
            params.put("method", "upload_image");
            params.put("image_name", imageName + "");
            params.put("image_data", image);
            params.put("id", SharedPreferenceHelper.getUserDetails(this).getId());
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(HPlusMainActivity.this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    try {
                        if (response.getInt("state") == ConnectionPresenter.SUCCESS) {
                            MUT.lToast(HPlusMainActivity.this, getResources().getString(R.string.image_uploaded));
                            ConnectionPresenter.loadImage(userImageView, "patient_profile/" + response.getString("image"));
                        } else {
                            MUT.lToast(HPlusMainActivity.this, getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {
                }
            });
            connectionPresenter.connect(params, true, false);
        } catch (Exception e) {
            e.getStackTrace();
        }

    }

    private void setDefaults(){
        notificationsImageView.setImageResource(R.mipmap.ic_un_colored_notification);
        appointmentImageView.setImageResource(R.mipmap.ic_un_colored_appointment);
        favoriteImageView.setImageResource(R.mipmap.ic_un_colored_favorites);
        searchImageView.setImageResource(R.mipmap.ic_un_colored_search);
    }


    @Override
    protected void onResume() {
        setUserInfo();
        super.onResume();
    }

    public void selectImage() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.select_image));
        builder.setItems(new CharSequence[]{getResources().getString(R.string.gallery),
                        getResources().getString(R.string.camera)},
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent();
                                galleryIntent.setType("image/*");
                                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(galleryIntent, 1024);
                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, 1023);
                                break;

                            default:
                                break;
                        }
                    }
                });

        builder.show();
    }
}
