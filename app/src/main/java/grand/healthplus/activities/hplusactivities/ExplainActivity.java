package grand.healthplus.activities.hplusactivities;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.signin.SignIn;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ChooseDepartmentActivity;
import grand.healthplus.activities.SignInActivity;
import grand.healthplus.activities.SignUpActivity;
import grand.healthplus.activities.hcareactivities.HCareMainActivity;
import grand.healthplus.fragments.Explain1Fragment;
import grand.healthplus.fragments.Explain2Fragment;
import grand.healthplus.fragments.Explain3Fragment;
import grand.healthplus.utils.MUT;

public class ExplainActivity extends AppCompatActivity {

    @BindView(R.id.btn_explain_sign_up)
    Button signUpbtn;
    @BindView(R.id.btn_explain_get_started)
    Button getStartedbtn;
    @BindView(R.id.rl_have_account)
    RelativeLayout haveAccountLayout;

    ViewPager pager;
    int currentPage=0,NUM_PAGES=3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explain);
        ButterKnife.bind(this);
         pager = findViewById(R.id.vp_explain_explain_pager);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        autoMoveSlider();
        setEvents();
    }



    private void autoMoveSlider(){
        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {

                if (currentPage == NUM_PAGES ) {
                    currentPage = 0;
                }
                pager.setCurrentItem(currentPage++, true);
            }
        };


        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 100, 2000);
    }

    private void setEvents() {
        signUpbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(ExplainActivity.this, SignUpActivity.class);
            }
        });
        getStartedbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(ExplainActivity.this, ChooseDepartmentActivity.class);
            }
        });
        haveAccountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(ExplainActivity.this, SignInActivity.class);
            }
        });
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return Explain1Fragment.newInstance("Explain1Fragment, Instance 1");
                case 1:
                    return Explain2Fragment.newInstance("Explain2Fragment, Instance 2");
                case 2:
                    return Explain3Fragment.newInstance("Explain3Fragment, Instance 3");
                default:
                    return Explain3Fragment.newInstance("Explain3Fragment, Instance 3");
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
