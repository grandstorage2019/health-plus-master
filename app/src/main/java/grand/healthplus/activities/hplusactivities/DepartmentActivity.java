package grand.healthplus.activities.hplusactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;

public class DepartmentActivity extends AppCompatActivity {

    @BindView(R.id.ll_choose_department_health_plus)
    LinearLayout healthPlusLayout;
    @BindView(R.id.ll_choose_department_health_care)
    LinearLayout healthCareLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_department);
        ButterKnife.bind(this);
        MUT.setSelectedLanguage(this);
        setEvents();
    }


    private void setEvents() {
        healthPlusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(DepartmentActivity.this, HPlusMainActivity.class);
            }
        });
        healthCareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(DepartmentActivity.this, HPlusMainActivity.class);
            }
        });
    }


}
