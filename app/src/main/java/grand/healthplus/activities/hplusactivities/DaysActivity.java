package grand.healthplus.activities.hplusactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hplusadapters.DaysAdapter;
import grand.healthplus.adapters.hplusadapters.DaysTimesAdapter;
import grand.healthplus.models.hplusmodels.BookingTimesItem;
import grand.healthplus.models.hplusmodels.BookingTimesResponse;
import grand.healthplus.models.hplusmodels.DaysTimesModel;
import grand.healthplus.models.hplusmodels.DoctorInfoResponse;
import grand.healthplus.models.hplusmodels.DoctorsItem;
import grand.healthplus.models.hplusmodels.TimesItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class DaysActivity extends AppCompatActivity {

    @BindView(R.id.rv_days_collection)
    RecyclerView daysRecycleView;


    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    DoctorsItem doctorsItem;
    DaysTimesModel daysTimesModel;

    boolean isDoctor=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_days);
        ButterKnife.bind(this);

        try {
            doctorsItem = (DoctorsItem) getIntent().getSerializableExtra("doctorId");
            daysTimesModel = (DaysTimesModel) getIntent().getSerializableExtra("daysTimes");
        }
        catch (Exception e){
            e.getStackTrace();
        }


        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            isDoctor = getIntent().getBooleanExtra("isDoctor", false);
        }catch (Exception e) {
          e.getStackTrace();
        }
        if(isDoctor){
            getDaysTimes();
        }else {
            getDayTimes2();
        }
    }


    private void getDayTimes2() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method", "booking_times");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(DaysActivity.this));
            jsonObject.put("doctor_id", doctorsItem.getDoctorId());
            jsonObject.put("from", daysTimesModel.getTimesItem().getFromTime());
            jsonObject.put("to", daysTimesModel.getTimesItem().getToTime());
            jsonObject.put("waiting_time",daysTimesModel.getTimesItem().getWaitingTime());
            jsonObject.put("date", daysTimesModel.getTimesItem().getDate());


        } catch (Exception e) {
            e.getStackTrace();
        }


        ConnectionPresenter connectionPresenter = new ConnectionPresenter(DaysActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                BookingTimesResponse bookingTimesResponse = new Gson().fromJson(response.toString(), BookingTimesResponse.class);
                ArrayList<BookingTimesItem> bookingTimesItems =  new ArrayList<>(bookingTimesResponse.getBookingTimes());
                DaysAdapter daysAdapter;
                if(isDoctor) {
                     daysAdapter = new DaysAdapter(bookingTimesItems, DaysActivity.this, doctorsItem, daysTimesModel,getIntent().getIntExtra("patient_id",0));
                }else {
                    daysAdapter = new DaysAdapter(bookingTimesItems, DaysActivity.this, doctorsItem, daysTimesModel);
                }
                ViewOperations.setRVHVerticalGrid(DaysActivity.this, daysRecycleView,3);
                daysRecycleView.setAdapter(daysAdapter);
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true,false);
    }



    private void getDaysTimes() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "doctor_info");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
            jsonObject.put("doctor_id", doctorsItem.getDoctorId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DoctorInfoResponse doctorInfoResponse = new Gson().fromJson(response.toString(), DoctorInfoResponse.class);


                if (doctorInfoResponse.getState() == ConnectionPresenter.SUCCESS) {
                    List<TimesItem> timesItems = doctorInfoResponse.getTimes();
                    ArrayList<DaysTimesModel> daysTimesModels = new ArrayList<>();
                    for(int i=0; i<timesItems.size(); i++){
                        DaysTimesModel daysTimesModel = new DaysTimesModel();
                        daysTimesModel.setTimesItem(timesItems.get(i));
                        daysTimesModels.add(daysTimesModel);
                    }

                    DaysTimesAdapter timesAdapter = new DaysTimesAdapter(daysTimesModels, DaysActivity.this,doctorsItem ,getIntent().getIntExtra("patient_id",0));
                    ViewOperations.setRVHVertical(DaysActivity.this, daysRecycleView);
                    daysRecycleView.setAdapter(timesAdapter);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true , false);
    }

    public void goBack(View view) {
        finish();
    }


}
