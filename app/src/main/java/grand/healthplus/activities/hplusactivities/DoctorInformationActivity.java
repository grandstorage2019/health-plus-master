package grand.healthplus.activities.hplusactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ChatActivity;
import grand.healthplus.adapters.hplusadapters.TimesAdapter;
import grand.healthplus.models.hcaremodels.ChattersItem;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.models.hplusmodels.DoctorInfoResponse;
import grand.healthplus.models.hplusmodels.DoctorServicesItem;
import grand.healthplus.models.hplusmodels.DoctorsItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import me.gujun.android.taggroup.TagGroup;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class DoctorInformationActivity extends AppCompatActivity {

    @BindView(R.id.iv_health_care_info_image)
    ImageView photoImageView;
    @BindView(R.id.tv_health_care_name)
    TextView nameTextView;
    @BindView(R.id.tv_doctor_info_views)
    TextView viewsTextView;
    @BindView(R.id.rb_doctor_info_rating)
    MaterialRatingBar ratingBar;
    @BindView(R.id.iv_doctor_info_favourite)
    ImageView favouriteImageView;
    @BindView(R.id.tv_doctor_info_price)
    TextView priceTextView;
    @BindView(R.id.tv_doctor_info_time)
    TextView timeTextView;
    @BindView(R.id.rv_doctor_info_times)
    RecyclerView timesRecyclerView;
    @BindView(R.id.tv_doctor_information_specialization)
    TextView specializationTextView;
    @BindView(R.id.iv_doctor_info_rating_details)
    TextView ratingDetailsTextView;
    @BindView(R.id.tv_doctor_information_details)
    TextView doctorDetailsTextView;
    @BindView(R.id.tv_doctor_info_location)
    TextView doctorLocatoinTextView;
    @BindView(R.id.iv_actionbar_method)
    ImageView shareImageView;

    @BindView(R.id.iv_doctor_info_right_arrow)
    ImageView rightArrow;

    @BindView(R.id.iv_doctor_info_left_arrow)
    ImageView leftArrow;

    @BindView(R.id.iv_actionbar_message)
    ImageView messageImageView;

    @BindView(R.id.rl_doctor_information_rating_layout)
    RelativeLayout ratingLayout;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    @BindView(R.id.tg_doctor_information_services)
    TagGroup servicesTags;

    DoctorsItem doctorsItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_information);
        ButterKnife.bind(this);
        doctorsItem = ((DoctorsItem) getIntent().getSerializableExtra("doctorDetails"));

        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
            rightArrow.setRotation(180);
            leftArrow.setRotation(180);
        }

        setDoctorDetails();


    }

    private void setDoctorDetails() {
        ConnectionPresenter.loadImage(photoImageView, "doctor_profile/" + doctorsItem.getProfileImage());
        nameTextView.setText(getResources().getString(R.string.doctor)+" "+ StringUtils.capitalize(doctorsItem.getFirstName() + " " + doctorsItem.getLastName()));
        ratingBar.setRating((float) doctorsItem.getRate());
        priceTextView.setText(getResources().getString(R.string.detection_price) + "\n" + (int)doctorsItem.getPrice() + " " +getResources().getString(R.string.kd));
        timeTextView.setText(getResources().getString(R.string.detection_time) + "\n");
        viewsTextView.setText(doctorsItem.getViews()+" "+getResources().getString(R.string.views));


        if(doctorsItem.getWaitingHours()>0 || doctorsItem.getWaitingMinutes() > 0) {
            if (doctorsItem.getWaitingHours() > 0) {
                timeTextView.setText(timeTextView.getText() + "" + doctorsItem.getWaitingHours() + " " + getResources().getString(R.string.hours) + " ");
            }
            if (doctorsItem.getWaitingMinutes() > 0) {
                timeTextView.setText(timeTextView.getText() + "" + doctorsItem.getWaitingMinutes() + " " + getResources().getString(R.string.minutes));
            }
        }else {
            timeTextView.setText(getResources().getString(R.string.book_first));
        }

        specializationTextView.setText(doctorsItem.getTitle());
        doctorDetailsTextView.setText(doctorsItem.getAbout());
        doctorLocatoinTextView.setText(doctorsItem.getAreaName()+", "+doctorsItem.getStreetName()+", "+doctorsItem.getBlock()+", "+doctorsItem.getBuildingNumber()+", "+doctorsItem.getFloor()+", "+doctorsItem.getApartment()+", "+doctorsItem.getLandmark());
        getDoctorInfo();
        setEvents();


        if (doctorsItem.isFavourite()) {
            favouriteImageView.setImageResource(R.drawable.ic_heart_selected);
        } else {
            favouriteImageView.setImageResource(R.drawable.ic_empty_heart);
        }

        ratingDetailsTextView.setText("(" + getResources().getString(R.string.of) + " " + doctorsItem.getRaters() + " " + getResources().getString(R.string.visited_doctor) + ")");
    }


    public void goBack(View view) {
        finish();
    }

    private void addDeleteFavourite(final boolean isAdd) {

        JSONObject jsonObject = new JSONObject();
        try {
            String method =isAdd?"add_favourite":"delete_favourite";
            jsonObject.put("method",method);
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(DoctorInformationActivity.this).getId());
            jsonObject.put("doctor_id", doctorsItem.getDoctorId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(DoctorInformationActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);
                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    doctorsItem.setFavourite(!doctorsItem.isFavourite());
                    if (doctorsItem.isFavourite()) {
                        favouriteImageView.setImageResource(R.drawable.ic_heart_selected);
                    } else {
                        favouriteImageView.setImageResource(R.drawable.ic_empty_heart);
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }
    private void getDoctorInfo() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method", "doctor_info");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(this));
            jsonObject.put("doctor_id", doctorsItem.getDoctorId());
        } catch (Exception e) {
            e.getStackTrace();
        }



        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DoctorInfoResponse doctorInfoResponse = new Gson().fromJson(response.toString(), DoctorInfoResponse.class);
                if (doctorInfoResponse.getState() == ConnectionPresenter.SUCCESS) {

                    TimesAdapter timesAdapter = new TimesAdapter(new ArrayList<>(doctorInfoResponse.getTimes()),DoctorInformationActivity.this, doctorsItem);
                    ViewOperations.setRVHorzontial(DoctorInformationActivity.this, timesRecyclerView);
                    timesRecyclerView.setAdapter(timesAdapter);

                    List<DoctorServicesItem> doctorServicesItems = doctorInfoResponse.getDoctorServices();

                    String[] tags = new String[doctorServicesItems.size()];

                    for (int i = 0; i < doctorInfoResponse.getDoctorServices().size(); i++) {
                        tags[i] = doctorServicesItems.get(i).getName();
                    }

                    servicesTags.setTags(tags);

                } else {
                }

            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(DoctorInformationActivity.this, "wrong"+error.toString(), Toast.LENGTH_SHORT).show();
            }

        });
        connectionPresenter.connect(jsonObject, true , false);
    }

    private void setEvents(){

        ratingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DoctorInformationActivity.this,RatingActivity.class);
                intent.putExtra("doctorId",doctorsItem.getDoctorId());
                startActivity(intent);
            }
        });


        favouriteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SharedPreferenceHelper.isLogined(DoctorInformationActivity.this)) {
                    addDeleteFavourite(!doctorsItem.isFavourite());
                }
            }
        });

        shareImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                share();
            }
        });

        messageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DoctorInformationActivity.this, ChatActivity.class);
                ChattersItem chattersItem = new ChattersItem();
                chattersItem.setDoctorId(doctorsItem.getDoctorId());
                intent.putExtra("chattersItemDetails",chattersItem);
                 startActivity(intent);
            }
        });


    }


    public void share( ) {
        int applicationNameId = getApplicationInfo().labelRes;
        final String appPackageName = getPackageName();
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, getString(applicationNameId));
        String text = getResources().getString(R.string.instal_this);
        String link = "https://play.google.com/store/apps/details?id=" + appPackageName;
        i.putExtra(Intent.EXTRA_TEXT, doctorsItem.getFirstName()+" "+doctorsItem.getLastName() + " \n " +  doctorsItem.getAbout() + " \n " + text + " \n " + link);
        startActivity(Intent.createChooser(i, "Share link:"));
    }



}
