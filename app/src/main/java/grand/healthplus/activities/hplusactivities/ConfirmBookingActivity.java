package grand.healthplus.activities.hplusactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.hcareactivities.HCareMainActivity;
import grand.healthplus.models.hplusmodels.BaseResponse;
import grand.healthplus.models.hplusmodels.DaysTimesModel;
import grand.healthplus.models.hplusmodels.DoctorsItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

/**
 * Created by sameh on 4/18/18.
 */

public class ConfirmBookingActivity extends AppCompatActivity {

    @BindView(R.id.iv_confirm_booking_image)
    ImageView photoImageView;
    @BindView(R.id.tv_health_care_name)
    TextView docotorNameTextView;
    @BindView(R.id.tv_confirm_booking_specialization)
    TextView specializatoinTextView;
    @BindView(R.id.tv_confirm_booking_time)
    TextView timeTextView;
    @BindView(R.id.tv_confirm_booking_date)
    TextView dateTextView;
    @BindView(R.id.tv_confirm_booking_address)
    TextView addressTextView;
    @BindView(R.id.tv_confirm_booking_pationt_name)
    TextView pationtNameTextView;
    @BindView(R.id.tv_confirm_booking_views)
    TextView viewsTextView;
    @BindView(R.id.btn_confirm_booking_confirm)
    Button confirmBooking;

    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;

    DaysTimesModel daysTimesModel;
    DoctorsItem doctorsItem;

    String language="",time="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_booking);
        ButterKnife.bind(this);

        doctorsItem = ((DoctorsItem) getIntent().getSerializableExtra("doctorDetails"));
        daysTimesModel = (DaysTimesModel) getIntent().getSerializableExtra("dayDetails");
        language = SharedPreferenceHelper.getCurrentLanguage(this);


        MUT.setSelectedLanguage(this);
        if(SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")){
            backImageView.setRotation(180);
        }

        setDoctorDetails();
    }


    private void setDoctorDetails() {

        if(doctorsItem==null)return ;

        if(doctorsItem.getProfileImage()!=null)
        ConnectionPresenter.loadImage(photoImageView, "doctor_profile/" + doctorsItem.getProfileImage());

        docotorNameTextView.setText(doctorsItem.getFirstName() + " " + doctorsItem.getLastName());
        specializatoinTextView.setText(doctorsItem.getMainSpecializationName());
        addressTextView.setText(doctorsItem.getStreetName() + ", " + doctorsItem.getBuildingNumber() + ", " + doctorsItem.getApartment() + ", " + doctorsItem.getFloor() + ", " + doctorsItem.getLandmark());





        if(getIntent().getStringExtra("time") == null || getIntent().getStringExtra("time").equals("null")){
            timeTextView.setText(getResources().getString(R.string.book_first));
        }else {

            time = getIntent().getStringExtra("time");

            if (language.equals("ar")) {
                String pmOrAm = "" + time.charAt(time.length() - 2) + time.charAt(time.length() - 1);

                if (pmOrAm.equals("AM")) {
                    time = time.replace(pmOrAm, "ص");
                } else {
                    time = time.replace(pmOrAm, "م");
                }

            }else {
                time = getIntent().getStringExtra("time");
            }

            timeTextView.setText("" + time);
        }

        String dayName = daysTimesModel.getTimesItem().getDay();

        if (language.equals("ar")) {
            if (dayName.equals("Saturday")) {
                dayName = "السبت";
            } else if (dayName.equals("Sunday")) {
                dayName = "الاحد";
            } else if (dayName.equals("Monday")) {
                dayName = "الاثنين";
            } else if (dayName.equals("Tuesday")) {
                dayName = "الثلاثاء";
            } else if (dayName.equals("Wednesday")) {
                dayName = "الاربعاء";
            } else if (dayName.equals("Thursday")) {
                dayName = "الخميس";
            } else if (dayName.equals("Friday")) {
                dayName = "الجمعة";
            }
        } else {
            dayName = daysTimesModel.getTimesItem().getDay();
        }

        dateTextView.setText(dayName + " " + daysTimesModel.getTimesItem().getDate());
        viewsTextView.setText(doctorsItem.getViews() + " " + getResources().getString(R.string.views));

        if(SharedPreferenceHelper.getUserDetails(this)!=null) {
            pationtNameTextView.setText(SharedPreferenceHelper.getUserDetails(this).getUsername() + " (" + getResources().getString(R.string.patient) + ")");
            pationtNameTextView.setVisibility(View.VISIBLE);
        }
        setEvents();
    }

    private void setEvents() {
        confirmBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPreferenceHelper.isLogined(ConfirmBookingActivity.this)) {
                    confirmBooking();
                }
            }
        });
    }

    private void confirmBooking() {

        JSONObject sentData = new JSONObject();

        try {
            sentData.put("method", "confirm_booking");
            sentData.put("doctor_id", doctorsItem.getDoctorId());
            sentData.put("date", daysTimesModel.getTimesItem().getDate());
            sentData.put("time", getIntent().getStringExtra("time"));
            sentData.put("type", daysTimesModel.getTimesItem().getType());
            sentData.put("patient_id", SharedPreferenceHelper.getUserDetails(this).getId());


        } catch (Exception e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                BaseResponse baseResponse = new Gson().fromJson(response.toString(), BaseResponse.class);
                if (baseResponse.getState() == ConnectionPresenter.SUCCESS) {
                    MUT.successDialog(ConfirmBookingActivity.this);
                    confirmBooking.setEnabled(false);
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(sentData, true, false);
    }

    public void goBack(View view) {
        finish();
    }

}