package grand.healthplus.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration.Builder;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;


public class ZoomedImageActivity extends Activity {
    Bitmap bitmap;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.iv_actionbar_icon)
    ImageView backImageView;
    @BindView(R.id.imageView)
    SubsamplingScaleImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoomed_image);
        ButterKnife.bind(this);


        MUT.setSelectedLanguage(this);
        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            backImageView.setRotation(180);
        }

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.mipmap.ic_launcher)
                .showImageOnFail(R.mipmap.ic_launcher)
                .cacheInMemory(true).cacheOnDisk(true).build();

        Builder img = new Builder(
                getApplicationContext());
        img.threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.loadImage(getIntent().getStringExtra("image"), options,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri,
                                                  View view, Bitmap loadedImage) {
                        bitmap = loadedImage;
                        imageView.setImage(ImageSource.bitmap(loadedImage));
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }


    public void goBack(View view) {
        finish();
    }


}
