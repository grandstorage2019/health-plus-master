package grand.healthplus.models.trashmodels;

/**
 * Created by sameh on 4/2/18.
 */

public class SpinnerModel {
    int id;
    String title;

    public SpinnerModel(int id,String title){
        this.id=id;
        this.title=title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

}
