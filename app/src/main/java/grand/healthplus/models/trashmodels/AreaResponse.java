package grand.healthplus.models.trashmodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.healthplus.models.trashmodels.AreaItem;


public class AreaResponse{

	@SerializedName("data")
	private List<AreaItem> areas;

	@SerializedName("status")
	private String status;

	public void setAreas(List<AreaItem> areas){
		this.areas = areas;
	}

	public List<AreaItem> getAreas(){
		return areas;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AreaResponse{" + 
			"areas = '" + areas + '\'' +
			",status = '" + status + '\'' + 
			"}";
		}
}