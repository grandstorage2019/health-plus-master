package grand.healthplus.models.trashmodels;


import com.google.gson.annotations.SerializedName;


public class UserItem {

	@SerializedName("google_id")
	private String googleId;

	@SerializedName("os")
	private String os;

	@SerializedName("app_version")
	private String appVersion;

	@SerializedName("gender")
	private String gender;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("firebase_token")
	private String firebaseToken;

	@SerializedName("facebook_id")
	private String facebookId;

	@SerializedName("birth_day")
	private String birthDay;

	@SerializedName("registred")
	private int registred;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("phone")
	private String phone;

	@SerializedName("device_token")
	private Object deviceToken;

	@SerializedName("name")
	private String name;

	@SerializedName("model")
	private String model;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("country_id")
	private String countryId;

	@SerializedName("status")
	private int status;

	@SerializedName("username")
	private String username;

	public void setGoogleId(String googleId){
		this.googleId = googleId;
	}

	public String getGoogleId(){
		return googleId;
	}

	public void setOs(String os){
		this.os = os;
	}

	public String getOs(){
		return os;
	}

	public void setAppVersion(String appVersion){
		this.appVersion = appVersion;
	}

	public String getAppVersion(){
		return appVersion;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setFirebaseToken(String firebaseToken){
		this.firebaseToken = firebaseToken;
	}

	public String getFirebaseToken(){
		return firebaseToken;
	}

	public void setFacebookId(String facebookId){
		this.facebookId = facebookId;
	}

	public String getFacebookId(){
		return facebookId;
	}

	public void setBirthDay(String birthDay){
		this.birthDay = birthDay;
	}

	public String getBirthDay(){
		return birthDay;
	}

	public void setRegistred(int registred){
		this.registred = registred;
	}

	public int getRegistred(){
		return registred;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDeviceToken(Object deviceToken){
		this.deviceToken = deviceToken;
	}

	public Object getDeviceToken(){
		return deviceToken;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setModel(String model){
		this.model = model;
	}

	public String getModel(){
		return model;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setCountryId(String countryId){
		this.countryId = countryId;
	}

	public String getCountryId(){
		return countryId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"google_id = '" + googleId + '\'' + 
			",os = '" + os + '\'' + 
			",app_version = '" + appVersion + '\'' + 
			",gender = '" + gender + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",firebase_token = '" + firebaseToken + '\'' + 
			",facebook_id = '" + facebookId + '\'' + 
			",birth_day = '" + birthDay + '\'' + 
			",registred = '" + registred + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",phone = '" + phone + '\'' + 
			",device_token = '" + deviceToken + '\'' + 
			",name = '" + name + '\'' + 
			",model = '" + model + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",country_id = '" + countryId + '\'' + 
			",status = '" + status + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}