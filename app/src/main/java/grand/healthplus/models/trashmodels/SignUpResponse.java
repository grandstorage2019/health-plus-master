package grand.healthplus.models.trashmodels;


import com.google.gson.annotations.SerializedName;


public class SignUpResponse{

	@SerializedName("data")
	private UserItem data;

	@SerializedName("status")
	private String status;

	public void setData(UserItem data){
		this.data = data;
	}

	public UserItem getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SignUpResponse{" + 
			"data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}