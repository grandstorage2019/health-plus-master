package grand.healthplus.models.trashmodels;


import com.google.gson.annotations.SerializedName;


public class DeviceInfoResponse {

	@SerializedName("data")
	private DeviceInfo deviceInfo;

	@SerializedName("status")
	private String status;

	public void setDeviceInfo(DeviceInfo deviceInfo){
		this.deviceInfo = deviceInfo;
	}

	public DeviceInfo getDeviceInfo(){
		return deviceInfo;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"device_info = '" + deviceInfo + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}