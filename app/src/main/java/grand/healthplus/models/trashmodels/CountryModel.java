package grand.healthplus.models.trashmodels;


import com.google.gson.annotations.SerializedName;


public class CountryModel {

	@SerializedName("name_ar")
	private String nameAr;

	@SerializedName("id")
	private int id;

	@SerializedName("name_en")
	private String nameEn;

	public void setNameAr(String nameAr){
		this.nameAr = nameAr;
	}

	public String getNameAr(){
		return nameAr;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setNameEn(String nameEn){
		this.nameEn = nameEn;
	}

	public String getNameEn(){
		return nameEn;
	}

	public String getName(String language){
		return language.equals("ar")?getNameAr():getNameEn();
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"name_ar = '" + nameAr + '\'' + 
			",id = '" + id + '\'' + 
			",name_en = '" + nameEn + '\'' + 
			"}";
		}
}