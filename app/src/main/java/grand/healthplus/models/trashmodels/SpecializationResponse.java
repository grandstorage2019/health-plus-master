package grand.healthplus.models.trashmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class SpecializationResponse{

	@SerializedName("data")
	private List<SpecializationItem> data;

	@SerializedName("status")
	private String status;

	public void setData(List<SpecializationItem> data){
		this.data = data;
	}

	public List<SpecializationItem> getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SpecializationResponse{" + 
			"data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}