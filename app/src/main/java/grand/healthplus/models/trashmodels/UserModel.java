package grand.healthplus.models.trashmodels;

/**
 * Created by sameh on 1/11/18.
 */


import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("id")
    private int id;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("grand_father_name")
    private String grandFatherName;
    @SerializedName("family_name")
    private String familyName;
    @SerializedName("password")
    private String password;
    @SerializedName("id_number")
    private int idNumber;
    @SerializedName("id_start_date")
    private String idStartDate;
    @SerializedName("id_end_date")
    private String idEndDate;
    @SerializedName("email")
    private String email;
    @SerializedName("birth_day_date")
    private String birthDayDate;
    @SerializedName("birth_day_place")
    private String birthDayPlace;
    @SerializedName("save_place")
    private String savePlace;
    @SerializedName("build_name")
    private String buildName;
    @SerializedName("build_activity")
    private String buildActivity;
    @SerializedName("commercial")
    private String commercial;
    @SerializedName("branch")
    private String branch;
    @SerializedName("phone")
    private String phone;

    @SerializedName("photo")
    private String photo;


    @SerializedName("iban_number")
    private int ibanNumber;
    @SerializedName("account_number")
    private int accountNumber;
    @SerializedName("bank_id")
    private int bankId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGrandFatherName() {
        return grandFatherName;
    }

    public void setGrandFatherName(String grandFatherName) {
        this.grandFatherName = grandFatherName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(int idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdStartDate() {
        return idStartDate;
    }

    public void setIdStartDate(String idStartDate) {
        this.idStartDate = idStartDate;
    }

    public String getIdEndDate() {
        return idEndDate;
    }

    public void setIdEndDate(String idEndDate) {
        this.idEndDate = idEndDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDayDate() {
        return birthDayDate;
    }

    public void setBirthDayDate(String birthDayDate) {
        this.birthDayDate = birthDayDate;
    }

    public String getBirthDayPlace() {
        return birthDayPlace;
    }

    public void setBirthDayPlace(String birthDayPlace) {
        this.birthDayPlace = birthDayPlace;
    }

    public String getSavePlace() {
        return savePlace;
    }

    public void setSavePlace(String savePlace) {
        this.savePlace = savePlace;
    }

    public String getBuildName() {
        return buildName;
    }

    public void setBuildName(String buildName) {
        this.buildName = buildName;
    }

    public String getBuildActivity() {
        return buildActivity;
    }

    public void setBuildActivity(String buildActivity) {
        this.buildActivity = buildActivity;
    }

    public String getCommercial() {
        return commercial;
    }

    public void setCommercial(String commercial) {
        this.commercial = commercial;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getIbanNumber() {
        return ibanNumber;
    }

    public void setIbanNumber(int ibanNumber) {
        this.ibanNumber = ibanNumber;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public int getBankId() {
        return bankId;
    }
}