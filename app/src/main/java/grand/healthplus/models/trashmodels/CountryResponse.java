package grand.healthplus.models.trashmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class CountryResponse{

	@SerializedName("data")
	private List<CountryModel> countryItems;

	@SerializedName("status")
	private String status;

	public void setCountryItems(List<CountryModel> countryItems){
		this.countryItems = countryItems;
	}

	public List<CountryModel> getCountryItems(){
		return countryItems;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CountryResponse{" + 
			"countryItems = '" + countryItems + '\'' +
			",status = '" + status + '\'' + 
			"}";
		}
}