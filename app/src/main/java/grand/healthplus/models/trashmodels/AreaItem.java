package grand.healthplus.models.trashmodels;


import com.google.gson.annotations.SerializedName;


public class AreaItem {

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("name_ar")
	private String nameAr;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("governorate_id")
	private int governorateId;

	@SerializedName("name_en")
	private String nameEn;

	@SerializedName("status")
	private int status;

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setNameAr(String nameAr){
		this.nameAr = nameAr;
	}

	public String getNameAr(){
		return nameAr;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setGovernorateId(int governorateId){
		this.governorateId = governorateId;
	}

	public int getGovernorateId(){
		return governorateId;
	}

	public void setNameEn(String nameEn){
		this.nameEn = nameEn;
	}

	public String getNameEn(){
		return nameEn;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public String getName(String language){
		return language.equals("ar")?getNameAr():getNameEn();
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"updated_at = '" + updatedAt + '\'' + 
			",name_ar = '" + nameAr + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",governorate_id = '" + governorateId + '\'' + 
			",name_en = '" + nameEn + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}