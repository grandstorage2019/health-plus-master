package grand.healthplus.models.trashmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class GovernorateResponse{

	@SerializedName("data")
	private List<GovernorateItem> data;

	@SerializedName("status")
	private String status;

	public void setData(List<GovernorateItem> data){
		this.data = data;
	}

	public List<GovernorateItem> getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GovernorateResponse{" + 
			"data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}