package grand.healthplus.models.trashmodels;


import com.google.gson.annotations.SerializedName;


public class DeviceInfo{

	@SerializedName("os")
	private String os;

	@SerializedName("app_version")
	private String appVersion;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("model")
	private String model;

	@SerializedName("id")
	private int id;

	@SerializedName("firebase_token")
	private String firebaseToken;

	public void setOs(String os){
		this.os = os;
	}

	public String getOs(){
		return os;
	}

	public void setAppVersion(String appVersion){
		this.appVersion = appVersion;
	}

	public String getAppVersion(){
		return appVersion;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setDeviceToken(String deviceToken){
		this.deviceToken = deviceToken;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setModel(String model){
		this.model = model;
	}

	public String getModel(){
		return model;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFirebaseToken(String firebaseToken){
		this.firebaseToken = firebaseToken;
	}

	public String getFirebaseToken(){
		return firebaseToken;
	}

	@Override
 	public String toString(){
		return 
			"DeviceInfo{" + 
			"os = '" + os + '\'' + 
			",app_version = '" + appVersion + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",device_token = '" + deviceToken + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",model = '" + model + '\'' + 
			",id = '" + id + '\'' + 
			",firebase_token = '" + firebaseToken + '\'' + 
			"}";
		}
}