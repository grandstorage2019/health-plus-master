package grand.healthplus.models.hplusmodels;


import com.google.gson.annotations.SerializedName;


public class RatingItem{

	@SerializedName("name")
	private String name;

	@SerializedName("comment")
	private String comment;

	@SerializedName("stars")
	private double stars;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public String getComment(){
		return comment;
	}

	public void setStars(double stars){
		this.stars = stars;
	}

	public double getStars(){
		return stars;
	}

	@Override
 	public String toString(){
		return 
			"RatingItem{" + 
			"name = '" + name + '\'' + 
			",comment = '" + comment + '\'' + 
			",stars = '" + stars + '\'' + 
			"}";
		}
}