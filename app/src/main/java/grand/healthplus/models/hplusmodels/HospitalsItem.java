package grand.healthplus.models.hplusmodels;


import com.google.gson.annotations.SerializedName;


public class HospitalsItem{

	@SerializedName("image")
	private String image;

	@SerializedName("name")
	private String name;

	@SerializedName("link")
	private String link;

	@SerializedName("is_promited")
	private boolean isPromited;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLink(String link){
		this.link = link;
	}

	public String getLink(){
		return link;
	}

	public void setIsPromited(boolean isPromited){
		this.isPromited = isPromited;
	}

	public boolean isIsPromited(){
		return isPromited;
	}


}