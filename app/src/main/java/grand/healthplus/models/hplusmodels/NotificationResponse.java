package grand.healthplus.models.hplusmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class NotificationResponse {

    @SerializedName("state")
    private int state;

    @SerializedName("notifications")
    private List<NotificationsItem> notifications;

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public void setNotifications(List<NotificationsItem> notifications) {
        this.notifications = notifications;
    }

    public List<NotificationsItem> getNotifications() {
        return notifications;
    }


}