package grand.healthplus.models.hplusmodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class DoctorResponse{

	@SerializedName("doctors")
	private List<DoctorsItem> doctors;

	@SerializedName("state")
	private int state;

	public void setDoctors(List<DoctorsItem> doctors){
		this.doctors = doctors;
	}

	public List<DoctorsItem> getDoctors(){
		return doctors;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"DoctorResponse{" + 
			"doctors = '" + doctors + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}