package grand.healthplus.models.hplusmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class GovernorateResponse{

	@SerializedName("governorates")
	private List<GovernoratesItem> governorates;

	@SerializedName("state")
	private int state;

	public void setGovernorates(List<GovernoratesItem> governorates){
		this.governorates = governorates;
	}

	public List<GovernoratesItem> getGovernorates(){
		return governorates;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"GovernorateResponse{" + 
			"governorates = '" + governorates + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}