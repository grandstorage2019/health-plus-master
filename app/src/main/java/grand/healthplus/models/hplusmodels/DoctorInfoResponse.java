package grand.healthplus.models.hplusmodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class DoctorInfoResponse{

	@SerializedName("times")
	private List<TimesItem> times;

	@SerializedName("state")
	private int state;

	@SerializedName("doctor_services")
	private List<DoctorServicesItem> doctorServices;

	public void setTimes(List<TimesItem> times){
		this.times = times;
	}

	public List<TimesItem> getTimes(){
		return times;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setDoctorServices(List<DoctorServicesItem> doctorServices){
		this.doctorServices = doctorServices;
	}

	public List<DoctorServicesItem> getDoctorServices(){
		return doctorServices;
	}

	@Override
 	public String toString(){
		return 
			"DoctorInfoResponse{" + 
			"times = '" + times + '\'' + 
			",state = '" + state + '\'' + 
			",doctor_services = '" + doctorServices + '\'' + 
			"}";
		}
}