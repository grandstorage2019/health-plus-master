package grand.healthplus.models.hplusmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class RatingResponse{

	@SerializedName("raters")
	private int raters;

	@SerializedName("rating")
	private List<RatingItem> rating;

	@SerializedName("total_rating")
	private double totalRating;

	@SerializedName("state")
	private int state;

	public void setRaters(int raters){
		this.raters = raters;
	}

	public int getRaters(){
		return raters;
	}

	public void setRating(List<RatingItem> rating){
		this.rating = rating;
	}

	public List<RatingItem> getRating(){
		return rating;
	}

	public void setTotalRating(double totalRating){
		this.totalRating = totalRating;
	}

	public double getTotalRating(){
		return totalRating;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"RatingResponse{" + 
			"raters = '" + raters + '\'' + 
			",rating = '" + rating + '\'' + 
			",total_rating = '" + totalRating + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}