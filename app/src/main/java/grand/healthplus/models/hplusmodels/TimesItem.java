package grand.healthplus.models.hplusmodels;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class TimesItem implements Serializable{

	@SerializedName("to_time")
	private String toTime;

	@SerializedName("day")
	private String day;

	@SerializedName("waiting_time")
	private String waitingTime;

	@SerializedName("from_time")
	private String fromTime;

	@SerializedName("date")
	private String date;

	@SerializedName("time_type")
	private String type;

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getDay() {
		return day;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setWaitingTime(String waitingTime) {
		this.waitingTime = waitingTime;
	}

	public String getWaitingTime() {
		return waitingTime;
	}

	public String getDate() {
		return date;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}