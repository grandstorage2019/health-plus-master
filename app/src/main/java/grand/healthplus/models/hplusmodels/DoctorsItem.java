package grand.healthplus.models.hplusmodels;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class DoctorsItem implements Serializable{

	@SerializedName("num_reservation")
	private String numReservation;

	@SerializedName("main_specialization_name")
	private String mainSpecializationName;

	@SerializedName("about")
	private String about;

	@SerializedName("from_date")
	private String fromDate;


	@SerializedName("building_number")
	private String buildingNumber;

	@SerializedName("waiting_hours")
	private int waitingHours;


	@SerializedName("waiting_minutes")
	private int waitingMinutes;


	@SerializedName("specializations")
	private String specializations;


	@SerializedName("waiting_time")
	private String waitingTime;

	@SerializedName("clinic_number")
	private String clinicNumber;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("clinic_name")
	private String clinicName;

	@SerializedName("type")
	private String type;

	@SerializedName("from_time")
	private String fromTime;

	@SerializedName("street_name")
	private String streetName;

	@SerializedName("doctor_id")
	private int doctorId;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("to_date")
	private String toDate;

	@SerializedName("rate")
	private double rate;

	@SerializedName("price")
	private double price;

	@SerializedName("id")
	private int id;

	@SerializedName("to_time")
	private String toTime;

	@SerializedName("level")
	private String level;

	@SerializedName("floor")
	private String floor;

	@SerializedName("landmark")
	private String landmark;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("apartment")
	private String apartment;


	@SerializedName("is_favourite")
	private boolean isFavourite;

	@SerializedName("available_from")
	private String availableFrom;

	@SerializedName("views")
	private int views;

	@SerializedName("raters")
	private int raters;

	@SerializedName("flat_number")
	private String flatNumber;

	@SerializedName("block")
	private String block;

	@SerializedName("area_name")
	private String areaName;

	@SerializedName("title")
	private String title;


	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAvailableFrom(String availableFrom) {
		this.availableFrom = availableFrom;
	}


	public String getAvailableFrom() {
		return availableFrom;
	}



	public void setNumReservation(String numReservation){
		this.numReservation = numReservation;
	}

	public String getNumReservation(){
		return numReservation;
	}

	public void setFromDate(String fromDate){
		this.fromDate = fromDate;
	}

	public String getFromDate(){
		return fromDate;
	}

	public void setWaitingTime(String waitingTime){
		this.waitingTime = waitingTime;
	}

	public String getWaitingTime(){
		return waitingTime;
	}

	public void setClinicNumber(String clinicNumber){
		this.clinicNumber = clinicNumber;
	}

	public String getClinicNumber(){
		return clinicNumber;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setClinicName(String clinicName){
		this.clinicName = clinicName;
	}

	public String getClinicName(){
		return clinicName;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setFromTime(String fromTime){
		this.fromTime = fromTime;
	}

	public String getFromTime(){
		return fromTime;
	}

	public void setStreetName(String streetName){
		this.streetName = streetName;
	}

	public String getStreetName(){
		return streetName;
	}

	public void setDoctorId(int doctorId){
		this.doctorId = doctorId;
	}

	public int getDoctorId(){
		return doctorId;
	}

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public void setToDate(String toDate){
		this.toDate = toDate;
	}

	public String getToDate(){
		return toDate;
	}

	public void setRate(double rate){
		this.rate = rate;
	}

	public double getRate(){
		return rate;
	}

	public void setPrice(double price){
		this.price = price;
	}

	public double getPrice(){
		return price;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setToTime(String toTime){
		this.toTime = toTime;
	}

	public String getToTime(){
		return toTime;
	}

	public void setFloor(String floor){
		this.floor = floor;
	}

	public String getFloor(){
		return floor;
	}

	public void setLandmark(String landmark){
		this.landmark = landmark;
	}

	public String getLandmark(){
		return landmark;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setApartment(String apartment){
		this.apartment = apartment;
	}

	public String getApartment(){
		return apartment;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public void setWaitingHours(int waitingHours) {
		this.waitingHours = waitingHours;
	}

	public void setWaitingMinutes(int waitingMinutes) {
		this.waitingMinutes = waitingMinutes;
	}

	public int getWaitingHours() {
		return waitingHours;
	}

	public int getWaitingMinutes() {
		return waitingMinutes;
	}

	public String getSpecializations() {
		return specializations;
	}

	public void setSpecializations(String specializations) {
		this.specializations = specializations;
	}

	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public String getMainSpecializationName() {
		return mainSpecializationName;
	}

	public void setMainSpecializationName(String mainSpecializationName) {
		this.mainSpecializationName = mainSpecializationName;
	}

	public void setFavourite(boolean favourite) {
		isFavourite = favourite;
	}

	public boolean isFavourite() {
		return isFavourite;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public int getViews() {
		return views;
	}

	public void setRaters(int raters) {
		this.raters = raters;
	}

	public int getRaters() {
		return raters;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public String getBlock() {
		return block;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevel() {
		return level;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
}