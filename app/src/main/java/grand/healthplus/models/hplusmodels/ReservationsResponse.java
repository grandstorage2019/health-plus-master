package grand.healthplus.models.hplusmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ReservationsResponse{

	@SerializedName("reservations")
	private List<ReservationsItem> reservations;

	@SerializedName("state")
	private int state;

	public void setReservations(List<ReservationsItem> reservations){
		this.reservations = reservations;
	}

	public List<ReservationsItem> getReservations(){
		return reservations;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"ReservationsResponse{" + 
			"reservations = '" + reservations + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}