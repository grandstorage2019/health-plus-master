package grand.healthplus.models.hplusmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class CountryResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("countries")
	private List<CountriesItem> countries;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setCountries(List<CountriesItem> countries){
		this.countries = countries;
	}

	public List<CountriesItem> getCountries(){
		return countries;
	}

	@Override
 	public String toString(){
		return 
			"CountryResponse{" + 
			"state = '" + state + '\'' + 
			",countries = '" + countries + '\'' + 
			"}";
		}
}