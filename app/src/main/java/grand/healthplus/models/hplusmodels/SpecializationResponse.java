package grand.healthplus.models.hplusmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class SpecializationResponse{

	@SerializedName("specializations")
	private List<SpecializationsItem> specializations;

	@SerializedName("state")
	private int state;

	public void setSpecializations(List<SpecializationsItem> specializations){
		this.specializations = specializations;
	}

	public List<SpecializationsItem> getSpecializations(){
		return specializations;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"SpecializationResponse{" + 
			"specializations = '" + specializations + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}