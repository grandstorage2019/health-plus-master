package grand.healthplus.models.hplusmodels;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class BookingTimesItem implements Serializable{

	@SerializedName("time")
	private String time;

	@SerializedName("is_booked")
	private boolean isBooked;

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setIsBooked(boolean isBooked){
		this.isBooked = isBooked;
	}

	public boolean isIsBooked(){
		return isBooked;
	}

	@Override
 	public String toString(){
		return 
			"BookingTimesItem{" + 
			"time = '" + time + '\'' + 
			",is_booked = '" + isBooked + '\'' + 
			"}";
		}
}