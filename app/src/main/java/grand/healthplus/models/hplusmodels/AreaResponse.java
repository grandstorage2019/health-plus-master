package grand.healthplus.models.hplusmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class AreaResponse{

	@SerializedName("areas")
	private List<AreasItem> areas;

	@SerializedName("state")
	private int state;

	public void setAreas(List<AreasItem> areas){
		this.areas = areas;
	}

	public List<AreasItem> getAreas(){
		return areas;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"AreaResponse{" + 
			"areas = '" + areas + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}