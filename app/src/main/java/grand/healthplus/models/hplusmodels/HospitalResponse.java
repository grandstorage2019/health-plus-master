package grand.healthplus.models.hplusmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class HospitalResponse{

	@SerializedName("hospitals")
	private List<HospitalsItem> hospitals;

	@SerializedName("state")
	private int state;

	public void setHospitals(List<HospitalsItem> hospitals){
		this.hospitals = hospitals;
	}

	public List<HospitalsItem> getHospitals(){
		return hospitals;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"HospitalResponse{" + 
			"hospitals = '" + hospitals + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}