package grand.healthplus.models.hplusmodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class BookingTimesResponse{

	@SerializedName("booking_times")
	private List<BookingTimesItem> bookingTimes;

	@SerializedName("state")
	private int state;

	public void setBookingTimes(List<BookingTimesItem> bookingTimes){
		this.bookingTimes = bookingTimes;
	}

	public List<BookingTimesItem> getBookingTimes(){
		return bookingTimes;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"BookingTimesResponse{" + 
			"booking_times = '" + bookingTimes + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}