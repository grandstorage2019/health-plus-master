package grand.healthplus.models.hplusmodels;

import com.google.gson.annotations.SerializedName;


public class DoctorServicesItem {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }
}