package grand.healthplus.models.hplusmodels;


import java.io.Serializable;
import java.util.ArrayList;

public class DaysTimesModel implements Serializable {

    private  TimesItem timesItem;

    private boolean isOpened=false;

    ArrayList<BookingTimesItem> bookingTimesItems;


    public void setBookingTimesItems(ArrayList<BookingTimesItem> bookingTimesItems) {
        this.bookingTimesItems = bookingTimesItems;
    }


    public ArrayList<BookingTimesItem> getBookingTimesItems() {
        return bookingTimesItems;
    }

    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    public boolean isOpened() {
        return isOpened;
    }




    public void setTimesItem(TimesItem timesItem) {
        this.timesItem = timesItem;
    }

    public TimesItem getTimesItem() {
        return timesItem;
    }
}
