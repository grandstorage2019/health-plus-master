package grand.healthplus.models.hplusmodels;


import com.google.gson.annotations.SerializedName;


public class ReservationsItem{


	@SerializedName("street_name")
	private String streetName;

	@SerializedName("reservation_id")
	private String reservationId;

	@SerializedName("date")
	private String date;

	@SerializedName("floor_name")
	private String floorName;

	@SerializedName("clinic_name")
	private String clinicName;

	@SerializedName("doctor_last_name")
	private String doctorLastName;

	@SerializedName("time")
	private String time;

	@SerializedName("type")
	private String type;

	@SerializedName("phone")
	private String phone;

	@SerializedName("lang")
	private double lang;

	@SerializedName("flat_number")
	private String flatNumber;

	@SerializedName("block")
	private String block;

	@SerializedName("doctor_first_name")
	private String doctorFirstName;

	@SerializedName("floor")
	private String floor;

	@SerializedName("landmark")
	private String landmark;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("title")
	private String title;

	@SerializedName("level")
	private String level;

	@SerializedName("apartment")
	private String apartment;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("lat")
	private double lat;

	@SerializedName("building_number")
	private String buildingNumber;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setFloorName(String floorName){
		this.floorName = floorName;
	}

	public String getFloorName(){
		return floorName;
	}

	public void setClinicName(String clinicName){
		this.clinicName = clinicName;
	}

	public String getClinicName(){
		return clinicName;
	}

	public void setDoctorLastName(String doctorLastName){
		this.doctorLastName = doctorLastName;
	}

	public String getDoctorLastName(){
		return doctorLastName;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setLang(double lang){
		this.lang = lang;
	}

	public double getLang(){
		return lang;
	}

	public void setLandmark(String landmark){
		this.landmark = landmark;
	}

	public String getLandmark(){
		return landmark;
	}

	public void setDoctorFirstName(String doctorFirstName){
		this.doctorFirstName = doctorFirstName;
	}

	public String getDoctorFirstName(){
		return doctorFirstName;
	}

	public void setStreetName(String streetName){
		this.streetName = streetName;
	}

	public String getStreetName(){
		return streetName;
	}

	public void setApartment(String apartment){
		this.apartment = apartment;
	}

	public String getApartment(){
		return apartment;
	}

	public void setLat(double lat){
		this.lat = lat;
	}

	public double getLat(){
		return lat;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getBlock() {
		return block;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getFloor() {
		return floor;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public String getLevel() {
		return level;
	}
}