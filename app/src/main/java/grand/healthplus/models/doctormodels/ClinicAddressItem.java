package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.SerializedName;


public class ClinicAddressItem{

	@SerializedName("area_name")
	private String areaName;

	@SerializedName("landmark_en")
	private String landmarkEn;

	@SerializedName("street_name_en")
	private String streetNameEn;

	@SerializedName("flat_number")
	private String flatNumber;

	@SerializedName("block_en")
	private String block;

	@SerializedName("block_ar")
	private String blockAr;


	@SerializedName("building_number_en")
	private String buildingNumberEn;

	@SerializedName("apartment_ar")
	private String apartmentAr;

	@SerializedName("clinic_number")
	private String clinicNumber;

	@SerializedName("floor_en")
	private String floorEn;

	@SerializedName("clinic_name")
	private String clinicName;

	@SerializedName("landmark_ar")
	private String landmarkAr;

	@SerializedName("area_id")
	private int areaId;

	@SerializedName("street_name_ar")
	private String streetNameAr;

	@SerializedName("building_number_ar")
	private String buildingNumberAr;

	@SerializedName("apartment_en")
	private String apartmentEn;

	@SerializedName("floor_ar")
	private String floorAr;

	@SerializedName("governorate_name")
	private String governorateName;

	@SerializedName("doctor_id")
	private String doctorId;

	@SerializedName("countries_name")
	private String countriesName;

	@SerializedName("id")
	private int id;

	@SerializedName("governorate_id")
	private int governorateId;

	@SerializedName("lang")
	private String lang;

	@SerializedName("lat")
	private String lat;

	@SerializedName("country_id")
	private int countryId;

	public void setAreaName(String areaName){
		this.areaName = areaName;
	}

	public String getAreaName(){
		return areaName;
	}

	public void setLandmarkEn(String landmarkEn){
		this.landmarkEn = landmarkEn;
	}

	public String getLandmarkEn(){
		return landmarkEn;
	}

	public void setStreetNameEn(String streetNameEn){
		this.streetNameEn = streetNameEn;
	}

	public String getStreetNameEn(){
		return streetNameEn;
	}

	public void setBuildingNumberEn(String buildingNumberEn){
		this.buildingNumberEn = buildingNumberEn;
	}

	public String getBuildingNumberEn(){
		return buildingNumberEn;
	}

	public void setApartmentAr(String apartmentAr){
		this.apartmentAr = apartmentAr;
	}

	public String getApartmentAr(){
		return apartmentAr;
	}

	public void setClinicNumber(String clinicNumber){
		this.clinicNumber = clinicNumber;
	}

	public String getClinicNumber(){
		return clinicNumber;
	}

	public void setFloorEn(String floorEn){
		this.floorEn = floorEn;
	}

	public String getFloorEn(){
		return floorEn;
	}

	public void setClinicName(String clinicName){
		this.clinicName = clinicName;
	}

	public String getClinicName(){
		return clinicName;
	}

	public void setLandmarkAr(String landmarkAr){
		this.landmarkAr = landmarkAr;
	}

	public String getLandmarkAr(){
		return landmarkAr;
	}

	public void setAreaId(int areaId){
		this.areaId = areaId;
	}

	public int getAreaId(){
		return areaId;
	}

	public void setStreetNameAr(String streetNameAr){
		this.streetNameAr = streetNameAr;
	}

	public String getStreetNameAr(){
		return streetNameAr;
	}

	public void setBuildingNumberAr(String buildingNumberAr){
		this.buildingNumberAr = buildingNumberAr;
	}

	public String getBuildingNumberAr(){
		return buildingNumberAr;
	}

	public void setApartmentEn(String apartmentEn){
		this.apartmentEn = apartmentEn;
	}

	public String getApartmentEn(){
		return apartmentEn;
	}

	public void setFloorAr(String floorAr){
		this.floorAr = floorAr;
	}

	public String getFloorAr(){
		return floorAr;
	}

	public void setGovernorateName(String governorateName){
		this.governorateName = governorateName;
	}

	public String getGovernorateName(){
		return governorateName;
	}

	public void setDoctorId(String doctorId){
		this.doctorId = doctorId;
	}

	public String getDoctorId(){
		return doctorId;
	}

	public void setCountriesName(String countriesName){
		this.countriesName = countriesName;
	}

	public String getCountriesName(){
		return countriesName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setGovernorateId(int governorateId){
		this.governorateId = governorateId;
	}

	public int getGovernorateId(){
		return governorateId;
	}

	public void setLang(String lang){
		this.lang = lang;
	}

	public String getLang(){
		return lang;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

	public void setCountryId(int countryId){
		this.countryId = countryId;
	}

	public int getCountryId(){
		return countryId;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public String getBlock() {
		return block;
	}

	public void setBlockAr(String blockAr) {
		this.blockAr = blockAr;
	}

	public String getBlockAr() {
		return blockAr;
	}

	@Override
 	public String toString(){
		return 
			"ClinicAddressItem{" + 
			"area_name = '" + areaName + '\'' + 
			",landmark_en = '" + landmarkEn + '\'' + 
			",street_name_en = '" + streetNameEn + '\'' + 
			",building_number_en = '" + buildingNumberEn + '\'' + 
			",apartment_ar = '" + apartmentAr + '\'' + 
			",clinic_number = '" + clinicNumber + '\'' + 
			",floor_en = '" + floorEn + '\'' + 
			",clinic_name = '" + clinicName + '\'' + 
			",landmark_ar = '" + landmarkAr + '\'' + 
			",area_id = '" + areaId + '\'' + 
			",street_name_ar = '" + streetNameAr + '\'' + 
			",building_number_ar = '" + buildingNumberAr + '\'' + 
			",apartment_en = '" + apartmentEn + '\'' + 
			",floor_ar = '" + floorAr + '\'' + 
			",governorate_name = '" + governorateName + '\'' + 
			",doctor_id = '" + doctorId + '\'' + 
			",countries_name = '" + countriesName + '\'' + 
			",id = '" + id + '\'' + 
			",governorate_id = '" + governorateId + '\'' + 
			",lang = '" + lang + '\'' + 
			",lat = '" + lat + '\'' + 
			",country_id = '" + countryId + '\'' + 
			"}";
		}
}