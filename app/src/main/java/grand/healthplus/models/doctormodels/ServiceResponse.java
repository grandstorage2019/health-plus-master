package grand.healthplus.models.doctormodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ServiceResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("services")
	private List<ServicesItem> services;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setServices(List<ServicesItem> services){
		this.services = services;
	}

	public List<ServicesItem> getServices(){
		return services;
	}

	@Override
 	public String toString(){
		return 
			"ServiceResponse{" + 
			"state = '" + state + '\'' + 
			",services = '" + services + '\'' + 
			"}";
		}
}