package grand.healthplus.models.doctormodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class PlanResponse{

	@SerializedName("plans")
	private List<PlansItem> plans;

	@SerializedName("state")
	private int state;

	public void setPlans(List<PlansItem> plans){
		this.plans = plans;
	}

	public List<PlansItem> getPlans(){
		return plans;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"PlanResponse{" + 
			"plans = '" + plans + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}