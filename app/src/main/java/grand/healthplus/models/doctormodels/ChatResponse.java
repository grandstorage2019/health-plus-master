package grand.healthplus.models.doctormodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ChatResponse{

	@SerializedName("chat")
	private List<ChatItem> chat;

	@SerializedName("state")
	private int state;

	public void setChat(List<ChatItem> chat){
		this.chat = chat;
	}

	public List<ChatItem> getChat(){
		return chat;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"ChatResponse{" + 
			"chat = '" + chat + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}