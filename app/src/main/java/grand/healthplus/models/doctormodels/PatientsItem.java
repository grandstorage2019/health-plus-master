package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class PatientsItem implements Serializable{
	@SerializedName("image")
	private String image;

	@SerializedName("gender")
	private String gender;

	@SerializedName("phone")
	private String phone;

	@SerializedName("added_by_doctor_id")
	private int addedByDoctorId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("country_id")
	private int countryId;

	@SerializedName("email")
	private String email;

	@SerializedName("notes")
	private String notes;

	@SerializedName("insurance_company")
	private String insuranceCompany;




	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setAddedByDoctorId(int addedByDoctorId){
		this.addedByDoctorId = addedByDoctorId;
	}

	public int getAddedByDoctorId(){
		return addedByDoctorId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCountryId(int countryId){
		this.countryId = countryId;
	}

	public int getCountryId(){
		return countryId;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getNotes() {
		return notes;
	}



	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}
}