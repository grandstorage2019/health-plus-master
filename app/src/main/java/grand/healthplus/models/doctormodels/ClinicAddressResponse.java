package grand.healthplus.models.doctormodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ClinicAddressResponse{

	@SerializedName("clinic_address")
	private List<ClinicAddressItem> clinicAddress;

	@SerializedName("state")
	private int state;

	public void setClinicAddress(List<ClinicAddressItem> clinicAddress){
		this.clinicAddress = clinicAddress;
	}

	public List<ClinicAddressItem> getClinicAddress(){
		return clinicAddress;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"ClinicAddressResponse{" + 
			"clinic_address = '" + clinicAddress + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}