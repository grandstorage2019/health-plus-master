package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.SerializedName;

public class SubSpecializationsItem {

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("checked")
	boolean checked;

	public boolean isChecked() {
		return checked;
	}


	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}



	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"SubSepcializationsItem{" + 
			"name = '" + name + '\'' +
			",id = '" + id + '\'' + 
			"}";
		}
}