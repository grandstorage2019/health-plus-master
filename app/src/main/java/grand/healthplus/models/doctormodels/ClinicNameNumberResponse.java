package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.SerializedName;


public class ClinicNameNumberResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("clinic_name_number")
	private ClinicNameNumber clinicNameNumber;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setClinicNameNumber(ClinicNameNumber clinicNameNumber){
		this.clinicNameNumber = clinicNameNumber;
	}

	public ClinicNameNumber getClinicNameNumber(){
		return clinicNameNumber;
	}

	@Override
 	public String toString(){
		return 
			"ClinicNameNumberResponse{" + 
			"state = '" + state + '\'' + 
			",clinic_name_number = '" + clinicNameNumber + '\'' + 
			"}";
		}
}