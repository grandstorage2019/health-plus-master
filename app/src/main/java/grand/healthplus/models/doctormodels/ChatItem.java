package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.SerializedName;


public class ChatItem{

	@SerializedName("massage")
	private String massage;

	@SerializedName("who")
	private String who;

	public void setMassage(String massage){
		this.massage = massage;
	}

	public String getMassage(){
		return massage;
	}

	public void setWho(String who){
		this.who = who;
	}

	public String getWho(){
		return who;
	}

	@Override
 	public String toString(){
		return 
			"ChatItem{" + 
			"massage = '" + massage + '\'' + 
			",who = '" + who + '\'' + 
			"}";
		}
}