package grand.healthplus.models.doctormodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class SubSpecializationResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("sub_sepcializations")
	private List<SubSpecializationsItem> subSepcializations;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setSubSepcializations(List<SubSpecializationsItem> subSepcializations){
		this.subSepcializations = subSepcializations;
	}

	public List<SubSpecializationsItem> getSubSepcializations(){
		return subSepcializations;
	}

	@Override
 	public String toString(){
		return 
			"SubSpecializationResponse{" + 
			"state = '" + state + '\'' + 
			",sub_sepcializations = '" + subSepcializations + '\'' + 
			"}";
		}
}