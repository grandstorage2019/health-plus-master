package grand.healthplus.models.doctormodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DoctorPatientsResponse{

	@SerializedName("patients")
	private List<PatientsItem> patients;

	@SerializedName("state")
	private int state;

	public void setPatients(List<PatientsItem> patients){
		this.patients = patients;
	}

	public List<PatientsItem> getPatients(){
		return patients;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"DoctorPatientsResponse{" + 
			"patients = '" + patients + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}