package grand.healthplus.models.doctormodels.registrationmodel;


import com.google.gson.annotations.SerializedName;

public class UserItem{

	@SerializedName("google_id")
	private String googleId;

	@SerializedName("app_version")
	private String appVersion;

	@SerializedName("gender")
	private String gender;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("lname_en")
	private String lnameEn;

	@SerializedName("password")
	private String password;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("specialization_id")
	private int specializationId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("price")
	private double price;

	@SerializedName("lname_ar")
	private String lnameAr;

	@SerializedName("model")
	private String model;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("about_en")
	private String aboutEn;

	@SerializedName("views")
	private int views;

	@SerializedName("os")
	private String os;

	@SerializedName("device_id")
	private String deviceId;

	@SerializedName("fname_ar")
	private String fnameAr;

	@SerializedName("about_ar")
	private String aboutAr;

	@SerializedName("firebase_token")
	private String firebaseToken;

	@SerializedName("fname_en")
	private String fnameEn;

	@SerializedName("is_accapted_by_admin")
	private String isAccaptedByAdmin;

	@SerializedName("facebook_id")
	private String facebookId;

	@SerializedName("birth_day")
	private String birthDay;

	@SerializedName("license")
	private String license;

	@SerializedName("registred")
	private String registred;

	@SerializedName("title_ar")
	private String titleAr;

	@SerializedName("phone")
	private String phone;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("title_en")
	private String titleEn;

	@SerializedName("plan_id")
	private int planId;

	@SerializedName("country_id")
	private int countryId;

	@SerializedName("status")
	private boolean status;

	@SerializedName("username")
	private String username;

	public void setGoogleId(String googleId){
		this.googleId = googleId;
	}

	public String getGoogleId(){
		return googleId;
	}

	public void setAppVersion(String appVersion){
		this.appVersion = appVersion;
	}

	public String getAppVersion(){
		return appVersion;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setLnameEn(String lnameEn){
		this.lnameEn = lnameEn;
	}

	public String getLnameEn(){
		return lnameEn;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public void setSpecializationId(int specializationId){
		this.specializationId = specializationId;
	}

	public int getSpecializationId(){
		return specializationId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setPrice(double price){
		this.price = price;
	}

	public double getPrice(){
		return price;
	}

	public void setLnameAr(String lnameAr){
		this.lnameAr = lnameAr;
	}

	public String getLnameAr(){
		return lnameAr;
	}

	public void setModel(String model){
		this.model = model;
	}

	public String getModel(){
		return model;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setAboutEn(String aboutEn){
		this.aboutEn = aboutEn;
	}

	public String getAboutEn(){
		return aboutEn;
	}

	public void setViews(int views){
		this.views = views;
	}

	public int getViews(){
		return views;
	}

	public void setOs(String os){
		this.os = os;
	}

	public String getOs(){
		return os;
	}

	public void setDeviceId(String deviceId){
		this.deviceId = deviceId;
	}

	public String getDeviceId(){
		return deviceId;
	}

	public void setFnameAr(String fnameAr){
		this.fnameAr = fnameAr;
	}

	public String getFnameAr(){
		return fnameAr;
	}

	public void setAboutAr(String aboutAr){
		this.aboutAr = aboutAr;
	}

	public String getAboutAr(){
		return aboutAr;
	}

	public void setFirebaseToken(String firebaseToken){
		this.firebaseToken = firebaseToken;
	}

	public String getFirebaseToken(){
		return firebaseToken;
	}

	public void setFnameEn(String fnameEn){
		this.fnameEn = fnameEn;
	}

	public String getFnameEn(){
		return fnameEn;
	}

	public void setIsAccaptedByAdmin(String isAccaptedByAdmin){
		this.isAccaptedByAdmin = isAccaptedByAdmin;
	}

	public String getIsAccaptedByAdmin(){
		return isAccaptedByAdmin;
	}

	public void setFacebookId(String facebookId){
		this.facebookId = facebookId;
	}

	public String getFacebookId(){
		return facebookId;
	}

	public void setBirthDay(String birthDay){
		this.birthDay = birthDay;
	}

	public String getBirthDay(){
		return birthDay;
	}

	public void setLicense(String license){
		this.license = license;
	}

	public String getLicense(){
		return license;
	}

	public void setRegistred(String registred){
		this.registred = registred;
	}

	public String getRegistred(){
		return registred;
	}

	public void setTitleAr(String titleAr){
		this.titleAr = titleAr;
	}

	public String getTitleAr(){
		return titleAr;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDeviceToken(String deviceToken){
		this.deviceToken = deviceToken;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public void setTitleEn(String titleEn){
		this.titleEn = titleEn;
	}

	public String getTitleEn(){
		return titleEn;
	}

	public void setPlanId(int planId){
		this.planId = planId;
	}

	public int getPlanId(){
		return planId;
	}

	public void setCountryId(int countryId){
		this.countryId = countryId;
	}

	public int getCountryId(){
		return countryId;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"UserItem{" + 
			"google_id = '" + googleId + '\'' + 
			",app_version = '" + appVersion + '\'' + 
			",gender = '" + gender + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",lname_en = '" + lnameEn + '\'' + 
			",password = '" + password + '\'' + 
			",profile_image = '" + profileImage + '\'' + 
			",specialization_id = '" + specializationId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",price = '" + price + '\'' + 
			",lname_ar = '" + lnameAr + '\'' + 
			",model = '" + model + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",about_en = '" + aboutEn + '\'' + 
			",views = '" + views + '\'' + 
			",os = '" + os + '\'' + 
			",device_id = '" + deviceId + '\'' + 
			",fname_ar = '" + fnameAr + '\'' + 
			",about_ar = '" + aboutAr + '\'' + 
			",firebase_token = '" + firebaseToken + '\'' + 
			",fname_en = '" + fnameEn + '\'' + 
			",is_accapted_by_admin = '" + isAccaptedByAdmin + '\'' + 
			",facebook_id = '" + facebookId + '\'' + 
			",birth_day = '" + birthDay + '\'' + 
			",license = '" + license + '\'' + 
			",registred = '" + registred + '\'' + 
			",title_ar = '" + titleAr + '\'' + 
			",phone = '" + phone + '\'' + 
			",device_token = '" + deviceToken + '\'' + 
			",title_en = '" + titleEn + '\'' + 
			",plan_id = '" + planId + '\'' + 
			",country_id = '" + countryId + '\'' + 
			",status = '" + status + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}