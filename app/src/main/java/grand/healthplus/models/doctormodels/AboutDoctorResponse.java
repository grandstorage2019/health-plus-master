package grand.healthplus.models.doctormodels;

import com.google.gson.annotations.SerializedName;

public class AboutDoctorResponse{

	@SerializedName("about_doctor")
	private AboutDoctor aboutDoctor;

	@SerializedName("state")
	private int state;

	public void setAboutDoctor(AboutDoctor aboutDoctor){
		this.aboutDoctor = aboutDoctor;
	}

	public AboutDoctor getAboutDoctor(){
		return aboutDoctor;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"AboutDoctorResponse{" + 
			"about_doctor = '" + aboutDoctor + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}