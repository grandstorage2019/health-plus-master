package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.SerializedName;


public class DoctorHomeResponse{

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("books")
	private int books;

	@SerializedName("specialization")
	private String specialization;

	@SerializedName("name")
	private String name;

	@SerializedName("state")
	private int state;

	@SerializedName("stars")
	private int stars;

	@SerializedName("specialization_id")
	private int specializationId;

	@SerializedName("views")
	private int views;

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBooks(int books){
		this.books = books;
	}

	public int getBooks(){
		return books;
	}

	public void setSpecialization(String specialization){
		this.specialization = specialization;
	}

	public String getSpecialization(){
		return specialization;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setStars(int stars){
		this.stars = stars;
	}

	public int getStars(){
		return stars;
	}

	public void setViews(int views){
		this.views = views;
	}

	public int getViews(){
		return views;
	}

	public void setSpecializationId(int specializationId) {
		this.specializationId = specializationId;
	}

	public int getSpecializationId() {
		return specializationId;
	}

	@Override
 	public String toString(){
		return 
			"DoctorHomeResponse{" + 
			"profile_image = '" + profileImage + '\'' + 
			",books = '" + books + '\'' + 
			",specialization = '" + specialization + '\'' + 
			",state = '" + state + '\'' + 
			",stars = '" + stars + '\'' + 
			",views = '" + views + '\'' + 
			"}";
		}
}