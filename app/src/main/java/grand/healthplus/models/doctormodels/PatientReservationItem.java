package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PatientReservationItem{

	@SerializedName("name")
	private String name;
	@SerializedName("id")
	private String id;
	@SerializedName("phone")
	private String phone;
	@SerializedName("gender")
	private String gender;
	@SerializedName("time")
	private String time;

	@Expose
	private String date;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public String getId() {
		return id;
	}

	public String getPhone() {
		return phone;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}