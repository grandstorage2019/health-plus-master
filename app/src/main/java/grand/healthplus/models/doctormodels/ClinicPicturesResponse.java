package grand.healthplus.models.doctormodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ClinicPicturesResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("clinic_pictures")
	private List<ClinicPicturesItem> clinicPictures;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setClinicPictures(List<ClinicPicturesItem> clinicPictures){
		this.clinicPictures = clinicPictures;
	}

	public List<ClinicPicturesItem> getClinicPictures(){
		return clinicPictures;
	}

	@Override
 	public String toString(){
		return 
			"ClinicPicturesResponse{" + 
			"state = '" + state + '\'' + 
			",clinic_pictures = '" + clinicPictures + '\'' + 
			"}";
		}
}