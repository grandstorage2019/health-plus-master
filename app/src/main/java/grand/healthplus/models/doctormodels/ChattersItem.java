package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.SerializedName;
import java.io.Serializable;


public class ChattersItem implements Serializable{

	@SerializedName("image")
	private String image;

	@SerializedName("patient_id")
	private int patientId;

	@SerializedName("doctor_id")
	private int doctorId;


	@SerializedName("name")
	private String name;

	@SerializedName("massage")
	private String massage;

	@SerializedName("who")
	private String who;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setPatientId(int patientId){
		this.patientId = patientId;
	}

	public int getPatientId(){
		return patientId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setMassage(String massage){
		this.massage = massage;
	}

	public String getMassage(){
		return massage;
	}

	public void setWho(String who){
		this.who = who;
	}

	public String getWho(){
		return who;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public int getDoctorId() {
		return doctorId;
	}

	@Override
 	public String toString(){
		return 
			"ChattersItem{" + 
			"image = '" + image + '\'' + 
			",patient_id = '" + patientId + '\'' + 
			",name = '" + name + '\'' + 
			",massage = '" + massage + '\'' + 
			",who = '" + who + '\'' + 
			"}";
		}
}