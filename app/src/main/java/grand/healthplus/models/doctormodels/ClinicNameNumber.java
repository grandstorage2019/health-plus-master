package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.SerializedName;


public class ClinicNameNumber{

	@SerializedName("clinic_number")
	private String clinicNumber;

	@SerializedName("clinic_name_en")
	private String clinicName;

	@SerializedName("clinic_name_ar")
	private String clinicNameAr;

	@SerializedName("price")
	private double price;

	public void setClinicNumber(String clinicNumber){
		this.clinicNumber = clinicNumber;
	}

	public String getClinicNumber(){
		return clinicNumber;
	}

	public void setClinicName(String clinicName){
		this.clinicName = clinicName;
	}

	public String getClinicName(){
		return clinicName;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getPrice() {
		return price;
	}

	public void setClinicNameAr(String clinicNameAr) {
		this.clinicNameAr = clinicNameAr;
	}

	public String getClinicNameAr() {
		return clinicNameAr;
	}


}