package grand.healthplus.models.doctormodels;


import android.content.Context;

import com.google.gson.annotations.SerializedName;

import grand.healthplus.R;

public class ReservationsItem{

	@SerializedName("have_reservations")
	private boolean haveReservations;

	@SerializedName("day_col")
	private String dayCol;

	@SerializedName("day")
	private String day;

	@SerializedName("num_of_reservations")
	private int reservations;

	@SerializedName("date")
	private String date;

	public void setHaveReservations(boolean haveReservations){
		this.haveReservations = haveReservations;
	}

	public void setReservations(int reservations) {
		this.reservations = reservations;
	}

	public int getReservations() {
		return reservations;
	}

	public boolean isHaveReservations(){
		return haveReservations;
	}

	public void setDayCol(String dayCol){
		this.dayCol = dayCol;
	}

	public String getDayCol(){
		return dayCol;
	}

	public void setDay(String day){
		this.day = day;
	}

	public String getDay(){
		return day;
	}

	public String getDayName(Context context){
		String dayName = "";
		if(dayCol.equals("")){

		}else if(dayCol.equals("Mon")){
			dayName = context.getResources().getString(R.string.monday);

		}else if(dayCol.equals("Tue")){
			dayName = context.getResources().getString(R.string.tuesday);
		}else if(dayCol.equals("Wed")){
			dayName = context.getResources().getString(R.string.wednesday);
		}else if(dayCol.equals("Thu")){
			dayName = context.getResources().getString(R.string.thursday);
		}else if(dayCol.equals("Fri")){
			dayName = context.getResources().getString(R.string.friday);
		}else if(dayCol.equals("Sat")){
			dayName = context.getResources().getString(R.string.saturday);
		}else if(dayCol.equals("Sun")){
			dayName = context.getResources().getString(R.string.sunday);
		}

		return dayName;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	@Override
 	public String toString(){
		return 
			"ReservationsItem{" + 
			"have_reservations = '" + haveReservations + '\'' + 
			",day_col = '" + dayCol + '\'' + 
			",day = '" + day + '\'' + 
			"}";
		}
}