package grand.healthplus.models.doctormodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class PatientReservationResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("patient_reservation")
	private List<PatientReservationItem> patientReservation;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setPatientReservation(List<PatientReservationItem> patientReservation){
		this.patientReservation = patientReservation;
	}

	public List<PatientReservationItem> getPatientReservation(){
		return patientReservation;
	}

	@Override
 	public String toString(){
		return 
			"PatientReservationResponse{" + 
			"state = '" + state + '\'' + 
			",patient_reservation = '" + patientReservation + '\'' + 
			"}";
		}
}