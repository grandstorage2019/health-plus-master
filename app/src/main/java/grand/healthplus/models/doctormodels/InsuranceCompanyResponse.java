package grand.healthplus.models.doctormodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class InsuranceCompanyResponse{

	@SerializedName("insurance_companies")
	private List<InsuranceCompaniesItem> insuranceCompanies;

	@SerializedName("state")
	private int state;

	public void setInsuranceCompanies(List<InsuranceCompaniesItem> insuranceCompanies){
		this.insuranceCompanies = insuranceCompanies;
	}

	public List<InsuranceCompaniesItem> getInsuranceCompanies(){
		return insuranceCompanies;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"InsuranceCompanyResponse{" + 
			"insurance_companies = '" + insuranceCompanies + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}