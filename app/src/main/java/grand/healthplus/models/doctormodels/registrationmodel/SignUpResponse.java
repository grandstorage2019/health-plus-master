package grand.healthplus.models.doctormodels.registrationmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class SignUpResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("user")
	private List<UserItem> user;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setUser(List<UserItem> user){
		this.user = user;
	}

	public List<UserItem> getUser(){
		return user;
	}

	@Override
 	public String toString(){
		return 
			"SignUpResponse{" + 
			"state = '" + state + '\'' + 
			",user = '" + user + '\'' + 
			"}";
		}
}