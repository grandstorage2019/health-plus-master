package grand.healthplus.models.doctormodels;



public class TimeModel {


    private String toTime="";
    private String day="";
    boolean dayOpened=false;
    boolean bookFirst=false;
    private String waitingTime="";
    private String fromTime="";
    private String date="";
    private String type="";
    private String reservations="";
    private String colName="";


    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDay() {
        return day;
    }

    public void setDayOpened(boolean dayOpened) {
        this.dayOpened = dayOpened;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWaitingTime() {
        return waitingTime;
    }

    public boolean isDayOpened() {
        return dayOpened;
    }

    public void setReservations(String reservations) {
        this.reservations = reservations;
    }

    public void setWaitingTime(String waitingTime) {
        this.waitingTime = waitingTime;
    }

    public String getReservations() {
        return reservations;
    }

    public void setBookFirst(boolean bookFirst) {
        this.bookFirst = bookFirst;
    }

    public boolean isBookFirst() {
        return bookFirst;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getColName() {
        return colName;
    }
}
