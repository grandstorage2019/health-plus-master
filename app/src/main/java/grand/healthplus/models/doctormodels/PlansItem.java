package grand.healthplus.models.doctormodels;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PlansItem implements Serializable{

	@SerializedName("name")
	private String name;

	@SerializedName("details")
	private String details;

	@SerializedName("id")
	private String id;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"PlansItem{" + 
			"name = '" + name + '\'' + 
			",details = '" + details + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}