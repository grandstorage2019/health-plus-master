package grand.healthplus.models.hcaremodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ChattersResponse{

	@SerializedName("chatters")
	private List<ChattersItem> chatters;

	@SerializedName("state")
	private int state;

	public void setChatters(List<ChattersItem> chatters){
		this.chatters = chatters;
	}

	public List<ChattersItem> getChatters(){
		return chatters;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"ChattersResponse{" + 
			"chatters = '" + chatters + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}