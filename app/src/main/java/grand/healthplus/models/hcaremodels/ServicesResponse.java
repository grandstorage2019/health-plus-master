package grand.healthplus.models.hcaremodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ServicesResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("raters")
	private int raters;

	@SerializedName("services")
	private List<ServicesItem> services;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setServices(List<ServicesItem> services){
		this.services = services;
	}

	public List<ServicesItem> getServices(){
		return services;
	}

	public void setRaters(int raters) {
		this.raters = raters;
	}

	public int getRaters() {
		return raters;
	}

	@Override
 	public String toString(){
		return 
			"ServicesResponse{" + 
			"state = '" + state + '\'' + 
			",services = '" + services + '\'' + 
			"}";
		}
}