package grand.healthplus.models.hcaremodels;


import com.google.gson.annotations.SerializedName;


public class ReservationsItem{

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("level")
	private String level;

	@SerializedName("reservation_id")
	private int reservationId;

	@SerializedName("area_name")
	private String areaName;

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("price")
	private double price;

	@SerializedName("care_services_name")
	private String careServicesName;

	@SerializedName("area_id")
	private int areaId;

	@SerializedName("lang")
	private double lang;

	@SerializedName("sub_services_name")
	private String subServicesName;

	@SerializedName("location_comment")
	private String locationComment;

	@SerializedName("lat")
	private double lat;

	@SerializedName("logo")
	private String logo;

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public void setAreaName(String areaName){
		this.areaName = areaName;
	}

	public String getAreaName(){
		return areaName;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setPrice(double price){
		this.price = price;
	}

	public double getPrice(){
		return price;
	}

	public void setCareServicesName(String careServicesName){
		this.careServicesName = careServicesName;
	}

	public String getCareServicesName(){
		return careServicesName;
	}

	public void setAreaId(int areaId){
		this.areaId = areaId;
	}

	public int getAreaId(){
		return areaId;
	}

	public void setLang(double lang){
		this.lang = lang;
	}

	public double getLang(){
		return lang;
	}

	public void setSubServicesName(String subServicesName){
		this.subServicesName = subServicesName;
	}

	public String getSubServicesName(){
		return subServicesName;
	}

	public void setLocationComment(String locationComment){
		this.locationComment = locationComment;
	}

	public String getLocationComment(){
		return locationComment;
	}

	public void setLat(double lat){
		this.lat = lat;
	}

	public double getLat(){
		return lat;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	public int getReservationId() {
		return reservationId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevel() {
		return level;
	}

	@Override
 	public String toString(){
		return 
			"ReservationsItem{" + 
			"area_name = '" + areaName + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",price = '" + price + '\'' + 
			",care_services_name = '" + careServicesName + '\'' + 
			",area_id = '" + areaId + '\'' + 
			",lang = '" + lang + '\'' + 
			",sub_services_name = '" + subServicesName + '\'' + 
			",location_comment = '" + locationComment + '\'' + 
			",lat = '" + lat + '\'' + 
			"}";
		}
}