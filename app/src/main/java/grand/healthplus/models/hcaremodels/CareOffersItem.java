package grand.healthplus.models.hcaremodels;


import com.google.gson.annotations.SerializedName;


public class CareOffersItem{

	@SerializedName("image")
	private String image;

	@SerializedName("link")
	private String link;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setLink(String link){
		this.link = link;
	}

	public String getLink(){
		return link;
	}


}