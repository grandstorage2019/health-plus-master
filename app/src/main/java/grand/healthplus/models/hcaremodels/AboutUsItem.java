package grand.healthplus.models.hcaremodels;


import com.google.gson.annotations.SerializedName;

public class AboutUsItem{

	@SerializedName("name")
	private String name;

	@SerializedName("details")
	private String details;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	@Override
 	public String toString(){
		return 
			"AboutUsItem{" + 
			"name = '" + name + '\'' + 
			",details = '" + details + '\'' + 
			"}";
		}
}