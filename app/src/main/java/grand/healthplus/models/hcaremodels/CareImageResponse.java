package grand.healthplus.models.hcaremodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class CareImageResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("care_pictures")
	private List<CarePicturesItem> carePictures;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setCarePictures(List<CarePicturesItem> carePictures){
		this.carePictures = carePictures;
	}

	public List<CarePicturesItem> getCarePictures(){
		return carePictures;
	}

	@Override
 	public String toString(){
		return 
			"CareImageResponse{" + 
			"state = '" + state + '\'' + 
			",care_pictures = '" + carePictures + '\'' + 
			"}";
		}
}