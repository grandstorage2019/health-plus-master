package grand.healthplus.models.hcaremodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class AboutUsResponse{

	@SerializedName("about_us")
	private List<AboutUsItem> aboutUs;

	@SerializedName("state")
	private int state;

	public void setAboutUs(List<AboutUsItem> aboutUs){
		this.aboutUs = aboutUs;
	}

	public List<AboutUsItem> getAboutUs(){
		return aboutUs;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"AboutUsResponse{" + 
			"about_us = '" + aboutUs + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}