package grand.healthplus.models.hcaremodels;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ServicesItem{

	@SerializedName("sub_services")
	private List<SubServicesItem> subServices;

	@SerializedName("logo")
	private String logo;

	@SerializedName("id")
	private int id;

	@SerializedName("name")
	private String name;

	@Expose
	boolean opened=true;


	public boolean isOpened() {
		return opened;
	}

	public void setOpened(boolean opened) {
		this.opened = opened;
	}

	public void setSubServices(List<SubServicesItem> subServices){
		this.subServices = subServices;
	}

	public List<SubServicesItem> getSubServices(){
		return subServices;
	}

	public void setLogo(String logo){
		this.logo = logo;
	}

	public String getLogo(){
		return logo;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"ServicesItem{" + 
			"sub_services = '" + subServices + '\'' + 
			",logo = '" + logo + '\'' + 
			",id = '" + id + '\'' + 
			",name_en = '" + name + '\'' +
			"}";
		}
}