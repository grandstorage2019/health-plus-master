package grand.healthplus.models.hcaremodels;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChattersItem implements Serializable{

	@SerializedName("doctor_id")
	private int doctorId;

	@SerializedName("fname")
	private String fname;

	@SerializedName("lname")
	private String lname;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("massage")
	private String massage;

	@SerializedName("who")
	private String who;

	@SerializedName("patient_id")
	private int patientId;

	public void setDoctorId(int doctorId){
		this.doctorId = doctorId;
	}

	public int getDoctorId(){
		return doctorId;
	}

	public void setFname(String fname){
		this.fname = fname;
	}

	public String getFname(){
		return fname;
	}

	public void setLname(String lname){
		this.lname = lname;
	}

	public String getLname(){
		return lname;
	}

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public void setMassage(String massage){
		this.massage = massage;
	}

	public String getMassage(){
		return massage;
	}

	public void setWho(String who){
		this.who = who;
	}

	public String getWho(){
		return who;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public int getPatientId() {
		return patientId;
	}
}