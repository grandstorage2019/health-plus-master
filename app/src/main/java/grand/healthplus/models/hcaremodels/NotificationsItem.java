package grand.healthplus.models.hcaremodels;


import com.google.gson.annotations.SerializedName;


public class NotificationsItem{

	@SerializedName("image")
	private String image;

	@SerializedName("message")
	private String message;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"NotificationsItem{" + 
			"image = '" + image + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}