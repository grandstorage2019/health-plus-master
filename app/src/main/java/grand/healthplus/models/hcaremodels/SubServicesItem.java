package grand.healthplus.models.hcaremodels;

  import com.google.gson.annotations.SerializedName;


public class SubServicesItem{

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("price")
	private double price;

	@SerializedName("name")
	private String name;

	@SerializedName("image")
	private String image;

	@SerializedName("unit")
	private String unit;

	@SerializedName("id")
	private int id;

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setPrice(double price){
		this.price = price;
	}

	public double getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImage() {
		return image;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnit() {
		return unit;
	}
}