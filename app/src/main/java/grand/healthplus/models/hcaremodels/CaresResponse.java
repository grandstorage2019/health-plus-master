package grand.healthplus.models.hcaremodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class CaresResponse{

	@SerializedName("cares")
	private List<HealthCareItem> healthCare;

	@SerializedName("state")
	private int state;

	public void setHealthCare(List<HealthCareItem> healthCare){
		this.healthCare = healthCare;
	}

	public List<HealthCareItem> getHealthCare(){
		return healthCare;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"CaresResponse{" + 
			"health_care = '" + healthCare + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}