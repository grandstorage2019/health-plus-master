package grand.healthplus.models.hcaremodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class CareOffersResponse{

	@SerializedName("care_offers")
	private List<CareOffersItem> careOffers;

	@SerializedName("state")
	private int state;

	public void setCareOffers(List<CareOffersItem> careOffers){
		this.careOffers = careOffers;
	}

	public List<CareOffersItem> getCareOffers(){
		return careOffers;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"CareOffersResponse{" + 
			"care_offers = '" + careOffers + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}