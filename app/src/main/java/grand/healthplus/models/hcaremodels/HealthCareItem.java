package grand.healthplus.models.hcaremodels;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HealthCareItem implements Serializable{

	@SerializedName("area_name")
	private String areaName;

	@SerializedName("facebook")
	private String facebook;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("about")
	private String about;

	@SerializedName("instagram")
	private String instagram;

	@SerializedName("snapchat")
	private String snapChat;

	@SerializedName("care_id")
	private int careId;

	@SerializedName("from_time")
	private String fromTime;

	@SerializedName("location_comment")
	private String locationComment;

	@SerializedName("care_logo")
	private String careLogo;

	@SerializedName("care_name")
	private String careName;

	@SerializedName("governorate_name")
	private String governorateName;

	@SerializedName("twitter")
	private String twitter;

	@SerializedName("rate")
	private double rate;

	@SerializedName("phone")
	private String phone;

	@SerializedName("to_time")
	private String toTime;

	@SerializedName("views")
	private int views;

	@SerializedName("is_favourite")
	private boolean favourite;


	public void setFavourite(boolean favourite) {
		this.favourite = favourite;
	}

	public boolean isFavourite() {
		return favourite;
	}

	public void setAreaName(String areaName){
		this.areaName = areaName;
	}

	public String getAreaName(){
		return areaName;
	}

	public void setFacebook(String facebook){
		this.facebook = facebook;
	}

	public String getFacebook(){
		return facebook;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setAbout(String about){
		this.about = about;
	}

	public String getAbout(){
		return about;
	}

	public void setInstagram(String instagram){
		this.instagram = instagram;
	}

	public String getInstagram(){
		return instagram;
	}

	public void setCareId(int careId){
		this.careId = careId;
	}

	public int getCareId(){
		return careId;
	}

	public void setFromTime(String fromTime){
		this.fromTime = fromTime;
	}

	public String getFromTime(){
		return fromTime;
	}

	public void setLocationComment(String locationComment){
		this.locationComment = locationComment;
	}

	public String getLocationComment(){
		return locationComment;
	}

	public void setCareLogo(String careLogo){
		this.careLogo = careLogo;
	}

	public String getCareLogo(){
		return careLogo;
	}

	public void setCareName(String careName){
		this.careName = careName;
	}

	public String getCareName(){
		return careName;
	}

	public void setGovernorateName(String governorateName){
		this.governorateName = governorateName;
	}

	public String getGovernorateName(){
		return governorateName;
	}

	public void setTwitter(String twitter){
		this.twitter = twitter;
	}

	public String getTwitter(){
		return twitter;
	}

	public void setRate(double rate){
		this.rate = rate;
	}

	public double getRate(){
		return rate;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setToTime(String toTime){
		this.toTime = toTime;
	}

	public String getToTime(){
		return toTime;
	}

	public void setViews(int views){
		this.views = views;
	}

	public int getViews(){
		return views;
	}

	public void setSnapChat(String snapChat) {
		this.snapChat = snapChat;
	}

	public String getSnapChat() {
		return snapChat;
	}
}