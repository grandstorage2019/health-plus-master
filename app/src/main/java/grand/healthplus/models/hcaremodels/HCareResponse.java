package grand.healthplus.models.hcaremodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class HCareResponse{

	@SerializedName("h_care_home")
	private List<HCareHomeItem> hCareHome;

	@SerializedName("state")
	private int state;

	public void setHCareHome(List<HCareHomeItem> hCareHome){
		this.hCareHome = hCareHome;
	}

	public List<HCareHomeItem> getHCareHome(){
		return hCareHome;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"HCareResponse{" + 
			"h_care_home = '" + hCareHome + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}