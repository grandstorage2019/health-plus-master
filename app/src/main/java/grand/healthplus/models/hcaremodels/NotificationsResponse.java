package grand.healthplus.models.hcaremodels;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class NotificationsResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("notifications")
	private List<NotificationsItem> notifications;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setNotifications(List<NotificationsItem> notifications){
		this.notifications = notifications;
	}

	public List<NotificationsItem> getNotifications(){
		return notifications;
	}

	@Override
 	public String toString(){
		return 
			"NotificationsResponse{" + 
			"state = '" + state + '\'' + 
			",notifications = '" + notifications + '\'' + 
			"}";
		}
}