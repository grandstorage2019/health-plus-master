package grand.healthplus.models.hcaremodels;


import com.google.gson.annotations.SerializedName;


public class CarePicturesItem{

	@SerializedName("image")
	private String image;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	@Override
 	public String toString(){
		return 
			"CarePicturesItem{" + 
			"image = '" + image + '\'' + 
			"}";
		}
}