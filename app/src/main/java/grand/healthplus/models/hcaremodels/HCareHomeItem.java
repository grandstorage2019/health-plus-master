package grand.healthplus.models.hcaremodels;

import com.google.gson.annotations.SerializedName;


public class HCareHomeItem{

	@SerializedName("image")
	private String image;

	@SerializedName("name")
	private String name;

	@SerializedName("details")
	private String details;

	@SerializedName("link")
	private String link;

	@SerializedName("id")
	private int id;

	@SerializedName("is_promoted")
	private boolean promoted;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLink() {
		return link;
	}

	public void setPromoted(boolean promoted) {
		this.promoted = promoted;
	}

	public boolean isPromoted() {
		return promoted;
	}

	@Override
 	public String toString(){
		return
			"HCareHomeItem{" +
			"image = '" + image + '\'' +
			",name = '" + name + '\'' +
			",details = '" + details + '\'' +
			",id = '" + id + '\'' +
			"}";
		}
}