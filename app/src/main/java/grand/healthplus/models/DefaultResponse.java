package grand.healthplus.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class DefaultResponse{

	@SerializedName("result")
	private List<ResultItem> result;

	@SerializedName("state")
	private int state;

	public void setResult(List<ResultItem> result){
		this.result = result;
	}

	public List<ResultItem> getResult(){
		return result;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"DefaultResponse{" + 
			"result = '" + result + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}