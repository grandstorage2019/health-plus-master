package grand.healthplus.models;


import com.google.gson.annotations.SerializedName;


public class ResultItem{

	@SerializedName("value")
	private String value;

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"ResultItem{" + 
			"value = '" + value + '\'' + 
			"}";
		}
}