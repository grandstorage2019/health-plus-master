package grand.healthplus.models;

import android.support.v4.app.Fragment;

public class MenuModel {
    private int imageResource, id;
    private String title;


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }


    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageResource() {
        return imageResource;
    }

    public String getTitle() {
        return title;
    }
}
