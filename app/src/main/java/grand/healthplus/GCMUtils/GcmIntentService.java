package grand.healthplus.GCMUtils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONObject;

import grand.healthplus.R;
import grand.healthplus.activities.SplashScreenActivity;
import grand.healthplus.activities.doctoractivities.DoctorMainActivity;
import grand.healthplus.activities.doctoractivities.DoctorSignUpActivity;
import grand.healthplus.activities.doctoractivities.DoctorSplashScreenActivity;
import grand.healthplus.activities.doctoractivities.ReservationsActivity;
import grand.healthplus.activities.hcareactivities.HCareMainActivity;
import grand.healthplus.activities.hplusactivities.HPlusMainActivity;
import grand.healthplus.models.doctormodels.registrationmodel.SignUpResponse;
import grand.healthplus.models.doctormodels.registrationmodel.UserItem;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class GcmIntentService extends GcmListenerService {
    NotificationManager mNotificationManager;

    public GcmIntentService() {
        super();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        if (data.getString("notification_type").equals("1")) {
            logIn(data);
        } else if (data.getString("notification_type").equals("2") || data.getString("notification_type").equals("3")) {
            generalNotification(data);
        }

    }


    public void generalNotification(final Bundle bundle) {
        Intent intent = new Intent(GcmIntentService.this, HCareMainActivity.class);
        MUT.setSelectedLanguage(this);


        System.out.println("Away "+bundle.getString("notification_type"));

        if (bundle.getString("notification_type").equals("3")) {
            intent = new Intent(GcmIntentService.this, HPlusMainActivity.class);
        }

        intent.putExtra("is_notifications",true);
        intent.putExtra("bundle",bundle);



        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);


        final PendingIntent contentIntent = PendingIntent.getActivity(
                GcmIntentService.this, 1485, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
                 mBuilder.setContentIntent(contentIntent);
                 mBuilder.setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(bundle.getString("details"))
                .setContentInfo("").setSubText("")
                .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon).
                setAutoCancel(true);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mNotificationManager = (NotificationManager) GcmIntentService.this
                .getSystemService(Context.NOTIFICATION_SERVICE);




        String channelId = "channelId";


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(bundle.getString("details"))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(contentIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel 123",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(1213, notificationBuilder.build());







        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext()).build();
        ImageLoader imageloader = ImageLoader.getInstance();
        imageloader.init(config);


        System.out.println(bundle.getString("image"));


        imageloader.loadImage(  bundle.getString("image"),
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view,
                                                  Bitmap loadedImage) {
                        mBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                                .setBigContentTitle("").bigPicture(loadedImage)
                                .setBigContentTitle(bundle.getString("title"))
                                .setSummaryText(""));
                        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
                        mNotificationManager.notify(123, mBuilder.build());
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {
                        mNotificationManager.notify(123, mBuilder.build());
                        super.onLoadingFailed(imageUri, view, failReason);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        mNotificationManager.notify(123, mBuilder.build());
                        super.onLoadingCancelled(imageUri, view);
                    }
                });

    }


    private void logIn(final Bundle bundle) {

        final UserItem userItem = DoctorSharedPreferenceHelper.getUserDetails(this);


        JSONObject params = new JSONObject();
        try {
            params.put("method", "notification_login");
            params.put("email", userItem.getEmail() + "");
            params.put("password", userItem.getPassword() + "");


            ConnectionPresenter connectionPresenter = new ConnectionPresenter(this, new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {

                    try {

                        SignUpResponse signUpResponse = new Gson().fromJson(response.toString(), SignUpResponse.class);
                        if (signUpResponse.getState() == ConnectionPresenter.SUCCESS) {
                            signUpResponse.getUser().get(0).setPassword(userItem.getPassword() + "");

                            DoctorSharedPreferenceHelper.saveUserDetails(GcmIntentService.this, signUpResponse.getUser().get(0));
                            newBooking(bundle);
                        }

                    } catch (Exception e) {

                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {

                }
            });
            connectionPresenter.doctorConnect(params, false, false);
        } catch (Exception e) {
            Toast.makeText(this, "" + e.getMessage() + " " + e.getStackTrace(), Toast.LENGTH_SHORT).show();
            e.getStackTrace();
        }

    }

    public void newBooking(final Bundle bundle) {
        Intent intent = new Intent(GcmIntentService.this, ReservationsActivity.class);
        intent.putExtra("bundleDetails", bundle);
        intent.putExtra("date", bundle.getString("date"));
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this);
        final PendingIntent contentIntent = PendingIntent.getActivity(
                GcmIntentService.this, Integer.parseInt(bundle.getString("id")), intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setContentTitle(getResources().getString(R.string.new_booking))
                .setContentText(getResources().getString(R.string._got_new_booking))
                .setContentInfo("By healthplus.com").setSubText("")
                .setSmallIcon(R.mipmap.ic_launcher).
                setAutoCancel(true);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mNotificationManager = (NotificationManager) GcmIntentService.this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(123, mBuilder.build());

    }


}