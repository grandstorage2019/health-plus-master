package grand.healthplus;

/**
 * Created by sameh on 5/2/18.
 */

public interface SimpleToolBarListener {

     void onBackClicked();
     void onSkipClicked();
}
