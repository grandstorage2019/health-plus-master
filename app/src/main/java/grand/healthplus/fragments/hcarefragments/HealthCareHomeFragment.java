package grand.healthplus.fragments.hcarefragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hcareadapters.OffersAdapter;
import grand.healthplus.adapters.hplusadapters.HospitalsAdapter;
import grand.healthplus.models.hcaremodels.HCareHomeItem;
import grand.healthplus.models.hcaremodels.HCareResponse;
import grand.healthplus.models.hplusmodels.HospitalsItem;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class HealthCareHomeFragment extends Fragment {
    View rootView;

    @BindView(R.id.rl_hcare_main_add)
    RelativeLayout addLayout;
    @BindView(R.id.iv_hcare_main_add_photo)
    ImageView addPhotoImageView;
    @BindView(R.id.iv_hcare_main_add_title)
    TextView addTitleTextView;
    @BindView(R.id.rv_hcare_main_offers)
    RecyclerView offersRecyclerView;
    String link = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_hcare_main, container, false);
        ButterKnife.bind(this, rootView);
        getHomePage();
        setEvent();
        return rootView;
    }


    private void setEvent() {
        addLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startWebPage(getActivity(), link);
            }
        });
    }


    private void getHomePage() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method", "h_care_home");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                HCareResponse hCareResponse = new Gson().fromJson(response.toString(), HCareResponse.class);
                if (hCareResponse.getState() == ConnectionPresenter.SUCCESS) {
                    List<HCareHomeItem> hCareHomeItems = new ArrayList<>(hCareResponse.getHCareHome());
                    ArrayList<HCareHomeItem> unPromotedList = new ArrayList<>();

                    for (int i = 0; i < hCareHomeItems.size(); i++) {
                        if (hCareHomeItems.get(i).isPromoted()) {
                            ConnectionPresenter.loadImage(addPhotoImageView, "care_adverting/" + hCareHomeItems.get(i).getImage());
                            addTitleTextView.setText(hCareHomeItems.get(i).getName());
                            link = hCareHomeItems.get(i).getLink();
                        } else {
                            unPromotedList.add(hCareHomeItems.get(i));
                        }
                    }
                    OffersAdapter offersAdapter = new OffersAdapter(unPromotedList, getActivity());
                    ViewOperations.setRVHorzontial(getActivity(), offersRecyclerView);
                    offersRecyclerView.setAdapter(offersAdapter);
                    setEvent();
                } else {
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, true);
    }


}
