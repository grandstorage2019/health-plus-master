package grand.healthplus.fragments.hcarefragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hcareadapters.CaresAdapter;
import grand.healthplus.models.hcaremodels.CaresResponse;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class HealthCareFavouriteFragment extends Fragment {
    View rootView;


    @BindView(R.id.rv_favourites_collection)
    RecyclerView caresRecycleView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_favourites, container, false);
        ButterKnife.bind(this, rootView);

        getHealthCares();
        setEvent();
        return rootView;
    }


    private void setEvent() {

    }


    private void getHealthCares() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method", "favourites");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserId(getActivity()));

            System.out.println("Hgas : "+jsonObject.toString());

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                CaresResponse caresResponse = new Gson().fromJson(response.toString(), CaresResponse.class);
                if (caresResponse.getState() == ConnectionPresenter.SUCCESS) {
                    CaresAdapter caresAdapter = new CaresAdapter(new ArrayList<>(caresResponse.getHealthCare()), getActivity());
                    ViewOperations.setRVHVertical(getActivity(), caresRecycleView);
                    caresRecycleView.setAdapter(caresAdapter);
                } else if (caresResponse.getState() == ConnectionPresenter.EMPTY) {
                    caresRecycleView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true,true);
    }


}
