package grand.healthplus.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hcareadapters.ChattersAdapter;
import grand.healthplus.models.hcaremodels.ChattersResponse;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ChattersFragment extends Fragment {

    @BindView(R.id.rv_chatters_collection)
    RecyclerView chattersRecycleView;
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chatters, container, false);
        ButterKnife.bind(this, rootView);
        getChatters();

        return rootView;
    }



    private void getChatters() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "chatters");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("patient_id",  SharedPreferenceHelper.getUserDetails(getActivity()).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }


        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ChattersResponse chattersResponse = new Gson().fromJson(response.toString(), ChattersResponse.class);
                if (chattersResponse.getState() == ConnectionPresenter.SUCCESS) {
                    ChattersAdapter timesAdapter = new ChattersAdapter(new ArrayList<>(chattersResponse.getChatters()), getActivity());
                    ViewOperations.setRVHVertical(getActivity(), chattersRecycleView);
                    chattersRecycleView.setAdapter(timesAdapter);
                }else if (chattersResponse.getState() == ConnectionPresenter.EMPTY) {
                    chattersRecycleView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true,true);
    }

}