package grand.healthplus.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.fragments.hcarefragments.HealthCareHomeFragment;
import grand.healthplus.models.hplusmodels.DefaultResponse;
import grand.healthplus.utils.Check;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.FragmentHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ContactUsFragment extends Fragment {

    @BindView(R.id.tv_contact_us_call)
    TextView callTextView;
    @BindView(R.id.et_contact_us_message)
    EditText messageEditText;
    @BindView(R.id.btn_contact_us_send)
    Button sendBtn;


    String phoneNumber="+96595505014";

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, rootView);
        setEvents();
        return rootView;
    }

    private void setEvents() {
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Check.isEmpty(messageEditText.getText() + "")) {
                    Toast.makeText(getActivity(), "" + getResources().getString(R.string.fill_data), Toast.LENGTH_SHORT).show();
                } else {
                    sendMessage();
                }
            }
        });

        callTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1084);
                } else {
                    try{
                        MUT.makeCall(getActivity(),phoneNumber);
                    }catch (Exception e){
                        e.getStackTrace();
                    }
                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try{
                MUT.makeCall(getActivity(),phoneNumber);
            }catch (Exception e){
                e.getStackTrace();
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1084);
        }

    }

    private void sendMessage() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "send_message");
            jsonObject.put("message", messageEditText.getText());

            if (getArguments().getBoolean("isDoctor", false)) {
                jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(getActivity()).getId());
            } else {
                jsonObject.put("patient_id", DoctorSharedPreferenceHelper.getUserDetails(getActivity()).getId());
            }

        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse = new Gson().fromJson(response.toString(), DefaultResponse.class);

                if (defaultResponse.getState() == ConnectionPresenter.SUCCESS) {
                    Toast.makeText(getActivity(), "" + getResources().getString(R.string.sent_successfully), Toast.LENGTH_SHORT).show();

                    FragmentHelper.popAllFragments(getActivity());
                    FragmentHelper.addFragment(getActivity(), new HealthCareHomeFragment(), "HealthCareHomeFragment");


                }
            }

            @Override
            public void onRequestError(Object error) {
            }

        });


        connectionPresenter.connect(jsonObject, true, true);
    }


}