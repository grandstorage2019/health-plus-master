package grand.healthplus.fragments.hplusfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hplusadapters.DoctorsAdapter;
import grand.healthplus.models.hplusmodels.DoctorResponse;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class FavouritesFragment extends Fragment {
    View rootView;

    @BindView(R.id.rv_favourites_collection)
    RecyclerView favouritesRecycleView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_favourites, container, false);
        ButterKnife.bind(this, rootView);
        getFavourites();
        return rootView;
    }



    private void getFavourites() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method", "favourites");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(getActivity()).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                DoctorResponse doctorResponse = new Gson().fromJson(response.toString(), DoctorResponse.class);
                if (doctorResponse.getState() == ConnectionPresenter.SUCCESS) {
                    DoctorsAdapter doctorsAdapter = new DoctorsAdapter(new ArrayList<>(doctorResponse.getDoctors()),getActivity());
                    ViewOperations.setRVHVertical(getActivity(), favouritesRecycleView);
                    favouritesRecycleView.setAdapter(doctorsAdapter);
                }else if (doctorResponse.getState() == ConnectionPresenter.EMPTY) {
                    favouritesRecycleView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true,false);
    }

}
