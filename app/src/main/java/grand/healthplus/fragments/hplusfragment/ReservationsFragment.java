package grand.healthplus.fragments.hplusfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;


import grand.healthplus.adapters.hplusadapters.ReservationsAdapter;
import grand.healthplus.models.hplusmodels.ReservationsResponse;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ReservationsFragment extends Fragment {
    View rootView;


    @BindView(R.id.rv_reservations_collection)
    RecyclerView reservationsRecycleView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_reservations, container, false);
        ButterKnife.bind(this, rootView);
        getReservations();
        return rootView;
    }


    private void getReservations() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "reservations");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(getActivity()).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        System.out.println("jsonObject "+jsonObject.toString());

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ReservationsResponse reservationResponse = new Gson().fromJson(response.toString(), ReservationsResponse.class);
                if (reservationResponse.getState() == ConnectionPresenter.SUCCESS) {
                    ReservationsAdapter reservationsAdapter = new ReservationsAdapter(new ArrayList<>(reservationResponse.getReservations()), getActivity());
                    ViewOperations.setRVHVertical(getActivity(), reservationsRecycleView);
                    reservationsRecycleView.setAdapter(reservationsAdapter);
                }else {
                    reservationsRecycleView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true , false);
    }
}
