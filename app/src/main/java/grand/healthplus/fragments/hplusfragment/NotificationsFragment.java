package grand.healthplus.fragments.hplusfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hplusadapters.DoctorsAdapter;
import grand.healthplus.adapters.hplusadapters.NotificationsAdapter;
import grand.healthplus.models.hplusmodels.DoctorResponse;
import grand.healthplus.models.hplusmodels.NotificationResponse;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class NotificationsFragment extends Fragment {
    View rootView;

    @BindView(R.id.rv_notifications_collection)
    RecyclerView notificationsRecycleView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_notifications, container, false);
        ButterKnife.bind(this, rootView);
        getNotifications();

        return rootView;
    }



    private void getNotifications() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method", "notifications");
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                NotificationResponse notificationResponse = new Gson().fromJson(response.toString(), NotificationResponse.class);
                if (notificationResponse.getState() == ConnectionPresenter.SUCCESS) {
                    NotificationsAdapter notificationsAdapter = new NotificationsAdapter(new ArrayList<>(notificationResponse.getNotifications()),getActivity());
                    ViewOperations.setRVHVertical(getActivity(), notificationsRecycleView);
                    notificationsRecycleView.setAdapter(notificationsAdapter);
                }else if(notificationResponse.getState() == ConnectionPresenter.EMPTY){
                    notificationsRecycleView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true,false);
    }

}
