package grand.healthplus.fragments.hplusfragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.activities.hcareactivities.HealthCareSearchResultActivity;
import grand.healthplus.models.hplusmodels.AreaResponse;
import grand.healthplus.models.hplusmodels.AreasItem;
import grand.healthplus.models.hplusmodels.CountriesItem;
import grand.healthplus.models.hplusmodels.CountryResponse;
import grand.healthplus.models.hplusmodels.GovernorateResponse;
import grand.healthplus.models.hplusmodels.GovernoratesItem;
import grand.healthplus.models.hplusmodels.SpecializationResponse;
import grand.healthplus.models.hplusmodels.SpecializationsItem;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.utils.MUT;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;
import grand.healthplus.R;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;


public class HPlusSearchFragment extends Fragment {
    View rootView;

    @BindView(R.id.rl_search_region_spinner)
    RelativeLayout regionSpinnerLayout;

    @BindView(R.id.et_search_doctor_name)
    EditText doctorNameEditText;

    @BindView(R.id.img_search_arrow)
    ImageView arrowImageView;

    ArrayList<SpinnerModel> countryItems, specialtyItems, governorateItems, areaItems;
    String language = "";
    Dialog countryDialog = null, specialtyDialog = null, governorateDialog = null, areaDialog = null;

    int selectedCountry = -1, selectedSpecialty = -1, selectedGovernorate = -1, selectedArea = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_hplus_search, container, false);
        ButterKnife.bind(this, rootView);
        language = SharedPreferenceHelper.getCurrentLanguage(getActivity());
        setEvents();

        if(SharedPreferenceHelper.getCurrentLanguage(getActivity()).equals("ar")){
            arrowImageView.setRotation(180);
        }


        return rootView;
    }


    private void setEvents() {
        regionSpinnerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSpecializations();
            }
        });

        doctorNameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if(doctorNameEditText.getText().length()>0) {
                        Intent intent = new Intent(getActivity(), HealthPlusSearchResultActivity.class);
                        intent.putExtra("isSearchByName", true);
                        intent.putExtra("query", doctorNameEditText.getText()+"");
                        startActivity(intent);
                    }else {
                        MUT.lToast(getActivity(),getResources().getString(R.string.please_enter_name));
                    }
                    return true;
                }
                return false;
            }
        });
    }


    private void getSpecializations() {
        specialtyItems = new ArrayList<>();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "specializations");
            jsonObject.put("language", language);
        }
        catch (Exception e){
            e.getStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                SpecializationResponse specializationResponse = new Gson().fromJson(response.toString(), SpecializationResponse.class);
                List<SpecializationsItem> specializationItems = specializationResponse.getSpecializations();

                for (int i = 0; i < specializationItems.size(); i++) {
                    specialtyItems.add(new SpinnerModel( specializationItems.get(i).getId(), specializationItems.get(i).getName()));
                }


                specialtyDialog = ViewOperations.setSpinnerDialog(getActivity(), specialtyItems, getResources().getString(R.string.specialty), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedSpecialty = specialtyItems.get(position).getId();
                        getCountries();
                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.connect(jsonObject, true,false);
    }

    private void getGovernerates() {
        governorateItems = new ArrayList<>();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "governorates");
            jsonObject.put("language", language);
            jsonObject.put("country_id",selectedCountry);
        }
        catch (Exception e){
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                GovernorateResponse governorateResponse = new Gson().fromJson(response.toString(), GovernorateResponse.class);
                List<GovernoratesItem> governoratesItems = governorateResponse.getGovernorates();

                for (int i = 0; i < governoratesItems.size(); i++) {

                    governorateItems.add(new SpinnerModel(governoratesItems.get(i).getId(), governoratesItems.get(i).getName()));
                }

                governorateDialog = ViewOperations.setSpinnerDialog(getActivity(), governorateItems, getResources().getString(R.string.governorates), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedGovernorate = governorateItems.get(position).getId();
                        getAreas();
                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });



        connectionPresenter.connect(jsonObject, true,false);
    }


    private void getCountries() {
        countryItems = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "countries");
            jsonObject.put("language", language);
            jsonObject.put("country_id",selectedGovernorate);
        }
        catch (Exception e){
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                CountryResponse countryResponse = new Gson().fromJson(response.toString(), CountryResponse.class);
                List<CountriesItem> data = countryResponse.getCountries();

                for (int i = 0; i < data.size(); i++) {
                    countryItems.add(new SpinnerModel(data.get(i).getId(), data.get(i).getName()));
                }


                countryDialog = ViewOperations.setSpinnerDialog(getActivity(), countryItems, getResources().getString(R.string.countries), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedCountry = countryItems.get(position).getId();
                        selectedGovernorate = -1;
                        getGovernerates();
                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.connect(jsonObject, true,false);
    }


    private void getAreas() {
        areaItems = new ArrayList<>();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "areas");
            jsonObject.put("language", language);
            jsonObject.put("governorate_id",selectedGovernorate);
        }
        catch (Exception e){
            e.getStackTrace();
        }


        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                AreaResponse areaResponse = new Gson().fromJson(response.toString(), AreaResponse.class);
                List<AreasItem> data = areaResponse.getAreas();
                areaItems.add(new SpinnerModel(-1, getResources().getString(R.string.all)));
                for (int i = 0; i < data.size(); i++) {
                    areaItems.add(new SpinnerModel(data.get(i).getId(), data.get(i).getName()));
                }


                areaDialog = ViewOperations.setSpinnerDialog(getActivity(), areaItems, getResources().getString(R.string.area), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedArea = areaItems.get(position).getId();
                        Intent intent = new Intent(getActivity(),HealthPlusSearchResultActivity.class);
                        intent.putExtra("selectedArea",selectedArea);
                        intent.putExtra("selectedSpecialty",selectedSpecialty);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.connect(jsonObject, true,false);
    }
}
