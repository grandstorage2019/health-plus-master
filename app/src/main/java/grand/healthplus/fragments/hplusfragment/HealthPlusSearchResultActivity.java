package grand.healthplus.fragments.hplusfragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.activities.doctoractivities.PatientsActivity;
import grand.healthplus.activities.hcareactivities.HealthCareSearchResultActivity;
import grand.healthplus.adapters.hplusadapters.DoctorsAdapter;
import grand.healthplus.R;
import grand.healthplus.models.doctormodels.InsuranceCompaniesItem;
import grand.healthplus.models.doctormodels.InsuranceCompanyResponse;
import grand.healthplus.models.hplusmodels.DoctorResponse;
import grand.healthplus.models.trashmodels.SpinnerModel;
import grand.healthplus.trash.SpinnerListener;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;


public class HealthPlusSearchResultActivity extends AppCompatActivity {

    @BindView(R.id.rv_search_result_collection)
    RecyclerView doctorsRecycleView;
    @BindView(R.id.iv_actionbar_icon)
    ImageView arrowImageView;
    @BindView(R.id.iv_actionbar_sort)
    ImageView sortImageView;
    boolean isSearchByName = false;
    boolean searchByInsurance = false;
    int   selectedInsurance=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        ButterKnife.bind(this);

        if (SharedPreferenceHelper.getCurrentLanguage(this).equals("ar")) {
            arrowImageView.setRotation(180);
        }

        setEvent();
    }


    private void setEvent() {
        sortImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getInsuranceCompanies();
            }
        });
    }


    @Override
    protected void onResume() {
        try {
            isSearchByName = getIntent().getBooleanExtra("isSearchByName", false);
        } catch (Exception e) {

        }
        getDoctors();
        super.onResume();
    }

    public void goBack(View view) {
        finish();
    }


    Dialog insuranceCompaniesDialog = null;
    ArrayList<SpinnerModel> insuranceCompaniesItems;

    private void getInsuranceCompanies() {
        insuranceCompaniesItems = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "insurances");
            jsonObject.put("language",SharedPreferenceHelper.getCurrentLanguage(this));
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(HealthPlusSearchResultActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {


                InsuranceCompanyResponse countryResponse = new Gson().fromJson(response.toString(), InsuranceCompanyResponse.class);
                List<InsuranceCompaniesItem> data = countryResponse.getInsuranceCompanies();
                insuranceCompaniesItems.add(new SpinnerModel(-1, getResources().getString(R.string.all)));
                for (int i = 0; i < data.size(); i++) {
                    insuranceCompaniesItems.add(new SpinnerModel(data.get(i).getId(), data.get(i).getName()));
                }


                insuranceCompaniesDialog = ViewOperations.setSpinnerDialog(HealthPlusSearchResultActivity.this, insuranceCompaniesItems, getResources().getString(R.string.insurance_company_title), new SpinnerListener() {
                    @Override
                    public void onItemClicked(int position) {
                        selectedInsurance = insuranceCompaniesItems.get(position).getId();
                        searchByInsurance = true;
                        getDoctors();

                    }
                });
            }

            @Override
            public void onRequestError(Object error) {
            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, false);
    }


    private void getDoctors() {
        JSONObject jsonObject = new JSONObject();

        try {

            /*if (searchByInsurance) {
                jsonObject.put("method", "search_by_insurance");


            } else*/

                if (isSearchByName) {
                jsonObject.put("method", "search_by_doctor");
                jsonObject.put("query", getIntent().getStringExtra("query"));
            } else {
                jsonObject.put("method", "search");
                jsonObject.put("area_id", getIntent().getIntExtra("selectedArea", -1));
                jsonObject.put("specialization_id", getIntent().getIntExtra("selectedSpecialty", -1));
            }
            jsonObject.put("insurance_id", selectedInsurance);
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(HealthPlusSearchResultActivity.this));
            jsonObject.put("patient_id", SharedPreferenceHelper.getUserDetails(this).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }



        ConnectionPresenter connectionPresenter = new ConnectionPresenter(HealthPlusSearchResultActivity.this, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DoctorResponse doctorResponse = new Gson().fromJson(response.toString(), DoctorResponse.class);
                if (doctorResponse.getState() == ConnectionPresenter.SUCCESS) {
                    DoctorsAdapter hospitalsAdapter = new DoctorsAdapter(new ArrayList<>(doctorResponse.getDoctors()), HealthPlusSearchResultActivity.this);
                    ViewOperations.setRVHVertical(HealthPlusSearchResultActivity.this, doctorsRecycleView);
                    doctorsRecycleView.setAdapter(hospitalsAdapter);
                    doctorsRecycleView.setVisibility(View.VISIBLE);
                } else if (doctorResponse.getState() == ConnectionPresenter.EMPTY) {
                    doctorsRecycleView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true, false);
    }


}
