package grand.healthplus.fragments.doctorfragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.doctoractivities.PatientsActivity;
import grand.healthplus.adapters.doctoradapters.PatentsAdapter;
import grand.healthplus.models.doctormodels.DoctorPatientsResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class AddPatientFragment extends Fragment {

    @BindView(R.id.ll_add_patients_add)
    LinearLayout addLayout;
    @BindView(R.id.rv_add_patients_collection)
    RecyclerView patientsRecyclerView;

    @BindView(R.id.iv_patients_add)
    ImageView addImageView;

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_patients, container, false);
        ButterKnife.bind(this, rootView);
        setEvents();
        return rootView;
    }


    @Override
    public void onResume() {
        getPatients();
        super.onResume();
    }

    private void getPatients() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "doctor_patients");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("added_by_doctor_id", DoctorSharedPreferenceHelper.getUserDetails(getActivity()).getId());


        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {

                DoctorPatientsResponse doctorPatientsResponse = new Gson().fromJson(response.toString(), DoctorPatientsResponse.class);
                if (doctorPatientsResponse.getState() == ConnectionPresenter.SUCCESS) {
                    PatentsAdapter plansAdapter = new PatentsAdapter(new ArrayList<>(doctorPatientsResponse.getPatients()), getActivity());
                    ViewOperations.setRVHVertical(getActivity(), patientsRecyclerView);
                    patientsRecyclerView.setAdapter(plansAdapter);
                    addLayout.setVisibility(View.GONE);
                    addImageView.setVisibility(View.VISIBLE);
                    patientsRecyclerView.setVisibility(View.VISIBLE);
                }else if(doctorPatientsResponse.getState() == ConnectionPresenter.EMPTY) {
                    addLayout.setVisibility(View.VISIBLE);
                    patientsRecyclerView.setVisibility(View.GONE);
                    addImageView.setVisibility(View.GONE);
                }

            }
            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, true);
    }


    private void setEvents() {

        addImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), PatientsActivity.class);
            }
        });

        addLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), PatientsActivity.class);
            }
        });


    }


}