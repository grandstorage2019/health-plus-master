package grand.healthplus.fragments.doctorfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.doctoractivities.ChattersActivity;
import grand.healthplus.activities.doctoractivities.ClinicAddressActivity;
import grand.healthplus.activities.doctoractivities.ClinicNameNumberActivity;
import grand.healthplus.activities.doctoractivities.AboutDoctorActivity;
import grand.healthplus.activities.doctoractivities.ClinicPicturesActivity;
import grand.healthplus.activities.doctoractivities.InsuranceCompaniesActivity;
import grand.healthplus.activities.doctoractivities.MedicalSpecialtiesActivity;
import grand.healthplus.activities.doctoractivities.ServicesActivity;
import grand.healthplus.activities.hplusactivities.RatingActivity;
import grand.healthplus.models.doctormodels.DoctorHomeResponse;
import grand.healthplus.trash.CompressObject;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;
import grand.healthplus.utils.FileOperations;
import grand.healthplus.utils.ImageCompression;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class AboutDoctorFragment extends Fragment {


    @BindView(R.id.iv_about_doctor_photo)
    ImageView photoImageView;
    @BindView(R.id.tv_about_doctor_name)
    TextView nameTextView;
    @BindView(R.id.tv_about_doctor_specialist)
    TextView specialistTextView;
    @BindView(R.id.tv_doctor_full_info_rating)
    TextView ratingTextView;
    @BindView(R.id.tv_doctor_full_info_reviewers)
    TextView reviewersTextView;
    @BindView(R.id.tv_doctor_full_info_books)
    TextView booksTextView;
    @BindView(R.id.rl_doctor_full_info_about)
    RelativeLayout aboutLayout;
    @BindView(R.id.rl_doctor_full_info_spcialties)
    RelativeLayout specialtiesLayout;
    @BindView(R.id.rl_doctor_full_info_detection)
    RelativeLayout detectionLayout;
    @BindView(R.id.rl_doctor_full_info_address)
    RelativeLayout clinicAddressLayout;
    @BindView(R.id.rl_doctor_full_info_clinic_name)
    RelativeLayout clinicNameLayout;
    @BindView(R.id.rl_doctor_full_info_pictures)
    RelativeLayout picturesLayout;
    @BindView(R.id.rl_doctor_full_info_company)
    RelativeLayout companyLayout;
    @BindView(R.id.tv_doctor_full_info_doctor_info)
    TextView doctorInfoTextView;
    @BindView(R.id.tv_doctor_full_info_clinic_info)
    TextView clinicInfoTextView;
    @BindView(R.id.ll_doctor_full_info_clinic_tab)
    LinearLayout clinicTabLayout;
    @BindView(R.id.ll_doctor_full_info_doctor_tab)
    LinearLayout doctorTabLayout;

    @BindView(R.id.ll_doctor_full_info_rating)
    LinearLayout ratingLayout;
    @BindView(R.id.ll_doctor_full_info_books)
    LinearLayout booksLayout;


    @BindView(R.id.iv_about_doctor_chat)
    ImageView chatImageView;
    View rootView;
    boolean startLoading = true;
    int specializationId = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_doctor_full_info, container, false);
        ButterKnife.bind(this, rootView);
        setEvents();

        return rootView;
    }


    @Override
    public void onResume() {
        if (startLoading) {
            getHomePage();
        }
        super.onResume();
    }

    private void getHomePage() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "doctor_home");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(getActivity()).getId());
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DoctorHomeResponse doctorHomeResponse = new Gson().fromJson(response.toString(), DoctorHomeResponse.class);


                if (doctorHomeResponse.getProfileImage().equals("NULL") || doctorHomeResponse.getProfileImage().equals("null") || doctorHomeResponse.getProfileImage().equals("") || doctorHomeResponse.getProfileImage() == null) {
                    photoImageView.setImageResource(R.drawable.ic_user_temp);
                } else {
                    ConnectionPresenter.loadImage(photoImageView, "doctor_profile/" + doctorHomeResponse.getProfileImage());
                }

                nameTextView.setText(doctorHomeResponse.getName());
                specialistTextView.setText(doctorHomeResponse.getSpecialization());
                ratingTextView.setText(doctorHomeResponse.getStars() + "");
                reviewersTextView.setText(doctorHomeResponse.getViews() + " " + getResources().getString(R.string.reviewer));
                booksTextView.setText(doctorHomeResponse.getBooks() + "");
                specializationId = doctorHomeResponse.getSpecializationId();
            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, true);
    }


    private void setEvents() {
        aboutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), AboutDoctorActivity.class);
            }
        });

        specialtiesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MedicalSpecialtiesActivity.class);
                intent.putExtra("specialization_id", specializationId);
                startActivity(intent);
            }
        });

        detectionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), ServicesActivity.class);
            }
        });


        clinicNameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), ClinicNameNumberActivity.class);
            }
        });

        picturesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), ClinicPicturesActivity.class);

            }
        });

        companyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), InsuranceCompaniesActivity.class);
            }
        });

        clinicAddressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), ClinicAddressActivity.class);
            }
        });

        doctorInfoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doctorInfoTextView.setBackgroundResource(R.drawable.multi_gray_color_background);
                clinicInfoTextView.setBackgroundResource(R.drawable.multi_light_gray_color_background);
                doctorInfoTextView.setTextColor(getResources().getColor(R.color.colorWhite));
                clinicInfoTextView.setTextColor(getResources().getColor(R.color.colorGrayLevel4));
                clinicTabLayout.setVisibility(View.GONE);
                doctorTabLayout.setVisibility(View.VISIBLE);
            }
        });

        clinicInfoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doctorInfoTextView.setBackgroundResource(R.drawable.multi_light_gray_color_background);
                clinicInfoTextView.setBackgroundResource(R.drawable.multi_gray_color_background);
                doctorInfoTextView.setTextColor(getResources().getColor(R.color.colorGrayLevel4));
                clinicInfoTextView.setTextColor(getResources().getColor(R.color.colorWhite));
                clinicTabLayout.setVisibility(View.VISIBLE);
                doctorTabLayout.setVisibility(View.GONE);
            }
        });

        chatImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), ChattersActivity.class);
            }
        });

        photoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });


        ratingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RatingActivity.class);
                intent.putExtra("doctor", true);
                intent.putExtra("doctorId",DoctorSharedPreferenceHelper.getUserDetails(getActivity()).getId());
                startActivity(intent);
            }
        });

        booksLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

    CompressObject compressObject;
    String file;
    byte[] bytes;
    String image = "", imageName = "";
    FileOperations fileOperations;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        startLoading = true;
        try {
            Uri lastData = data.getData();

            if (lastData == null || data == null) {
                lastData = FileOperations.specialCameraSelector(getActivity(), (Bitmap) data.getExtras().get("data"));
            }

            if (lastData == null) {
                return;
            } else {
                fileOperations = new FileOperations();
                file = fileOperations.getPath(getActivity(), lastData);
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_attach_image);
                try {
                    dialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(Color.TRANSPARENT));
                } catch (Exception e) {
                    e.getStackTrace();
                }
                final ImageView attachImage = dialog.findViewById(R.id.iv_attach_image_photo);
                Button send = dialog.findViewById(R.id.btn_attach_image_confirm);
                Button cancel = dialog.findViewById(R.id.btn_attach_image_cancel);
                final File f = new File(file);
                if (f.exists()) {

                    compressObject = new ImageCompression(getActivity()).compressImage(file, f.getName());
                    attachImage.setImageBitmap(compressObject.getImage());
                    bytes = compressObject.getByteStream().toByteArray();

                }

                send.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        image = Base64.encodeToString(bytes, 0);
                        imageName = f.getName();
                        uploadImage();
                        dialog.cancel();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void uploadImage() {

        JSONObject params = new JSONObject();
        try {
            params.put("method", "upload_profile_image");
            params.put("image_name", imageName + "");
            params.put("image_data", image);
            params.put("id", DoctorSharedPreferenceHelper.getUserDetails(getActivity()).getId());
            ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
                @Override
                public void onRequestSuccess(JSONObject response) {
                    try {
                        if (response.getInt("state") == ConnectionPresenter.SUCCESS) {
                            MUT.lToast(getActivity(), getResources().getString(R.string.image_uploaded));
                            ConnectionPresenter.loadImage(photoImageView, "doctor_profile/" + response.getString("image"));
                        } else {
                            MUT.lToast(getActivity(), getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }

                @Override
                public void onRequestError(Object error) {
                }
            });
            connectionPresenter.doctorConnect(params, true, false);
        } catch (Exception e) {
            e.getStackTrace();
        }

    }

    public void selectImage() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.select_image));
        builder.setItems(new CharSequence[]{getResources().getString(R.string.gallery),
                        getResources().getString(R.string.camera)},
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent();
                                galleryIntent.setType("image/*");
                                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                                startLoading = false;
                                startActivityForResult(galleryIntent, 1024);

                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startLoading = false;
                                startActivityForResult(intent, 1023);

                                break;

                            default:
                                break;
                        }
                    }
                });

        builder.show();
    }
}