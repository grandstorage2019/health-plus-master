package grand.healthplus.fragments.doctorfragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;

import grand.healthplus.activities.doctoractivities.TimesOfWorkActivity;
import grand.healthplus.adapters.doctoradapters.ReservationsAdapter;

import grand.healthplus.models.doctormodels.ReservationResponse;
import grand.healthplus.utils.DoctorSharedPreferenceHelper;

import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class ReservationsFragment extends Fragment {


    @BindView(R.id.rv_reservations_collection)
    RecyclerView reservationsRecycleView;

    @BindView(R.id.ll_doctor_reservation_add_times)
    LinearLayout addTimesLayout;

    @BindView(R.id.iv_reservations_add)
    ImageView addImageView;

    View rootView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_doctor_reservations, container, false);
        ButterKnife.bind(this, rootView);
        setEvents();


        addTimesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), TimesOfWorkActivity.class);
            }
        });

        addImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MUT.startActivity(getActivity(), TimesOfWorkActivity.class);
            }
        });


        return rootView;
    }


    @Override
    public void onResume() {
        getReservations();
        super.onResume();
    }

    private void getReservations() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "reservations");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("doctor_id", DoctorSharedPreferenceHelper.getUserDetails(getActivity()).getId());


        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                ReservationResponse reservationResponse = new Gson().fromJson(response.toString(), ReservationResponse.class);
                if (reservationResponse.getState() == ConnectionPresenter.SUCCESS) {
                    ReservationsAdapter reservationsAdapter = new ReservationsAdapter(new ArrayList<>(reservationResponse.getReservations()), getActivity());
                    ViewOperations.setRVHVertical(getActivity(), reservationsRecycleView);
                    reservationsRecycleView.setAdapter(reservationsAdapter);
                    addTimesLayout.setVisibility(View.GONE);
                    reservationsRecycleView.setVisibility(View.VISIBLE);
                    addImageView.setVisibility(View.VISIBLE);
                }else {
                    reservationsRecycleView.setVisibility(View.GONE);
                    addTimesLayout.setVisibility(View.VISIBLE);
                    addImageView.setVisibility(View.GONE);
                }

            }
            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.doctorConnect(jsonObject, true, true);
    }


    private void setEvents() {



    }


}