package grand.healthplus.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.activities.ChooseDepartmentActivity;
import grand.healthplus.activities.SignUpActivity;
import grand.healthplus.activities.SplashScreenActivity;
import grand.healthplus.activities.hplusactivities.HPlusMainActivity;
import grand.healthplus.utils.MUT;
import grand.healthplus.utils.SharedPreferenceHelper;

public class SettingsFragment extends Fragment {

    @BindView(R.id.rg_language_container)
    RadioGroup languageContainerRadioGroup;

    @BindView(R.id.tv_edit_profile)
    TextView editProfileTextView;

    @BindView(R.id.tv_settings_change_language)
    TextView changeLanguageTextView;
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, rootView);
        getEvents();

        if(SharedPreferenceHelper.getUserDetails(getActivity())==null){
            editProfileTextView.setVisibility(View.GONE);
        }
        return rootView;
    }



    private void getEvents() {

        languageContainerRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if(R.id.rb_settings_arabic == i){
                    MUT.changeLanguage(getActivity(),"ar");
                    getActivity().finishAffinity();
                    MUT.startActivity(getActivity(), ChooseDepartmentActivity.class);
                }else {
                    MUT.changeLanguage(getActivity(),"en");
                    getActivity().finishAffinity();
                    MUT.startActivity(getActivity(), ChooseDepartmentActivity.class);
                }

            }
        });

        editProfileTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SignUpActivity.class);
                intent.putExtra("isEdit",true);
                startActivity(intent);
            }
        });

        changeLanguageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Drawable img =null;



                if (languageContainerRadioGroup.getVisibility()==View.VISIBLE){
                    languageContainerRadioGroup.setVisibility(View.GONE);
                    img =  getActivity().getResources().getDrawable(R.drawable.ic_down_arrow );

                }else {
                    languageContainerRadioGroup.setVisibility(View.VISIBLE);
                    img =  getActivity().getResources().getDrawable( R.drawable.ic_up_arrow );

                }

                if(SharedPreferenceHelper.getCurrentLanguage(getActivity()).equals("en")) {
                    changeLanguageTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
                }else {
                    changeLanguageTextView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                }
            }
        });
    }

}