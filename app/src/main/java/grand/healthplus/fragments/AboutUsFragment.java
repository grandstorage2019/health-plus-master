package grand.healthplus.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import grand.healthplus.R;
import grand.healthplus.adapters.hcareadapters.AboutUsAdapter;
import grand.healthplus.models.hcaremodels.AboutUsResponse;
import grand.healthplus.utils.SharedPreferenceHelper;
import grand.healthplus.utils.ViewOperations;
import grand.healthplus.vollyutils.ConnectionPresenter;
import grand.healthplus.vollyutils.ConnectionView;

public class AboutUsFragment extends Fragment {

    @BindView(R.id.rv_about_us_collection)
    RecyclerView aboutUsRecycleView;
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_about_us, container, false);
        ButterKnife.bind(this, rootView);
        getAboutUs();
        return rootView;
    }



    private void getAboutUs() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "about_us");
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                AboutUsResponse aboutUsResponse = new Gson().fromJson(response.toString(), AboutUsResponse.class);
                if (aboutUsResponse.getState() == ConnectionPresenter.SUCCESS) {
                    AboutUsAdapter timesAdapter = new AboutUsAdapter(new ArrayList<>(aboutUsResponse.getAboutUs()), getActivity());
                    ViewOperations.setRVHVertical(getActivity(), aboutUsRecycleView);
                    aboutUsRecycleView.setAdapter(timesAdapter);
                }

            }

            @Override
            public void onRequestError(Object error) {

            }

        });
        connectionPresenter.connect(jsonObject, true,true);
    }

}